<?php

Route::get('/', function () {
    return redirect('/pt');
});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Admin', 'prefix' => 'cms', 'as' => 'admin.'], function () {
    Route::get('/', 'Auth\HomeController@index')->name('home');
    Route::get('home', 'Auth\HomeController@index')->name('home');

    //Admin Login
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    //Admin Register
    Route::get('register', 'Auth\RegisterController@showRegistrationForm');
    Route::post('register', 'Auth\RegisterController@register');

    //Admin Passwords
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');

    // Cadastros
    Route::group(['middleware' => 'auth:admin'], function () {
        Route::get('image_editor/upload', 'ImageUploadController@index');
        Route::post('image_editor/upload', 'ImageUploadController@imageUpload');

        Route::group(['namespace' => 'Company', 'prefix' => 'empresa', 'as' => 'empresa.'], function () {
            Route::get('institucional', 'InstitutionalController@index')->name('institucional');
            Route::post('institucional/store', 'InstitutionalController@store')->name('institucional.store');
            Route::post('institucional/store/{id}', 'InstitutionalController@store')->name('institucional.store.{id}');

            // Rotas Galeria Institucional
            Route::post('institucional/galeria/store/', 'InstitutionalController@storeGallery')->name('institucional.galeria.store');
            Route::post('institucional/galeria/reorder', 'InstitutionalController@sortableGallery')->name('institucional.galeria.reorder');
            Route::get('institucional/galeria/delete/{id}', 'InstitutionalController@destroyItemGallery')->name('institucional.galeria.delete.{id}');
            Route::get('institucional/get_items_gallery', 'InstitutionalController@getItemsGallery')->name('institucional.get_items_gallery');

            // Rotas de Conceitos Estrategicos Institucionais
            Route::post('institucional/conceitos/reorder', 'InstitutionalController@sortableConcept')->name('institucional.conceitos.reorder');
            Route::post('institucional/conceitos/store', 'InstitutionalController@storeConcept')->name('institucional.conceitos.store');
            Route::post('institucional/conceitos/update/{id}', 'InstitutionalController@updateConcept')->name('institucional.conceitos.update.{id}');
            Route::get('institucional/conceitos/editar/{id}', 'InstitutionalController@editConcept')->name('institucional.conceitos.editar.{id}');
            Route::get('institucional/conceitos/delete/{id}', 'InstitutionalController@destroyConcept')->name('institucional.conceitos.delete.{id}');

            // Rotas de Certificações
            Route::get('certificacoes', 'CertificationController@index')->name('certificacoes');
            Route::post('certificacoes/reorder', 'CertificationController@sortable')->name('certificacoes.reorder');
            Route::post('certificacoes/store', 'CertificationController@store')->name('certificacoes.store');
            Route::post('certificacoes/update/{id}', 'CertificationController@update')->name('certificacoes.update.{id}');
            Route::get('certificacoes/editar/{id}', 'CertificationController@edit')->name('certificacoes.editar.{id}');
            Route::get('certificacoes/delete/{id}', 'CertificationController@destroy')->name('certificacoes.delete.{id}');

            // Rotas de Responsabilidades Sociais
            Route::get('responsabilidade_social', 'SocialResponsabilityController@index')->name('responsabilidade_social');
            Route::post('responsabilidade_social/store', 'SocialResponsabilityController@store')->name('responsabilidade_social.store');
            Route::post('responsabilidade_social/store/{id}', 'SocialResponsabilityController@store')->name('responsabilidade_social.store.{id}');

            // Rotas de Responsabilidades Sociais Projetos
            Route::post('responsabilidade_social/projetos/reorder', 'SocialResponsabilityController@sortableProjects')->name('responsabilidade_social.projetos.reorder');
            Route::post('responsabilidade_social/projetos/store', 'SocialResponsabilityController@storeProjects')->name('responsabilidade_social.projetos.store');
            Route::post('responsabilidade_social/projetos/update/{id}', 'SocialResponsabilityController@updateProjects')->name('responsabilidade_social.projetos.update.{id}');
            Route::get('responsabilidade_social/projetos/editar/{id}', 'SocialResponsabilityController@editProjects')->name('responsabilidade_social.projetos.editar.{id}');
            Route::get('responsabilidade_social/projetos/delete/{id}', 'SocialResponsabilityController@destroyProjects')->name('responsabilidade_social.projetos.delete.{id}');

            // Rotas de Parceiros
            Route::get('parcerias', 'PartnersController@index')->name('parcerias');
            Route::post('parcerias/store', 'PartnersController@store')->name('parcerias.store');
            Route::post('parcerias/store/{id}', 'PartnersController@store')->name('parcerias.store.{id}');

            // Rotas de GALERIA PARCEIROS
            Route::post('parcerias/galeria/reorder', 'PartnersController@sortableGallery')->name('parcerias.galeria.reorder');
            Route::post('parcerias/galeria/store', 'PartnersController@storeGallery')->name('parcerias.galeria.store');
            Route::post('parcerias/galeria/update/{id}', 'PartnersController@updateGallery')->name('parcerias.galeria.update.{id}');
            Route::get('parcerias/galeria/editar/{id}', 'PartnersController@editGallery')->name('parcerias.galeria.editar.{id}');
            Route::get('parcerias/galeria/delete/{id}', 'PartnersController@destroyGallery')->name('parcerias.galeria.delete.{id}');

            // Rotas de DEPOIMENTOS PARCEIROS
            Route::post('parcerias/depoimentos/reorder', 'PartnersController@sortableTestimonials')->name('parcerias.depoimentos.reorder');
            Route::post('parcerias/depoimentos/store', 'PartnersController@storeTestimonials')->name('parcerias.depoimentos.store');
            Route::post('parcerias/depoimentos/update/{id}', 'PartnersController@updateTestimonials')->name('parcerias.depoimentos.update.{id}');
            Route::get('parcerias/depoimentos/editar/{id}', 'PartnersController@editTestimonials')->name('parcerias.depoimentos.editar.{id}');
            Route::get('parcerias/depoimentos/delete/{id}', 'PartnersController@destroyTestimonials')->name('parcerias.depoimentos.delete.{id}');

            Route::get('produtos_sob_medida', 'CustomProductsController@index')->name('produtos_sob_medida');
            Route::post('produtos_sob_medida/store', 'CustomProductsController@store')->name('produtos_sob_medida.store');
            Route::post('produtos_sob_medida/store/{id}', 'CustomProductsController@store')->name('produtos_sob_medida.store.{id}');

            // Rotas Galeria Institucional
            Route::post('produtos_sob_medida/galeria/store/', 'CustomProductsController@storeGallery')->name('produtos_sob_medida.galeria.store');
            Route::post('produtos_sob_medida/galeria/reorder', 'CustomProductsController@sortableGallery')->name('produtos_sob_medida.galeria.reorder');
            Route::get('produtos_sob_medida/galeria/delete/{id}', 'CustomProductsController@destroyItemGallery')->name('produtos_sob_medida.galeria.delete.{id}');
            Route::get('produtos_sob_medida/get_items_gallery', 'CustomProductsController@getItemsGallery')->name('produtos_sob_medida.get_items_gallery');

            // Rotas de Comentários
            Route::get('posts/{post_id}/comentarios/', 'CommentsController@index')->name('posts.{post_id}.comentarios');
            Route::get('posts/{post_id}/comentarios/editar/{id}', 'CommentsController@edit')->name('posts.{post_id}.comentarios.editar.{id}');
            Route::get('posts/{post_id}/comentarios/delete/{id}', 'CommentsController@destroy')->name('posts.{post_id}.comentarios.delete.{id}');
            Route::post('posts/{post_id}/comentarios/update/{id}', 'CommentsController@update')->name('posts.{post_id}.comentarios.update.{id}');
        });

        // Rotas de Sliders
        Route::get('sliders', 'SlidersController@index')->name('sliders');
        Route::post('sliders/reorder', 'SlidersController@sortable')->name('sliders.reorder');
        Route::post('sliders/store', 'SlidersController@store')->name('sliders.store');
        Route::post('sliders/update/{id}', 'SlidersController@update')->name('sliders.update.{id}');
        Route::get('sliders/editar/{id}', 'SlidersController@edit')->name('sliders.editar.{id}');
        Route::get('sliders/delete/{id}', 'SlidersController@destroy')->name('sliders.delete.{id}');

        // Rotas de Catalog
        Route::get('catalogos', 'CatalogController@index')->name('catalogos');
        Route::post('catalogos/reorder', 'CatalogController@sortable')->name('catalogos.reorder');
        Route::post('catalogos/store', 'CatalogController@store')->name('catalogos.store');
        Route::post('catalogos/update/{id}', 'CatalogController@update')->name('catalogos.update.{id}');
        Route::get('catalogos/editar/{id}', 'CatalogController@edit')->name('catalogos.editar.{id}');
        Route::get('catalogos/delete/{id}', 'CatalogController@destroy')->name('catalogos.delete.{id}');

        // Rotas de FAQ
        Route::get('faq', 'FaqController@index')->name('faq');
        Route::post('faq/reorder', 'FaqController@sortable')->name('faq.reorder');
        Route::post('faq/store', 'FaqController@store')->name('faq.store');
        Route::post('faq/update/{id}', 'FaqController@update')->name('faq.update.{id}');
        Route::get('faq/editar/{id}', 'FaqController@edit')->name('faq.editar.{id}');
        Route::get('faq/delete/{id}', 'FaqController@destroy')->name('faq.delete.{id}');

        // Rotas de Banco de Imagens
        Route::get('banco_de_imagens', 'ImageBankController@index')->name('banco_de_imagens');
        Route::post('banco_de_imagens/reorder', 'ImageBankController@sortable')->name('banco_de_imagens.reorder');
        Route::post('banco_de_imagens/store', 'ImageBankController@store')->name('banco_de_imagens.store');
        Route::post('banco_de_imagens/update/{id}', 'ImageBankController@update')->name('banco_de_imagens.update.{id}');
        Route::get('banco_de_imagens/editar/{id}', 'ImageBankController@edit')->name('banco_de_imagens.editar.{id}');
        Route::get('banco_de_imagens/delete/{id}', 'ImageBankController@destroy')->name('banco_de_imagens.delete.{id}');

        // Rotas de Banco de Imagens Lista
        Route::get('banco_de_imagens/lista/{category_id}', 'ImageBankListController@index')->name('banco_de_imagens.lista.{category_id}');
        Route::post('banco_de_imagens/lista/{category_id}/reorder', 'ImageBankListController@sortable')->name('banco_de_imagens.lista.{category_id}.reorder');
        Route::post('banco_de_imagens/lista/{category_id}/store', 'ImageBankListController@store')->name('banco_de_imagens.lista.{category_id}.store');
        Route::post('banco_de_imagens/lista/{category_id}/update/{id}', 'ImageBankListController@update')->name('banco_de_imagens.lista.{category_id}.update.{id}');
        Route::get('banco_de_imagens/lista/{category_id}/editar/{id}', 'ImageBankListController@edit')->name('banco_de_imagens.lista.{category_id}.editar.{id}');
        Route::get('banco_de_imagens/lista/{category_id}/delete/{id}', 'ImageBankListController@destroy')->name('banco_de_imagens.lista.{category_id}.delete.{id}');

        // Rotas de Banners
        Route::get('banners', 'BannersController@index')->name('banners');
        Route::post('banners/reorder', 'BannersController@sortable')->name('banners.reorder');
        Route::post('banners/store', 'BannersController@store')->name('banners.store');
        Route::post('banners/update/{id}', 'BannersController@update')->name('banners.update.{id}');
        Route::get('banners/editar/{id}', 'BannersController@edit')->name('banners.editar.{id}');
        Route::get('banners/delete/{id}', 'BannersController@destroy')->name('banners.delete.{id}');

        // Rotas para Textos
        Route::get('textos', 'TextsController@index')->name('textos');
        Route::post('textos/store', 'TextsController@store')->name('textos.store');
        Route::post('textos/store/{id}', 'TextsController@store')->name('textos.store.{id}');

        // Rotas para Configurações
        Route::get('configuracoes', 'ConfigsController@index')->name('configuracoes');
        Route::post('configuracoes/store', 'ConfigsController@store')->name('configuracoes.store');
        Route::post('configuracoes/store/{id}', 'ConfigsController@store')->name('configuracoes.store.{id}');

        // Rotas de Newsletter
        Route::get('newsletter', 'NewsletterController@index')->name('newsletter');
        Route::get('newsletter/delete/{id}', 'NewsletterController@destroy')->name('newsletter.delete.{id}');

        // Rotas de Newsletter
        Route::get('clientes_download_arquivos', 'ClientsDownloadFilesController@index')->name('clientes_download_arquivos');
        Route::get('clientes_download_arquivos/editar/{id}', 'ClientsDownloadFilesController@edit')->name('clientes_download_arquivos.editar.{id}');
        Route::get('clientes_download_arquivos/delete/{id}', 'ClientsDownloadFilesController@destroy')->name('clientes_download_arquivos.delete.{id}');

        // Rotas do Blog
        Route::group(['namespace' => 'Blog', 'prefix' => 'blog', 'as' => 'blog.'], function () {
            // Rotas de Categorias
            Route::get('categorias', 'PostsCategoriesController@index')->name('categorias');
            Route::post('categorias/reorder', 'PostsCategoriesController@sortable')->name('categorias.reorder');
            Route::post('categorias/store', 'PostsCategoriesController@store')->name('categorias.store');
            Route::post('categorias/update/{id}', 'PostsCategoriesController@update')->name('categorias.update.{id}');
            Route::get('categorias/editar/{id}', 'PostsCategoriesController@edit')->name('categorias.editar.{id}');
            Route::get('categorias/delete/{id}', 'PostsCategoriesController@destroy')->name('categorias.delete.{id}');

            // Rotas de Posts
            Route::get('posts', 'PostsController@index')->name('posts');
            Route::post('posts/reorder', 'PostsController@sortable')->name('posts.reorder');
            Route::post('posts/store', 'PostsController@store')->name('posts.store');
            Route::post('posts/update/{id}', 'PostsController@update')->name('posts.update.{id}');
            Route::get('posts/editar/{id}', 'PostsController@edit')->name('posts.editar.{id}');
            Route::get('posts/delete/{id}', 'PostsController@destroy')->name('posts.delete.{id}');

            // Rotas de Comentários
            Route::get('posts/{post_id}/comentarios/', 'CommentsController@index')->name('posts.{post_id}.comentarios');
            Route::get('posts/{post_id}/comentarios/editar/{id}', 'CommentsController@edit')->name('posts.{post_id}.comentarios.editar.{id}');
            Route::get('posts/{post_id}/comentarios/delete/{id}', 'CommentsController@destroy')->name('posts.{post_id}.comentarios.delete.{id}');
            Route::post('posts/{post_id}/comentarios/update/{id}', 'CommentsController@update')->name('posts.{post_id}.comentarios.update.{id}');

            // Rotas de Respostas
            Route::get('posts/{post_id}/comentarios/{comment_id}/respostas', 'CommentAnswersController@index')->name('posts.{post_id}.comentarios.{comment_id}.respostas');
            Route::get('posts/{post_id}/comentarios/{comment_id}/respostas/editar/{id}', 'CommentAnswersController@edit')->name('posts.{post_id}.comentarios.{comment_id}.respostas.editar.{id}');
            Route::get('posts/{post_id}/comentarios/{comment_id}/respostas/delete/{id}', 'CommentAnswersController@destroy')->name('posts.{post_id}.comentarios.{comment_id}.respostas.delete.{id}');
            Route::post('posts/{post_id}/comentarios/{comment_id}/respostas/update/{id}', 'CommentAnswersController@update')->name('posts.{post_id}.comentarios.{comment_id}.respostas.update.{id}');

        });

        Route::group(['namespace' => 'Products', 'prefix' => 'produtos', 'as' => 'produtos.'], function () {

            // Rotas de Categorias
            Route::get('lista', 'ProductsController@index')->name('lista');
            Route::post('lista/reorder', 'ProductsController@sortable')->name('lista.reorder');
            Route::post('lista/store', 'ProductsController@store')->name('lista.store');
            Route::post('lista/update/{id}', 'ProductsController@update')->name('lista.update.{id}');
            Route::get('lista/editar/{id}', 'ProductsController@edit')->name('lista.editar.{id}');
            Route::get('lista/delete/{id}', 'ProductsController@destroy')->name('lista.delete.{id}');

            // Rotas Galeria
            Route::get('galeria/{id}', 'ProductGalleryController@index')->name('galeria.{id}');
            Route::post('galeria/store/{id}', 'ProductGalleryController@store')->name('galeria.store.{id}');
            Route::post('galeria/{id}/reorder', 'ProductGalleryController@sortable')->name('galeria.{id}.reorder');
            Route::get('galeria/delete/{id}', 'ProductGalleryController@destroy')->name('galeria.delete.{id}');
            Route::get('galeria/{id}/get_items_gallery', 'ProductGalleryController@getItemsGallery')->name('galeria.{id}.get_items_gallery');

            // Rotas de Sliders
            Route::get('videos/{id}', 'ProductsVideosController@index')->name('videos.{id}');
            Route::post('videos/reorder', 'ProductsVideosController@sortable')->name('videos.reorder');
            Route::post('videos/store/{id}', 'ProductsVideosController@store')->name('videos.store.{id}');
            Route::post('videos/update/{product_id}/{id}', 'ProductsVideosController@update')->name('videos.update.{product_id}.{id}');
            Route::get('videos/editar/{product_id}/{id}', 'ProductsVideosController@edit')->name('videos.editar.{product_id}.{id}');
            Route::get('videos/delete/{product_id}/{id}', 'ProductsVideosController@destroy')->name('videos.delete.{product_id}.{id}');

            // Rotas de Arquivos
            Route::get('arquivos', 'ProductsFilesController@index')->name('arquivos');
            Route::post('arquivos/reorder', 'ProductsFilesController@sortable')->name('arquivos.reorder');
            Route::post('arquivos/store', 'ProductsFilesController@store')->name('arquivos.store');
            Route::post('arquivos/update/{id}', 'ProductsFilesController@update')->name('arquivos.update.{id}');
            Route::get('arquivos/editar/{id}', 'ProductsFilesController@edit')->name('arquivos.editar.{id}');
            Route::get('arquivos/delete/{id}', 'ProductsFilesController@destroy')->name('arquivos.delete.{id}');

            // Rotas de Aplicações
            Route::get('aplicacoes', 'ProductsApplicationsController@index')->name('aplicacoes');
            Route::post('aplicacoes/reorder', 'ProductsApplicationsController@sortable')->name('aplicacoes.reorder');
            Route::post('aplicacoes/store', 'ProductsApplicationsController@store')->name('aplicacoes.store');
            Route::post('aplicacoes/update/{id}', 'ProductsApplicationsController@update')->name('aplicacoes.update.{id}');
            Route::get('aplicacoes/editar/{id}', 'ProductsApplicationsController@edit')->name('aplicacoes.editar.{id}');
            Route::get('aplicacoes/delete/{id}', 'ProductsApplicationsController@destroy')->name('aplicacoes.delete.{id}');

            // Rotas de Categorias
            Route::get('categorias', 'ProductTypeController@index')->name('categorias');
            Route::post('categorias/reorder', 'ProductTypeController@sortable')->name('categorias.reorder');
            Route::post('categorias/store', 'ProductTypeController@store')->name('categorias.store');
            Route::post('categorias/update/{id}', 'ProductTypeController@update')->name('categorias.update.{id}');
            Route::get('categorias/editar/{id}', 'ProductTypeController@edit')->name('categorias.editar.{id}');
            Route::get('categorias/delete/{id}', 'ProductTypeController@destroy')->name('categorias.delete.{id}');
            
            // Rotas de SubCategorias
            Route::get('subcategorias', 'ProductFamilyController@index')->name('subcategorias');
            Route::post('subcategorias/reorder', 'ProductFamilyController@sortable')->name('subcategorias.reorder');
            Route::post('subcategorias/store', 'ProductFamilyController@store')->name('subcategorias.store');
            Route::post('subcategorias/update/{id}', 'ProductFamilyController@update')->name('subcategorias.update.{id}');
            Route::get('subcategorias/editar/{id}', 'ProductFamilyController@edit')->name('subcategorias.editar.{id}');
            Route::get('subcategorias/delete/{id}', 'ProductFamilyController@destroy')->name('subcategorias.delete.{id}');

            // Rotas Subcategorias Galeria
            Route::get('subcategorias/galeria/{id}', 'ProductFamilyGalleryController@index')->name('subcategorias.galeria.{id}');
            Route::post('subcategorias/galeria/store/{id}', 'ProductFamilyGalleryController@store')->name('subcategorias.galeria.store.{id}');
            Route::post('subcategorias/galeria/{id}/reorder', 'ProductFamilyGalleryController@sortable')->name('subcategorias.galeria.{id}.reorder');
            Route::get('subcategorias/galeria/delete/{id}', 'ProductFamilyGalleryController@destroy')->name('subcategorias.galeria.delete.{id}');
            Route::get('subcategorias/galeria/{id}/get_items_gallery', 'ProductFamilyGalleryController@getItemsGallery')->name('subcategorias.galeria.{id}.get_items_gallery');

        });
        Route::group(['namespace' => 'Markets', 'prefix' => 'mercados', 'as' => 'mercados.'], function () {
            // Rotas de Mercado
            Route::get('categorias', 'MarketsController@index')->name('categorias');
            Route::post('categorias/reorder', 'MarketsController@sortable')->name('categorias.reorder');
            Route::post('categorias/store', 'MarketsController@store')->name('categorias.store');
            Route::post('categorias/update/{id}', 'MarketsController@update')->name('categorias.update.{id}');
            Route::get('categorias/editar/{id}', 'MarketsController@edit')->name('categorias.editar.{id}');
            Route::get('categorias/delete/{id}', 'MarketsController@destroy')->name('categorias.delete.{id}');

            // Rotas Galeria
            Route::get('galeria/{id}', 'MarketsGalleryController@index')->name('galeria.{id}');
            Route::post('galeria/store/{id}', 'MarketsGalleryController@store')->name('galeria.store.{id}');
            Route::post('galeria/{id}/reorder', 'MarketsGalleryController@sortable')->name('galeria.{id}.reorder');
            Route::get('galeria/delete/{id}', 'MarketsGalleryController@destroy')->name('galeria.delete.{id}');
            Route::get('galeria/{id}/get_items_gallery', 'MarketsGalleryController@getItemsGallery')->name('galeria.{id}.get_items_gallery');

            // Rotas de Arquivos
            Route::get('arquivos', 'MarketsFilesController@index')->name('arquivos');
            Route::post('arquivos/reorder', 'MarketsFilesController@sortable')->name('arquivos.reorder');
            Route::post('arquivos/store', 'MarketsFilesController@store')->name('arquivos.store');
            Route::post('arquivos/update/{id}', 'MarketsFilesController@update')->name('arquivos.update.{id}');
            Route::get('arquivos/editar/{id}', 'MarketsFilesController@edit')->name('arquivos.editar.{id}');
            Route::get('arquivos/delete/{id}', 'MarketsFilesController@destroy')->name('arquivos.delete.{id}');
        });
        Route::group(['namespace' => 'Marks', 'prefix' => 'marcas', 'as' => 'marcas.'], function () {
            // Rotas de Mercado
            Route::get('lista', 'MarksController@index')->name('lista');
            Route::post('lista/reorder', 'MarksController@sortable')->name('lista.reorder');
            Route::post('lista/store', 'MarksController@store')->name('lista.store');
            Route::post('lista/update/{id}', 'MarksController@update')->name('lista.update.{id}');
            Route::get('lista/editar/{id}', 'MarksController@edit')->name('lista.editar.{id}');
            Route::get('lista/delete/{id}', 'MarksController@destroy')->name('lista.delete.{id}');

            // Rotas Galeria
            Route::get('galeria/{id}', 'MarksGalleryController@index')->name('galeria.{id}');
            Route::post('galeria/store/{id}', 'MarksGalleryController@store')->name('galeria.store.{id}');
            Route::post('galeria/{id}/reorder', 'MarksGalleryController@sortable')->name('galeria.{id}.reorder');
            Route::get('galeria/delete/{id}', 'MarksGalleryController@destroy')->name('galeria.delete.{id}');
            Route::get('galeria/{id}/get_items_gallery', 'MarksGalleryController@getItemsGallery')->name('galeria.{id}.get_items_gallery');
        });
    });
});
Route::get('{locale}', 'HomeController@index')->name('home');
Route::get('{locale}/institucional', 'InstitucionalController@index')->name('{locale}.institucional');
Route::get('{locale}/catalogo', 'CatalogoController@index')->name('{locale}.catalogo');
Route::get('{locale}/certificacoes', 'CertificacoesController@index')->name('{locale}.certificacoes');
Route::get('{locale}/responsabilidade_social', 'ResponsabilidadeSocialController@index')->name('{locale}.responsabilidade_social');
Route::get('{locale}/responsabilidade_social/{slug}', 'ResponsabilidadeSocialController@projects')->name('{locale}.responsabilidade_social.{slug}');
Route::get('{locale}/nossas_marcas/{slug}', 'NossasMarcasController@index')->name('{locale}.nossas_marcas.{slug}');

Route::get('{locale}/parceria', 'ParceriaController@index')->name('{locale}.parceria');

Route::get('{locale}/produtos_sob_medida', 'ProdutosSobMedidaController@index')->name('{locale}.produtos_sob_medida');
Route::post('{locale}/produtos_sob_medida/send_solution', 'ProdutosSobMedidaController@sendForm')->name('{locale}.produtos_sob_medida.send_solution');

Route::get('{locale}/contato', 'ContatoController@index')->name('{locale}.contato');
Route::post('{locale}/contato/send', 'ContatoController@send')->name('{locale}.contato.send');

// PERGUNTAS FREQUENTES
Route::get('{locale}/faq', 'FaqController@index')->name('{locale}.faq');
Route::get('{locale}/faq/get_list_market/{market_id}', 'FaqController@get_list_market')->name('{locale}.perguntas_frequentes.{get_list_market}.{market_id}');
Route::get('{locale}/faq/get_list_type/{type_id}', 'FaqController@get_list_type')->name('{locale}.perguntas_frequentes.{get_list_type}.{type_id}');

// Soluções
Route::get('{locale}/solucoes/{category_slug}/{subcategory_slug}', 'ProdutosSubcategoriasController@index')->name('{locale}.solucoes.{category_slug}.{subcategory_slug}');
Route::get('{locale}/solucoes/{category_slug}/{subcategory_slug}/{product_slug}', 'ProdutosController@index')->name('{locale}.solucoes.{category_slug}.{subcategory_slug}.{product_slug}');

// Mercados
Route::get('{locale}/mercados/{category_slug}', 'MercadosCategoriasController@index')->name('{locale}.mercados.{category_slug}');
Route::get('{locale}/mercados/{category_slug}/{subcategory_slug}', 'MercadosSubcategoriasController@index')->name('{locale}.mercados.{category_slug}.{subcategory_slug}');
Route::get('{locale}/mercados/{category_slug}/{subcategory_slug}/{product_slug}', 'MercadosProdutosController@index')->name('{locale}.mercados.{category_slug}.{subcategory_slug}.{product_slug}');

// BLOG
Route::get('{locale}/blog', 'BlogController@index')->name('{locale}.blog');
Route::get('{locale}/blog/{category_slug}', 'BlogController@index')->name('{locale}.blog.{category_slug}');

Route::get('{locale}/post/{post_slug}', 'BlogPostController@index')->name('{locale}.blog.{post_slug}');
Route::post('{locale}/post/{post_slug}/send_comment', 'BlogPostController@sendComment')->name('{locale}.post.{post_slug}.send_comment');
Route::post('{locale}/post/{post_slug}/send_answer', 'BlogPostController@sendAnswer')->name('{locale}.post.{post_slug}.send_answer');

// BANCO DE IMAGENS
Route::get('{locale}/banco_de_imagens', 'BancoImagensController@index')->name('{locale}.banco_de_imagens');
Route::get('{locale}/banco_de_imagens/{category_slug}', 'BancoImagensController@list')->name('{locale}.banco_de_imagens.{category_slug}');

// OUVIDORIA
Route::get('{locale}/ouvidoria', 'OuvidoriaController@index')->name('{locale}.ouvidoria');
Route::get('{locale}/ouvidoria/send', 'OuvidoriaController@send')->name('{locale}.ouvidoria.send');

// NEWSLETTER
Route::post('{locale}/newsletter', 'NewsletterController@save')->name('{locale}.newsletter');
Route::post('{locale}/save_user_download_file', 'FormsController@saveUserDownloadFile')->name('{locale}.save_user_download_file');

