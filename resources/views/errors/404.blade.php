@extends('front.layout.master')

@section('header_title', 'Página não encontrada')

@section('main-content')
    <main id="main" class="page-faq">

        <section id="page-header" class="red">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i>
                </div>
                <h1 class="page-title">
                    Página não encontrada
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container" id="vue-faq">

                <div class="row">
                    @include('front/layout/partials/sidebar-single')
                    <div class="col-sm-9 content">

                        <div class="box">
                            <h2 class="content-title h2 text-center">OPS! Página não encontrada!</h2>
                        </div>

                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->

@endsection
