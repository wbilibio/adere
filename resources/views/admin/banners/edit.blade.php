@extends('admin.layouts.app')

@section('htmlheader_title','Banner Home')
@section('contentheader_title','Banner Home')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li class="active"><a href="{{ route('admin.banners') }}">Banner Home</a></li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Editar Campos</h3>
				</div>
				<form action="{{ route('admin.banners.update.{id}', array($item->id)) }}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="col-xs-4">
							<div class="form-group">
								<label for="image">Imagem:</label>
								<div class="upload">
									<div class="btn btn-primary">Atualizar Imagem</div>
									<input type="file" id="image" name="file_image" />
									@if (!empty($item->id))
										(<a href="{{ asset('_files/banners/' . $item->image) }}" title="ver imagem atual" target="_blank">Ver Imagem Atual</a>)
									@endif
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<label for="link">Link:</label>
								<input type="text" class="form-control" id="link" name="link" value="{{ empty($item->link) ? old('link') : $item->link}}" />
							</div>
						</div>
						<div class="col-xs-12">
							<div class="form-group">
								<label for="type">Selecione a sessão do site:</label>
								<select id="type" name="type" class="form-control">
									<optoin value="">Selecione a sessão</optoin>
									<option value="home-left" {{ ($item->type == 'home-left') ? 'selected' : '' }}>Home esquerdo</option>
									<option value="home-right" {{ ($item->type == 'home-right') ? 'selected' : '' }}>Home direito</option>
									<option value="about" {{ ($item->type == 'about') ? 'selected' : '' }}>Sobre a empresa</option>
									<option value="solution-left" {{ ($item->type == 'solution-left') ? 'selected' : '' }}>Solução esquerdo</option>
									<option value="solution-center" {{ ($item->type == 'solution-center') ? 'selected' : '' }}>Solução direito</option>
									<option value="product-left" {{ ($item->type == 'product-left') ? 'selected' : '' }}>Produto esquerdo</option>
									<option value="product-center" {{ ($item->type == 'product-center') ? 'selected' : '' }}>Produto centro</option>
									<option value="menu-solution" {{ ($item->type == 'menu-solution') ? 'selected' : '' }}>Menu solução</option>
									<option value="menu-market" {{ ($item->type == 'menu-market') ? 'selected' : '' }}>Menu mercado</option>
									<option value="market-left" {{ ($item->type == 'market-left') ? 'selected' : '' }}>Mercado lateral</option>
									<option value="market-center" {{ ($item->type == 'market-center') ? 'selected' : '' }}>Mercado centro</option>
									<option value="blog" {{ ($item->type == 'blog') ? 'selected' : '' }}>Blog</option>
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="box-footer">
						<button class="btn btn-primary btn-send pull-right">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
