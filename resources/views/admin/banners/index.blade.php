@extends('admin.layouts.app')

@section('htmlheader_title','Banner')
@section('contentheader_title','Banner')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li class="active">Banners Home</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">

					<div class="alert alert-success alert-dismissible" v-show="msgSuccessReorder" v-cloak>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> @{{ msgSuccess }}</h4>
					</div>

					<vue-table-list :items-list="items_list" urls='{"reorder": "/cms/banners/reorder","image":"/_files/banners/","delete":"/cms/banners/delete/","edit":"/cms/banners/editar/"}'></vue-table-list>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Adicionar Banner</h3>
				</div>
				<form action="{{ route('admin.banners.store') }}" class="form-table-list" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<label for="image">Imagem:</label>
									<div class="upload">
										<div class="btn btn-primary">Adicionar Imagem</div>
										<input type="file" id="image" name="file_image" />
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="form-group">
									<label for="link">Link:</label>
									<input type="text" id="link" name="link" class="form-control" />
								</div>
							</div>
							<div class="col-xs-12">
								<div class="form-group">
									<label for="type">Selecione a sessão do site para deixar o banner visível:</label>
									<select id="type" name="type" class="form-control" required>
										<option value="">Seleciona uma sessão</option>
										<option value="home-left">Home - esquerdo</option>
										<option value="home-right">Home - direito</option>
										<option value="about">Sobre a empresa - lateral</option>
										<option value="solution-left">Solução Subcategoria - esquerdo</option>
										<option value="solution-center">Solução Subcategoria - centro</option>
										<option value="product-left">Produto - esquerdo</option>
										<option value="product-center">Produto - centro</option>
										<option value="menu-solution">Menu solução</option>
										<option value="menu-market">Menu mercado</option>
										<option value="market-left">Mercado - lateral</option>
										<option value="market-center">Mercado - centro</option>
										<option value="blog">Blog</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<button class="btn btn-primary btn-send pull-right">Salvar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
