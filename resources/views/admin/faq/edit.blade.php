@extends('admin.layouts.app')

@section('htmlheader_title','Slider Home')
@section('contentheader_title','Slider Home')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><i class="fa fa-building-o"></i> Empresa</li>
		<li class="active"><a href="{{ route('admin.sliders') }}">Slider Home</a></li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Editar Campos</h3>
				</div>
				<form action="{{ route('admin.faq.update.{id}', array($item->id)) }}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="col-xs-6">
							<div class="checkbox checklist">
								<p><b>Relacione suas perguntas com soluções:</b></p>
								<div class="scrolling">
									@foreach($items_product_types as $item_product_type)
										<label><input type="checkbox" name="list_types[]" value="{{ $item_product_type->id }}" {{ ($item_product_type->select) ? 'checked' : '' }}> {{ $item_product_type->title }}</label>
									@endforeach
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="checkbox checklist">
								<p><b>Relacione suas perguntas com mercados:</b></p>
								<div class="scrolling">
									@foreach($items_markets as $item_market)
										<label><input type="checkbox" name="list_markets[]" value="{{ $item_market->id }}" {{ ($item_market->select) ? 'checked' : '' }}> {{ $item_market->title }}</label>
									@endforeach
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="checkbox">
								<p>Status:</p>
								<label><input type="checkbox" v-model="visible_select"><span v-cloak>@{{ (visible_select) ? 'Ativo' : 'Inativo' }}</span></label>
							</div>
						</div>
						<div class="clearfix"></div>
						@foreach(['pt','en','es'] as $locale)
							<div class="col-md-4">
								<!-- DIRECT CHAT SUCCESS -->
								<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success">
									<div class="box-header with-border">
										<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="fa fa-minus"></i>
											</button>
										</div>
									</div>
									<!-- /.box-header -->
									<div class="box-body">
										<div class="col-xs-12">
											<div class="form-group">
												<label>Título:</label>
												<input type="text" name="title_{{$locale}}" class="form-control" value="{{ empty($item->getTranslation($locale)->title) ? old('title') : $item->getTranslation($locale)->title}}">
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group">
												<label>Descrição:</label>
												<textarea name="text_{{$locale}}" class="form-control">{{ empty($item->getTranslation($locale)->title) ? old('text') : $item->getTranslation($locale)->text}}</textarea>
											</div>
										</div>
									</div>
								</div>
								<!--/.direct-chat -->
							</div>
							<!-- /.col -->
						@endforeach
					</div>
					<div class="box-footer">
						<input type="hidden" name="status" :value="status" />
						<button class="btn btn-primary btn-send pull-right">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
