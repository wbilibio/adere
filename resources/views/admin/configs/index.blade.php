@extends('admin.layouts.app')

@section('htmlheader_title','Dados gerais do Site')
@section('contentheader_title','Dados gerais do Site')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li>Geral</li>
		<li class="active">Dados gerais do Site</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-info">

				<form action="{{ empty($item->id) ? route('admin.configuracoes.store') : route('admin.configuracoes.store.{id}', array($item->id)) }}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="box-header">
							<h3 class="box-title">Dados de Contato</h3>
						</div>
						<div class="col-xs-3">
							<div class="form-group">
								<label>Telefone Adere no Mundo:</label>
								<input type="text" name="phone_world" class="form-control" value="{{ empty($item->phone_world) ? old('phone_world') : $item->phone_world }}" />
							</div>
						</div>
						<div class="col-xs-3">
							<div class="form-group">
								<label>Telefone SAC:</label>
								<input type="text" name="phone_sac" class="form-control" value="{{ empty($item->phone_sac) ? old('phone_sac') : $item->phone_sac }}" />
							</div>
						</div>
						<div class="col-xs-3">
							<div class="form-group">
								<label>Telefone PABX:</label>
								<input type="text" name="phone_pabx" class="form-control" value="{{ empty($item->phone_pabx) ? old('phone_pabx') : $item->phone_pabx }}" />
							</div>
						</div>
						<div class="col-xs-3">
							<div class="form-group">
								<label>Telefone FAX:</label>
								<input type="text" name="phone_fax" class="form-control" value="{{ empty($item->phone_fax) ? old('phone_fax') : $item->phone_fax }}" />
							</div>
						</div>
						<div class="col-xs-3">
							<div class="form-group">
								<label>Horários de Atendimento:</label>
								<textarea name="office_hours" class="form-control">{{ empty($item->office_hours) ? old('office_hours') : $item->office_hours }}</textarea>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<label>Endereço:</label>
								<textarea name="address" class="form-control tinymce">{{ empty($item->address) ? old('address') : $item->address }}</textarea>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="box-header">
							<h3 class="box-title">Link Externos</h3>
						</div>
						<div class="col-xs-3">
							<div class="form-group">
								<label>Link Loja:</label>
								<input type="text" name="link_store" class="form-control" value="{{ empty($item->link_store) ? old('link_store') : $item->link_store }}" />
							</div>
						</div>
						<div class="col-xs-3">
							<div class="form-group">
								<label>Link 2 via de Boletos:</label>
								<input type="text" name="link_2_via_boletos" class="form-control" value="{{ empty($item->link_2_via_boletos) ? old('link_2_via_boletos') : $item->link_2_via_boletos }}" />
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="box-header">
							<h3 class="box-title">Redes Sociais</h3>
						</div>
						<div class="col-xs-3">
							<div class="form-group">
								<label>Link Facebook:</label>
								<input type="text" name="link_facebook" class="form-control" value="{{ empty($item->link_facebook) ? old('link_facebook') : $item->link_facebook }}" />
							</div>
						</div>
						<div class="col-xs-3">
							<div class="form-group">
								<label>Link Twitter:</label>
								<input type="text" name="link_twitter" class="form-control" value="{{ empty($item->link_twitter) ? old('link_twitter') : $item->link_twitter }}" />
							</div>
						</div>
						<div class="col-xs-3">
							<div class="form-group">
								<label>Link Linkedin:</label>
								<input type="text" name="link_linkedin" class="form-control" value="{{ empty($item->link_linkedin) ? old('link_linkedin') : $item->link_linkedin }}" />
							</div>
						</div>
						<div class="col-xs-3">
							<div class="form-group">
								<label>Link Google Plus:</label>
								<input type="text" name="link_google_plus" class="form-control" value="{{ empty($item->link_google_plus ) ? old('link_google_plus') : $item->link_google_plus  }}" />
							</div>
						</div>
						<div class="col-xs-3">
							<div class="form-group">
								<label>Link Canal no Youtube:</label>
								<input type="text" name="link_youtube" class="form-control" value="{{ empty($item->link_youtube ) ? old('link_youtube') : $item->link_youtube  }}" />
							</div>
						</div>
						<div class="clearfix"></div>

						<div class="box-header">
							<h3 class="box-title">SEO</h3>
						</div>
						<div class="col-xs-3">
							<div class="form-group">
								<label>Google Analytics:</label>
								<textarea name="google_analytics" class="form-control" rows="10">{{ empty($item->google_analytics) ? old('google_analytics') : $item->google_analytics }}</textarea>
							</div>
						</div>

						<div class="clearfix"></div>
						<div class="col-md-12">
							<button class="btn btn-primary btn-send pull-right">Salvar</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
@endsection
