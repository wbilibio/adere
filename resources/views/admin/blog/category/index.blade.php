@extends('admin.layouts.app')

@section('htmlheader_title','Blog - Categorias')
@section('contentheader_title','Blog - Categorias')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-commenting"></i> Blog</a></li>
		<li class="active">Categorias</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">

					<div class="alert alert-success alert-dismissible" v-show="msgSuccessReorder" v-cloak>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> @{{ msgSuccess }}</h4>
					</div>

					<vue-table-list :items-list="items_list" urls='{"reorder": "/cms/blog/categorias/reorder","image":"false","delete":"/cms/blog/categorias/delete/","edit":"/cms/blog/categorias/editar/"}'></vue-table-list>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Adicionar Categorias</h3>
				</div>
				<form action="{{ route('admin.blog.categorias.store') }}" class="form-table-list" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="row">
							@foreach(['pt','en','es'] as $key=>$locale)
								<div class="col-md-12">
									<!-- DIRECT CHAT SUCCESS -->
									<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success {{ $key == 0 ? '' : 'collapsed-box'}}">
										<div class="box-header with-border">
											<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa {{ $key == 0 ? 'fa-minus' : 'fa-plus'}}"></i>
												</button>
											</div>
										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<div class="col-xs-12">
												<div class="form-group">
													<label>Título:</label>
													<input type="text" name="title_{{$locale}}" class="form-control">
												</div>
											</div>
										</div>
									</div>
									<!--/.direct-chat -->
								</div>
								<!-- /.col -->
							@endforeach
						</div>
						<div class="col-md-12">
							<button class="btn btn-primary btn-send pull-right">Salvar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
