@extends('admin.layouts.app')

@section('htmlheader_title','Blog - Categorias')
@section('contentheader_title','Blog - Categorias')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><i class="fa fa-commenting"></i> Blog</li>
		<li class="active"><a href="{{ route('admin.blog.categorias') }}">Categorias</a></li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Editar Campos</h3>
				</div>
				<form action="{{ route('admin.blog.categorias.update.{id}', array($item->id)) }}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						@foreach(['pt','en','es'] as $locale)
							<div class="col-md-4">
								<!-- DIRECT CHAT SUCCESS -->
								<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success">
									<div class="box-header with-border">
										<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="fa fa-minus"></i>
											</button>
										</div>
									</div>
									<!-- /.box-header -->
									<div class="box-body">
										<div class="col-xs-12">
											<div class="form-group">
												<label>Título:</label>
												<input type="text" name="title_{{$locale}}" class="form-control" value="{{ empty($item->getTranslation($locale)->title) ? old('title') : $item->getTranslation($locale)->title}}">
											</div>
										</div>
									</div>
								</div>
								<!--/.direct-chat -->
							</div>
							<!-- /.col -->
						@endforeach
					</div>
					<div class="box-footer">
						<button class="btn btn-primary btn-send pull-right">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
