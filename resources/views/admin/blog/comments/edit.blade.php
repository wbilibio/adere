@extends('admin.layouts.app')

@section('htmlheader_title','Blog - Comentários')
@section('contentheader_title','Blog - '. $item_post->title .'- Comentários')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="{{ url('cms/blog/posts/') }}"><i class="fa fa-commenting"></i> Blog</a></li>
		<li class="active">Comentários</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Editar Campos</h3>
				</div>
				<form action="{{ route('admin.blog.posts.{post_id}.comentarios.update.{id}', array($item_post->id,$item->id)) }}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="col-xs-12">
							<div class="form-group">
								<label>Título:</label>
								<input type="text" name="title" class="form-control" value="{{ empty($item->title) ? old('title') : $item->title }}" readonly>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="form-group">
								<label>E-mail:</label>
								<input type="text" name="email" class="form-control" value="{{ empty($item->email) ? old('email') : $item->email }}" readonly>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="form-group">
								<label>Descrição:</label>
								<textarea name="description" class="form-control" readonly>{{ empty($item->text) ? old('text') : $item->text }}</textarea>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="checkbox">
								<p>Aprovar comentário:</p>
								<label><input type="checkbox" v-model="visible_select"><span v-cloak>@{{ (visible_select) ? 'Ativo' : 'Inativo' }}</span></label>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<input type="hidden" name="status" :value="status" />
						<button class="btn btn-primary btn-send pull-right">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
