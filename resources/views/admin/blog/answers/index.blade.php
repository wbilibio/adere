@extends('admin.layouts.app')

@section('htmlheader_title','Blog - Respostas')
@section('contentheader_title','Blog > '.$item_post->title.' > '.$item_comment->title.' > Respostas')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="{{ url('cms/blog/posts/') }}"><i class="fa fa-commenting"></i> Blog</a></li>
		<li>Comentários</li>
		<li class="active">Respostas</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">

					<div class="alert alert-success alert-dismissible" v-show="msgSuccessReorder" v-cloak>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> @{{ msgSuccess }}</h4>
					</div>

					<vue-table-list :items-list="items_list" urls='{"reorder": "false","image":"false","delete":"/cms/blog/posts/{{$item_post->id}}/comentarios/{{$item_comment->id}}/respostas/delete/","edit":"/cms/blog/posts/{{$item_post->id}}/comentarios/{{$item_comment->id}}/respostas/editar/"}'></vue-table-list>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
@endsection
