@extends('admin.layouts.app')

@section('htmlheader_title','Blog - Posts')
@section('contentheader_title','Blog - Posts')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-commenting"></i> Blog</a></li>
		<li class="active">Posts</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<div class="vue-table" id="vue-tables-options">
						<div class="col-xs-12">
							<v-client-table :data="blogData" :columns="blogColumns" :options="blogOptions"></v-client-table>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Adicionar Post</h3>
				</div>

				<form action="{{ route('admin.blog.posts.store') }}" class="form-table-list" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<label for="image">Imagem:</label>
									<div class="upload">
										<div class="btn btn-primary">Adicionar Imagem</div>
										<input type="file" id="image" name="file_image" />
									</div>
								</div>
							</div>
{{--							<div class="col-xs-12">
								<div class="form-group">
									<label>Categoria:</label>
									<select name="category_id" class="form-control" required>
										<option value="">Selecione uma categoria</option>
										@foreach($items_categories as $item_category)
											<option value="{{ $item_category->id }}">{{ $item_category->getTranslation('pt')->title }}</option>
										@endforeach
									</select>
								</div>
							</div>--}}
							<div class="col-xs-12">
								<div class="checkbox checklist">
									<p>Selecione familias relacionadas:</p>
									<div class="scrolling">
										@foreach($items_families as $item_family)
											<label><input type="checkbox" name="checklist[]" value="{{ $item_family->id }}"> {{ $item_family->title }}</label>
										@endforeach
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="form-group">
									<label>Adicione tags:</label>
									<tags-input-field :on-change="callbackMethod"></tags-input-field>
								</div>
							</div>
							@foreach(['pt','en','es'] as $key=>$locale)
								<div class="col-md-12">
									<!-- DIRECT CHAT SUCCESS -->
									<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success {{ $key == 0 ? '' : 'collapsed-box'}}">
										<div class="box-header with-border">
											<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa {{ $key == 0 ? 'fa-minus' : 'fa-plus'}}"></i>
												</button>
											</div>
										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<div class="col-xs-12">
												<div class="form-group">
													<label>Título:</label>
													<input type="text" name="title_{{$locale}}" class="form-control">
												</div>
											</div>
											<div class="col-xs-12">
												<div class="form-group">
													<label>Descrição:</label>
													<div class="content-editor">
														<div class="btn {{($locale == 'pt' ? 'btn-success' : ($locale == 'en' ? 'btn-danger' : ($locale == 'es' ? 'btn-warning' : ''))) }} open-editor">Adicionar Descrição</div>
														<div class="modal-editor">
															<div class="overlay-editor"></div>
															<div class="container-editor">
																<div class="close-editor">
																	<span class="fa-stack fa-lg">
																	  <i class="fa fa-circle fa-stack-2x"></i>
																	  <i class="fa fa-close fa-stack-1x fa-inverse"></i>
																	</span>
																</div>
																<textarea class="tinymce" name="text_{{$locale}}" id="text_gallery" cols="30" rows="10"></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--/.direct-chat -->
								</div>
								<!-- /.col -->
							@endforeach
						</div>
						<div class="col-md-12">
							<input type="hidden" name="tags" v-model="input_tags" />
							<button class="btn btn-primary btn-send pull-right">Salvar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
