@extends('admin.layouts.app')

@section('htmlheader_title','Blog - Post')
@section('contentheader_title','Blog - Post')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><i class="fa fa-commenting"></i> Blog</li>
		<li class="active"><a href="{{ route('admin.blog.posts') }}">Post</a></li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Editar Campos</h3>
				</div>
				<form action="{{ route('admin.blog.posts.update.{id}', array($item->id)) }}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="col-xs-4">
							<div class="form-group">
								<label for="image">Imagem:</label>
								<div class="upload">
									<div class="btn btn-primary">Adicionar Imagem</div>
									<input type="file" id="image" name="file_image" />
									@if (!empty($item->id))
										(<a href="{{ asset('_files/posts/' . $item->image) }}" title="ver imagem atual" target="_blank">Ver Imagem Atual</a>)
									@endif
								</div>
							</div>
						</div>
{{--						<div class="col-xs-4">
							<div class="form-group">
								<label>Categoria:</label>
								<select name="category_id" class="form-control" required>
									<option value="">Selecione uma categoria</option>
									@foreach($items_categories as $item_category)
										<option value="{{ $item_category->id }}" {{ ($item_category->id == $item->category_id) ? 'selected' : '' }}>{{ $item_category->getTranslation('pt')->title }}</option>
									@endforeach
								</select>
							</div>
						</div>--}}
						<div class="col-xs-4">
							<div class="form-group">
								<label>Adicione tags:</label>
								<tags-input-field :on-change="callbackMethod" :tags='getTags'></tags-input-field>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="checkbox checklist">
								<p>Selecione produtos relacionados:</p>
								<div class="scrolling">
									@foreach($items_families as $item_family)
										<label><input type="checkbox" name="checklist[]" value="{{ $item_family->id }}" {{ ($item_family->select) ? 'checked' : '' }}> {{ $item_family->title }}</label>
									@endforeach
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						@foreach(['pt','en','es'] as $locale)
							<div class="col-md-4">
								<!-- DIRECT CHAT SUCCESS -->
								<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success">
									<div class="box-header with-border">
										<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="fa fa-minus"></i>
											</button>
										</div>
									</div>
									<!-- /.box-header -->
									<div class="box-body">
										<div class="col-xs-12">
											<div class="form-group">
												<label>Título:</label>
												<input type="text" name="title_{{$locale}}" class="form-control" value="{{ empty($item->getTranslation($locale)->title) ? old('title') : $item->getTranslation($locale)->title}}">
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group">
												<label>Descrição:</label>
												<textarea name="text_{{$locale}}" class="form-control tinymce">{{ empty($item->getTranslation($locale)->text) ? old('text') : $item->getTranslation($locale)->text}}</textarea>
											</div>
										</div>
									</div>
								</div>
								<!--/.direct-chat -->
							</div>
							<!-- /.col -->
						@endforeach
					</div>
					<div class="box-footer">
						<input type="hidden" name="tags" v-model="input_tags" />
						<button class="btn btn-primary btn-send pull-right">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
