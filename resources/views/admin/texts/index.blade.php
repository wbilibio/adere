@extends('admin.layouts.app')

@section('htmlheader_title','Textos do Site')
@section('contentheader_title','Textos do Site')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li>Geral</li>
		<li class="active">Textos do Site</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-md-12">
			<form action="{{ empty($item->id) ? route('admin.textos.store') : route('admin.textos.store.{id}', array($item->id)) }}" method="POST" enctype="multipart/form-data">
				{!! csrf_field() !!}
				@foreach(['pt','en','es'] as $locale)
					<div class="col-md-4">
						<!-- DIRECT CHAT SUCCESS -->
						<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success">
							<div class="box-header with-border">
								<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Descrição Mercado de Atuação:</label>
										<textarea name="text_market_of_action_{{$locale}}" class="form-control">{{ empty($item) ? old('text_market_of_action') : $item->getTranslation($locale)->text_market_of_action }}</textarea>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label>Descrição Nossas Marcas:</label>
										<textarea name="text_our_brands_{{$locale}}" class="form-control">{{ empty($item) ? old('text_our_brands') : $item->getTranslation($locale)->text_our_brands }}</textarea>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label>Descrição Produtos mais buscados:</label>
										<textarea name="text_most_wanted_products_{{$locale}}" class="form-control">{{ empty($item) ? old('text_most_wanted_products') : $item->getTranslation($locale)->text_most_wanted_products }}</textarea>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label>Descrição de Produtos sob medida:</label>
										<textarea name="text_custom_product_{{$locale}}" class="form-control">{{ empty($item) ? old('text_custom_product') : $item->getTranslation($locale)->text_custom_product }}</textarea>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label>Descrição de Adere no Mundo:</label>
										<textarea name="text_company_in_the_world_{{$locale}}" class="form-control">{{ empty($item) ? old('text_company_in_the_world') : $item->getTranslation($locale)->text_company_in_the_world }}</textarea>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label>Descrição de Últimos posts:</label>
										<textarea name="text_latest_posts_{{$locale}}" class="form-control">{{ empty($item) ? old('text_latest_posts') : $item->getTranslation($locale)->text_latest_posts }}</textarea>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label>Descrição de Ouvidoria:</label>
										<textarea name="text_ombudsman_{{$locale}}" class="form-control">{{ empty($item) ? old('text_ombudsman') : $item->getTranslation($locale)->text_ombudsman }}</textarea>
									</div>
								</div>
							</div>
						</div>
						<!--/.direct-chat -->
					</div>
					<!-- /.col -->
				@endforeach
				<div class="col-md-12">
					<button class="btn btn-primary btn-send pull-right">Salvar</button>
				</div>
			</form>
		</div>
	</div>
@endsection
