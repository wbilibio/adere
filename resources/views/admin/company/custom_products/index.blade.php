@extends('admin.layouts.app')

@section('htmlheader_title','Produtos Sob Medida')
@section('contentheader_title','Produtos Sob Medida')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-building-o"></i> Empresa</a></li>
		<li class="active">Produtos Sob Medida</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li :class='isHash("#tab_1")'><a href="#tab_1" data-toggle="tab"><i class="fa fa-file-text-o"></i> Título e textos</a></li>
					<li :class='isHash("#tab_2")'><a href="#tab_2" data-toggle="tab"><i class="fa fa-image"></i> Galeria de Imagens</a></li>
				</ul>
				<div class="tab-content bg-gray color-palette">
					<div class="tab-pane" :class='isHash("#tab_1")' id="tab_1">
						<div class="row">
							<form action="{{ empty($item->id) ? route('admin.empresa.produtos_sob_medida.store') : route('admin.empresa.produtos_sob_medida.store.{id}', array($item->id)) }}" method="POST" enctype="multipart/form-data">
								{!! csrf_field() !!}
								@foreach(['pt','en','es'] as $locale)
									<div class="col-md-4">
										<!-- DIRECT CHAT SUCCESS -->
										<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success">
											<div class="box-header with-border">
												<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
												<div class="box-tools pull-right">
													<button type="button" class="btn btn-box-tool" data-widget="collapse">
														<i class="fa fa-minus"></i>
													</button>
												</div>
											</div>
											<!-- /.box-header -->
											<div class="box-body">
												<div class="col-xs-12">
													<div class="form-group">
														<label>Título:</label>
														<input type="text" name="title_{{$locale}}" class="form-control" value="{{ empty($item) ? old('title') : $item->getTranslation($locale)->title}}">
													</div>
												</div>
												<div class="col-xs-12">
													<div class="form-group">
														<label>Descrição:</label>
														<textarea name="text_{{$locale}}" class="form-control tinymce">{{ empty($item) ? old('text') : $item->getTranslation($locale)->text}}</textarea>
													</div>
												</div>
												<div class="col-xs-12">
													<div class="form-group">
														<label>Descrição de soluções:</label>
														<textarea name="text_solution_{{$locale}}" class="form-control">{{ empty($item) ? old('text_solution') : $item->getTranslation($locale)->text_solution}}</textarea>
													</div>
												</div>
											</div>
										</div>
										<!--/.direct-chat -->
									</div>
									<!-- /.col -->
								@endforeach
								<div class="col-md-12">
									<button class="btn btn-primary btn-send pull-right">Salvar</button>
								</div>
							</form>
						</div>
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" :class='isHash("#tab_2")' id="tab_2">
						<div class="box box-info">
							<div class="box-header">
								<h3 class="box-title">Adicionar Imagens</h3>
							</div>
							<!-- Dropzone -->
							<div id="actions" class="multiple-upload">
								<form action="{{ route('admin.empresa.produtos_sob_medida.galeria.store') }}" class="dropzone" id="myDropzone">
									{!! csrf_field() !!}
								</form>

							</div>
						</div>

						<div class="box">
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding">

								<div class="alert alert-success alert-dismissible" v-show="msgSuccessGallery">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<h4><i class="icon fa fa-check"></i> @{{ msgSuccess }}</h4>
								</div>

								<vue-table-gallery v-on:success="imageGalleryDelete(),updateGallery()" :items="items_gallery" urls='{"reorder": "/cms/empresa/produtos_sob_medida/galeria/reorder","image":"/_files/produtos_sob_medida/","delete":"/cms/empresa/produtos_sob_medida/galeria/delete/"}'></vue-table-gallery>

							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.tab-pane -->
				</div>
				<!-- /.tab-content -->
			</div>
			<!-- nav-tabs-custom -->
		</div>
	</div>
@endsection
