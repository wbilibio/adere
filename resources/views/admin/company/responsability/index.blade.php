@extends('admin.layouts.app')

@section('htmlheader_title','Responsabilidade Social')
@section('contentheader_title','Responsabilidade Social')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-building-o"></i> Empresa</a></li>
		<li class="active">Responsabilidade Social</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-md-12">
			<!-- Custom Tabs -->
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li :class='isHash("#tab_1")'><a href="#tab_1" data-toggle="tab"><i class="fa fa-file-text-o"></i> Título e textos</a></li>
					<li :class='isHash("#tab_2")'><a href="#tab_2" data-toggle="tab"><i class="fa fa-check-square-o"></i> Projetos Sociais</a></li>
				</ul>
				<div class="tab-content bg-gray color-palette">
					<div class="tab-pane" :class='isHash("#tab_1")' id="tab_1">
						<div class="row">
							<form action="{{ empty($item->id) ? route('admin.empresa.responsabilidade_social.store') : route('admin.empresa.responsabilidade_social.store.{id}', array($item->id)) }}" method="POST" enctype="multipart/form-data">
								{!! csrf_field() !!}
								<div class="col-xs-6">
									<div class="form-group">
										<label for="image">Imagem Principal:</label>
										<div class="upload">
											<div class="btn btn-primary">Adicionar Imagem</div>
											<input type="file" id="image" name="file_image" />
											@if (!empty($item->image))
												(<a href="{{ asset('_files/responsabilidade_social/' . $item->image) }}" title="ver imagem atual" target="_blank">Ver Imagem Atual</a>)
											@endif
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
								@foreach(['pt','en','es'] as $locale)
									<div class="col-md-4">
										<!-- DIRECT CHAT SUCCESS -->
										<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success">
											<div class="box-header with-border">
												<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
												<div class="box-tools pull-right">
													<button type="button" class="btn btn-box-tool" data-widget="collapse">
														<i class="fa fa-minus"></i>
													</button>
												</div>
											</div>
											<!-- /.box-header -->
											<div class="box-body">
												<div class="col-xs-12">
													<div class="form-group">
														<label>Título:</label>
														<input type="text" name="title_{{$locale}}" class="form-control" value="{{ empty($item) ? old('title') : $item->getTranslation($locale)->title}}">
													</div>
												</div>
												<div class="col-xs-12">
													<div class="form-group">
														<label>Descrição:</label>
														<textarea name="text_{{$locale}}" class="form-control tinymce">{{ empty($item) ? old('text') : $item->getTranslation($locale)->text}}</textarea>
													</div>
												</div>
												<div class="col-xs-12">
													<div class="form-group">
														<label>Título Projetos:</label>
														<input type="text" name="title_projects_{{$locale}}" class="form-control" value="{{ empty($item) ? old('title_projects') : $item->getTranslation($locale)->title_projects}}">
													</div>
												</div>
												<div class="col-xs-12">
													<div class="form-group">
														<label>Descrição projetos sociais:</label>
														<textarea name="text_projects_{{$locale}}" class="form-control">{{ empty($item) ? old('text_projects') : $item->getTranslation($locale)->text_projects}}</textarea>
													</div>
												</div>
												<div class="col-xs-12">
													<div class="form-group">
														<label>Descrição Galeria:</label>
														<textarea class="form-control" name="text_gallery_{{$locale}}" id="text_gallery" cols="30" rows="3">{{ empty($item) ? old('text_gallery') : $item->getTranslation($locale)->text_gallery}}</textarea>
													</div>
												</div>
											</div>
										</div>
										<!--/.direct-chat -->
									</div>
									<!-- /.col -->
								@endforeach
								<div class="col-md-12">
									<button class="btn btn-primary btn-send pull-right">Salvar</button>
								</div>
							</form>
						</div>
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" :class='isHash("#tab_2")' id="tab_2">
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<div class="box">
									<!-- /.box-header -->
									<div class="box-body table-responsive no-padding">
										<div class="alert alert-success alert-dismissible" v-show="msgSuccessReorder">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											<h4><i class="icon fa fa-check"></i> @{{ msgSuccess }}</h4>
										</div>
										<vue-table-list :items-list="items_list" urls='{"reorder": "/cms/empresa/responsabilidade_social/projetos/reorder","image":"/_files/projetos_sociais/","delete":"/cms/empresa/responsabilidade_social/projetos/delete/","edit":"/cms/empresa/responsabilidade_social/projetos/editar/"}'></vue-table-list>
									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
							<div class="col-xs-12 col-md-4">
								<div class="box box-info">
									<div class="box-header">
										<h3 class="box-title">Adicionar Projeto</h3>
									</div>
									<form action="{{ route('admin.empresa.responsabilidade_social.projetos.store') }}" class="form-table-list" method="POST" enctype="multipart/form-data">
										{!! csrf_field() !!}
										<div class="box-body">
											<div class="row">
												<div class="col-xs-12">
													<div class="form-group">
														<label for="image">Imagem de banner do Projeto:</label>
														<div class="upload">
															<div class="btn btn-primary">Adicionar Imagem</div>
															<input type="file" id="image" name="file_image" />
														</div>
													</div>
												</div>
												@foreach([1,2,3,4] as $i)
													<div class="col-xs-12">
														<div class="form-group">
															<label for="video_{{$i}}">Copie e cole seu link do youtube {{$i}}:</label>
															<input type="text" name="link_youtube_{{$i}}" class="form-control">
														</div>
													</div>
												@endforeach
												@foreach(['pt','en','es'] as $key=>$locale)
													<div class="col-md-12">
														<!-- DIRECT CHAT SUCCESS -->
														<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success {{ $key == 0 ? '' : 'collapsed-box'}}">
															<div class="box-header with-border">
																<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
																<div class="box-tools pull-right">
																	<button type="button" class="btn btn-box-tool" data-widget="collapse">
																		<i class="fa {{ $key == 0 ? 'fa-minus' : 'fa-plus'}}"></i>
																	</button>
																</div>
															</div>
															<!-- /.box-header -->
															<div class="box-body">
																<div class="col-xs-12">
																	<div class="form-group">
																		<label>Título:</label>
																		<input type="text" name="title_{{$locale}}" class="form-control" {{($locale == 'pt') ? 'required' : '' }}>
																	</div>
																</div>
																<div class="col-xs-12">
																	<div class="form-group">
																		<label>Descrição:</label>
																		<div class="content-editor">
																			<div class="btn {{($locale == 'pt' ? 'btn-success' : ($locale == 'en' ? 'btn-danger' : ($locale == 'es' ? 'btn-warning' : ''))) }} open-editor">Adicionar Descrição</div>
																			<div class="modal-editor">
																				<div class="overlay-editor"></div>
																				<div class="container-editor">
																					<div class="close-editor">
																						<span class="fa-stack fa-lg">
																						  <i class="fa fa-circle fa-stack-2x"></i>
																						  <i class="fa fa-close fa-stack-1x fa-inverse"></i>
																						</span>
																					</div>
																					<textarea class="tinymce" name="text_{{$locale}}" id="text" cols="30" rows="10"></textarea>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!--/.direct-chat -->
													</div>
													<!-- /.col -->
												@endforeach
											</div>
											<div class="col-md-12">
												<button class="btn btn-primary btn-send pull-right">Salvar</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<!-- /.tab-pane -->
				</div>
				<!-- /.tab-content -->
			</div>
			<!-- nav-tabs-custom -->
		</div>
	</div>
@endsection
