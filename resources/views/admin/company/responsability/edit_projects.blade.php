@extends('admin.layouts.app')

@section('htmlheader_title','Responsabilidade Social')
@section('contentheader_title','Responsabilidade Social')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-building-o"></i> Empresa</a></li>
		<li class="active"><a href="{{ route('admin.empresa.responsabilidade_social') }}#tab_2"></a>Responsabilidade Social</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Editar Projeto</h3>
				</div>
				<form action="{{ route('admin.empresa.responsabilidade_social.projetos.update.{id}', array($item->id)) }}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="col-xs-6">
							<div class="form-group">
								<label for="image">Imagem Principal:</label>
								<div class="upload">
									<div class="btn btn-primary">Atualizar Imagem</div>
									<input type="file" id="image" name="file_image" />
									@if (!empty($item->id))
										(<a href="{{ asset('_files/projetos_sociais/' . $item->image) }}" title="ver imagem atual" target="_blank">Ver Imagem Atual</a>)
									@endif
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						@foreach([1,2,3,4] as $i)
							<div class="col-xs-3">
								<div class="form-group">
									<label for="video_{{$i}}">Link video {{$i}}:</label>
									<input type="text" name="link_youtube_{{$i}}" value="{{ empty($item['link_youtube_'.$i]) ? old('link_youtube_'.$i) : $item['link_youtube_'.$i] }}" class="form-control">
								</div>
							</div>
							<div class="col-xs-3">
								<div class="form-group">
									<label for="image_{{$i}}">Imagem do video {{$i}}:</label>
									<div class="upload">
										<div class="btn btn-primary">Adicionar Imagem</div>
										<input type="file" id="image" name="file_image_{{$i}}" />
										@if (!empty($item['image_'.$i]))
											(<a href="{{ asset('_files/projetos_sociais/' . $item['image_'.$i]) }}" title="ver imagem atual" target="_blank">Ver Imagem Atual</a>)
										@endif
									</div>
								</div>
							</div>
						@endforeach
						<div class="clearfix"></div>
						@foreach(['pt','en','es'] as $locale)
							<div class="col-md-4">
								<!-- DIRECT CHAT SUCCESS -->
								<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success">
									<div class="box-header with-border">
										<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="fa fa-minus"></i>
											</button>
										</div>
									</div>
									<!-- /.box-header -->
									<div class="box-body">
										<div class="col-xs-12">
											<div class="form-group">
												<label>Título:</label>
												<input type="text" name="title_{{$locale}}" class="form-control" value="{{ empty($item->getTranslation($locale)->title) ? old('title') : $item->getTranslation($locale)->title}}">
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group">
												<label>Descrição:</label>
												<textarea name="text_{{$locale}}" class="form-control tinymce">{{ empty($item->getTranslation($locale)->text) ? old('text') : $item->getTranslation($locale)->text}}</textarea>
											</div>
										</div>
									</div>
								</div>
								<!--/.direct-chat -->
							</div>
							<!-- /.col -->
						@endforeach
					</div>
					<div class="box-footer">
						<button class="btn btn-primary btn-send pull-right">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
