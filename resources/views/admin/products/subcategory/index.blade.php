@extends('admin.layouts.app')

@section('htmlheader_title','Subcategorias de produto')
@section('contentheader_title','Subcategorias de produto')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-laptop"></i> Produtos</a></li>
		<li class="active">Subcategorias</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<div class="vue-table" id="vue-tables-options">
						<div class="col-xs-12">
							<v-client-table :data="subcategoryData" :columns="subcategoryColumns" :options="subcategoryOptions"></v-client-table>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Adicionar Subcategoria (Família)</h3>
				</div>
				<form action="{{ route('admin.produtos.subcategorias.store') }}" class="form-table-list" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="product-type">Categoria(tipo) Pai:</label>
									<select name="product_type_id" id="product-type" class="form-control" required>
										<option value="">Selecione uma categoria</option>
										@foreach($items_types as $item_type)
											<option value="{{$item_type->id}}">{{$item_type->title}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="checkbox">
									<p>Selecione um mercado:</p>
									<label><input type="checkbox" v-model="visible_select">  <span v-cloak>@{{ (visible_select) ? 'Ativo' : 'Inativo' }}</span></label>
								</div>
							</div>
							@foreach(['pt','en','es'] as $key=>$locale)
								<div class="col-md-12">
									<!-- DIRECT CHAT SUCCESS -->
									<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success {{ $key == 0 ? '' : 'collapsed-box'}}">
										<div class="box-header with-border">
											<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa {{ $key == 0 ? 'fa-minus' : 'fa-plus'}}"></i>
												</button>
											</div>
										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<div class="col-xs-12">
												<div class="form-group">
													<label>Título:</label>
													<input type="text" name="{{$locale}}_title" class="form-control">
												</div>
											</div>
											<div class="col-xs-12">
												<div class="form-group">
													<label>Descrição:</label>
													<div class="content-editor">
														<div class="btn {{($locale == 'pt' ? 'btn-success' : ($locale == 'en' ? 'btn-danger' : ($locale == 'es' ? 'btn-warning' : ''))) }} open-editor">Adicionar Descrição</div>
														<div class="modal-editor">
															<div class="overlay-editor"></div>
															<div class="container-editor">
																<div class="close-editor">
																<span class="fa-stack fa-lg">
																  <i class="fa fa-circle fa-stack-2x"></i>
																  <i class="fa fa-close fa-stack-1x fa-inverse"></i>
																</span>
																</div>
																<textarea class="tinymce" name="{{$locale}}_description" id="text_gallery" cols="30" rows="10"></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--/.direct-chat -->
								</div>
								<!-- /.col -->
							@endforeach
						</div>
						<div class="col-md-12">
							<input type="hidden" name="status" :value="status" />
							<button class="btn btn-primary btn-send pull-right">Salvar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
