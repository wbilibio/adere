@extends('admin.layouts.app')

@section('htmlheader_title','Subcategorias de produto')
@section('contentheader_title','Subcategorias de produto')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-laptop"></i> Produtos</a></li>
		<li class="active"><a href="{{ route('admin.produtos.subcategorias') }}">Subcategorias</a></li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Editar Campos</h3>
				</div>
				<form action="{{ route('admin.produtos.subcategorias.update.{id}', array($item->id)) }}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="col-md-12">
							<div class="form-group">
								<label for="product-type">Categoria(tipo) Pai:</label>
								<select name="product_type_id" id="product-type" class="form-control" required>
									<option value="">Selecione uma categoria</option>
									@foreach($items_types as $item_type)
										<option value="{{$item_type->id}}" {{ ($item_type->id == $item->product_type_id) ? 'selected' : '' }}>{{$item_type->title}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="checkbox">
								<p>Status:</p>
								<label><input type="checkbox" v-model="visible_select"><span v-cloak>@{{ (visible_select) ? 'Ativo' : 'Inativo' }}</span></label>
							</div>
						</div>
						<div class="clearfix"></div>
						@foreach(['pt','en','es'] as $locale)
							<div class="col-md-4">
								<!-- DIRECT CHAT SUCCESS -->
								<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success">
									<div class="box-header with-border">
										<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="fa fa-minus"></i>
											</button>
										</div>
									</div>
									<!-- /.box-header -->
									<div class="box-body">
										<div class="col-xs-12">
											<div class="form-group">
												<label>Título:</label>
												<input type="text" name="{{$locale}}_title" class="form-control" value="{{ empty($item[$locale.'_title']) ? old('title') : $item[$locale.'_title'] }}">
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group">
												<label>Descrição:</label>
												<textarea name="{{$locale}}_description" class="form-control tinymce">{{ empty($item[$locale.'_description']) ? old('description') : $item[$locale.'_description'] }}</textarea>
											</div>
										</div>
									</div>
								</div>
								<!--/.direct-chat -->
							</div>
							<!-- /.col -->
						@endforeach
					</div>
					<div class="box-footer">
						<input type="hidden" name="status" :value="status" />
						<button class="btn btn-primary btn-send pull-right">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
