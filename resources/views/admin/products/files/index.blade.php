@extends('admin.layouts.app')

@section('htmlheader_title','Produtos - Arquivos')
@section('contentheader_title','Produtos - Arquivos')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-building-o"></i> Produtos</a></li>
		<li class="active">Arquivos</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">

					<div class="alert alert-success alert-dismissible" v-show="msgSuccessReorder" v-cloak>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> @{{ msgSuccess }}</h4>
					</div>

					<vue-table-list :items-list="items_list" urls='{"reorder": "/cms/produtos/arquivos/reorder","image":"false","delete":"/cms/produtos/arquivos/delete/","edit":"/cms/produtos/arquivos/editar/"}'></vue-table-list>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Adicionar Arquivo</h3>
				</div>
				<form action="{{ route('admin.produtos.arquivos.store') }}" class="form-table-list" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<label for="archive">Adicionar arquivo:</label>
									<div class="upload">
										<div class="btn btn-primary">Adicionar arquivo</div>
										<input type="file" id="archive" name="file_archive" />
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="checkbox checklist">
									<p>Selecione produtos relacionados:</p>
									<div class="scrolling">
										@foreach($items_products as $item_product)
											<label><input type="checkbox" name="checklist[]" value="{{ $item_product->id }}"> {{ $item_product->title }}</label>
										@endforeach
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="checkbox">
									<p>Status:</p>
									<label><input type="checkbox" v-model="visible_select">  <span v-cloak>@{{ (visible_select) ? 'Ativo' : 'Inativo' }}</span></label>
								</div>
							</div>
							@foreach(['pt','en','es'] as $key=>$locale)
								<div class="col-md-12">
									<!-- DIRECT CHAT SUCCESS -->
									<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success {{ $key == 0 ? '' : 'collapsed-box'}}">
										<div class="box-header with-border">
											<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa {{ $key == 0 ? 'fa-minus' : 'fa-plus'}}"></i>
												</button>
											</div>
										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<div class="col-xs-12">
												<div class="form-group">
													<label>Título:</label>
													<input type="text" name="title_{{$locale}}" class="form-control">
												</div>
											</div>
										</div>
									</div>
									<!--/.direct-chat -->
								</div>
								<!-- /.col -->
							@endforeach
						</div>
						<div class="col-md-12">
							<input type="hidden" name="status" :value="status" />
							<button class="btn btn-primary btn-send pull-right">Salvar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
