@extends('admin.layouts.app')

@section('htmlheader_title','Produtos')
@section('contentheader_title','Produtos')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-laptop"></i> Produtos</a></li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Editar Campos</h3>
				</div>
				<form action="{{ route('admin.produtos.lista.update.{id}', array($item->id)) }}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="col-md-4">
							<div class="form-group">
								<label for="product-type">Subcategoria(família) Pai:</label>
								<select name="product_family_id" id="product-family" class="form-control" required>
									<option value="">Selecione uma subcategoria</option>
									@foreach($items_families as $item_family)
										<option value="{{$item_family->id}}" {{ ($item_family->id == $item->product_family_id) ? 'selected' : '' }}>{{$item_family->title}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="product-mark">Selecione uma marca:</label>
								<select name="mark_id" id="product-mark" class="form-control">
									<option value="">Selecione uma subcategoria</option>
									@foreach($items_marks as $item_mark)
										<option value="{{$item_mark->id}}" {{ ($item_mark->id == $item->mark_id) ? 'selected' : '' }}>{{$item_mark->title}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="form-group">
								<label>Link da loja Virtual:</label>
								<input type="text" name="url_loja_virtual" class="form-control" value="{{ empty($item->url_loja_virtual) ? old('url_loja_virtual') : $item->url_loja_virtual }}">
							</div>
						</div>
						<div class="col-xs-4">
							<div class="checkbox">
								<p>Status:</p>
								<label><input type="checkbox" v-model="visible_select"><span v-cloak>@{{ (visible_select) ? 'Ativo' : 'Inativo' }}</span></label>
							</div>
						</div>
						<div class="clearfix"></div>
						@foreach(['pt','en','es'] as $locale)
							<div class="col-md-4">
								<!-- DIRECT CHAT SUCCESS -->
								<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success">
									<div class="box-header with-border">
										<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="fa fa-minus"></i>
											</button>
										</div>
									</div>
									<!-- /.box-header -->
									<div class="box-body">
										<div class="col-xs-12">
											<div class="form-group">
												<label>Título:</label>
												<input type="text" name="{{$locale}}_title" class="form-control" value="{{ empty($item[$locale.'_title']) ? old('title') : $item[$locale.'_title'] }}">
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group">
												<label>Descrição:</label>
												<textarea class="tinymce" name="{{$locale}}_description" id="description" cols="30" rows="10">{{ empty($item[$locale.'_description']) ? old('description') : $item[$locale.'_description'] }}</textarea>
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group">
												<label>Aplicação:</label>
												<textarea class="tinymce" name="{{$locale}}_application" id="application" cols="30" rows="10">{{ empty($item[$locale.'_application']) ? old('application') : $item[$locale.'_application'] }}</textarea>
											</div>
										</div>
									</div>
								</div>
								<!--/.direct-chat -->
							</div>
							<!-- /.col -->
						@endforeach
					</div>
					<div class="box-footer">
						<input type="hidden" name="status" :value="status" />
						<button class="btn btn-primary btn-send pull-right">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
