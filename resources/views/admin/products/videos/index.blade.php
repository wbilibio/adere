@extends('admin.layouts.app')

@section('htmlheader_title','Videos de Produtos')
@section('contentheader_title','Videos de Produtos')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-tags"></i> Videos</a></li>
		<li class="active">{{  $item->title }}</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">

					<div class="alert alert-success alert-dismissible" v-show="msgSuccessReorder" v-cloak>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> @{{ msgSuccess }}</h4>
					</div>

					<vue-table-list :items-list="items_list" urls='{"reorder": "/cms/produtos/videos/reorder","image":"/_files/produtos_videos/","delete":"/cms/produtos/videos/delete/{{$item->id}}/","edit":"/cms/produtos/videos/editar/{{$item->id}}/"}'></vue-table-list>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Adicionar Video</h3>
				</div>
				<form action="{{ route('admin.produtos.videos.store.{id}', array($item->id)) }}" class="form-table-list" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<label for="image">Imagem:</label>
									<div class="upload">
										<div class="btn btn-primary">Adicionar Imagem</div>
										<input type="file" id="image" name="file_image" />
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="form-group">
									<label for="link">Link youtube:</label>
									<input type="text" id="link" name="link_youtube" class="form-control" />
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<button class="btn btn-primary btn-send pull-right">Salvar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
