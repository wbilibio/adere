@extends('admin.layouts.app')

@section('htmlheader_title','Videos de Produtos')
@section('contentheader_title','Videos de Produtos')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-tags"></i> Videos</a></li>
		<li class="active">{{  $item_product->title }}</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Editar Campos</h3>
				</div>
				<form action="{{ route('admin.produtos.videos.update.{product_id}.{id}', array($item_product->id,$item->id)) }}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="col-xs-4">
							<div class="form-group">
								<label for="image">Imagem:</label>
								<div class="upload">
									<div class="btn btn-primary">Atualizar Imagem</div>
									<input type="file" id="image" name="file_image" />
									@if (!empty($item->image))
										(<a href="{{ asset('_files/produtos_videos/' . $item->image) }}" title="ver imagem atual" target="_blank">Ver Imagem Atual</a>)
									@endif
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<label for="link">Link:</label>
								<input type="text" class="form-control" id="link" name="link_youtube" value="{{ empty($item->link_youtube) ? old('link_youtube') : $item->link_youtube}}" />
							</div>
						</div>


					</div>
					<div class="box-footer">
						<button class="btn btn-primary btn-send pull-right">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
