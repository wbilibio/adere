@extends('admin.layouts.app')

@section('htmlheader_title','Nossas marcas')
@section('contentheader_title','Nossas marcas')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-briefcase"></i> Nossas marcas</a></li>
		<li class="active">Lista</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">

					<div class="alert alert-success alert-dismissible" v-show="msgSuccessReorder" v-cloak>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> @{{ msgSuccess }}</h4>
					</div>

					<vue-table-list :items-list="items_list" urls='{"reorder": "/cms/marcas/lista/reorder","image":"/_files/marcas/","gallery":"/cms/marcas/galeria/","delete":"/cms/marcas/lista/delete/","edit":"/cms/marcas/lista/editar/"}'></vue-table-list>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Adicionar Marca</h3>
				</div>
				<form action="{{ route('admin.marcas.lista.store') }}" class="form-table-list" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<label for="image">Logo:</label>
									<div class="upload">
										<div class="btn btn-primary">Adicionar Logo</div>
										<input type="file" id="image" name="file_image" />
									</div>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="checkbox">
									<p>Status:</p>
									<label><input type="checkbox" v-model="visible_select">  <span v-cloak>@{{ (visible_select) ? 'Ativo' : 'Inativo' }}</span></label>
								</div>
							</div>
							@foreach(['pt','en','es'] as $key=>$locale)
								<div class="col-md-12">
									<!-- DIRECT CHAT SUCCESS -->
									<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success {{ $key == 0 ? '' : 'collapsed-box'}}">
										<div class="box-header with-border">
											<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa {{ $key == 0 ? 'fa-minus' : 'fa-plus'}}"></i>
												</button>
											</div>
										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<div class="col-xs-12">
												<div class="form-group">
													<label>Título:</label>
													<input type="text" name="title_{{$locale}}" class="form-control">
												</div>
											</div>
											<div class="col-xs-12">
												<div class="form-group">
													<label>Descrição:</label>
													<div class="content-editor">
														<div class="btn {{($locale == 'pt' ? 'btn-success' : ($locale == 'en' ? 'btn-danger' : ($locale == 'es' ? 'btn-warning' : ''))) }} open-editor">Adicionar Descrição</div>
														<div class="modal-editor">
															<div class="overlay-editor"></div>
															<div class="container-editor">
																<div class="close-editor">
																	<span class="fa-stack fa-lg">
																	  <i class="fa fa-circle fa-stack-2x"></i>
																	  <i class="fa fa-close fa-stack-1x fa-inverse"></i>
																	</span>
																</div>
																<textarea class="tinymce" name="text_{{$locale}}" id="text_gallery" cols="30" rows="10"></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--/.direct-chat -->
								</div>
								<!-- /.col -->
							@endforeach
						</div>
						<div class="col-md-12">
							<input type="hidden" name="status" :value="status" />
							<button class="btn btn-primary btn-send pull-right">Salvar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
