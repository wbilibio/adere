@extends('admin.layouts.app')

@section('htmlheader_title','Galeria de Mercados')
@section('contentheader_title','Galeria de Mercados')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-building-o"></i> Mercados</a></li>
		<li class="active">{{  $item->title }}</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Adicionar Imagens</h3>
				</div>
				<!-- Dropzone -->
				<div id="actions" class="multiple-upload">
					<form action="{{ route('admin.mercados.galeria.store.{id}', array($item->id)) }}" class="dropzone" id="myDropzone">
						{!! csrf_field() !!}
					</form>

				</div>
			</div>

			<div class="box">
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<div class="alert alert-success alert-dismissible" v-show="msgSuccessGallery" v-cloak>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> @{{ msgSuccess }}</h4>
					</div>

					<vue-table-gallery v-on:success="imageGalleryDelete(),updateGallery()" :items="items_gallery" urls='{"reorder": "/cms/mercados/galeria/{{$item->id}}/reorder","image":"/_files/mercados_galeria/","delete":"/cms/mercados/galeria/delete/"}'></vue-table-gallery>

				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
@endsection
