@extends('admin.layouts.app')

@section('htmlheader_title','Mercados - Arquivos')
@section('contentheader_title','Mercados - Arquivos')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-building-o"></i> Mercados</a></li>
		<li class="active">Arquivos</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12 col-md-8">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">

					<div class="alert alert-success alert-dismissible" v-show="msgSuccessReorder" v-cloak>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> @{{ msgSuccess }}</h4>
					</div>

					<vue-table-list :items-list="items_list" urls='{"reorder": "/cms/mercados/arquivos/reorder","image":"false","delete":"/cms/mercados/arquivos/delete/","edit":"/cms/mercados/arquivos/editar/"}'></vue-table-list>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<div class="col-xs-12 col-md-4">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Adicionar Arquivo</h3>
				</div>
				<form action="{{ route('admin.mercados.arquivos.store') }}" class="form-table-list" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<label for="archive">Adicionar arquivo:</label>
									<div class="upload">
										<div class="btn btn-primary">Adicionar arquivo</div>
										<input type="file" id="archive" name="file_archive" />
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="checkbox checklist">
									<p>Selecione itens de mercado:</p>
									@foreach($items_markets as $item_market)
										<label><input type="checkbox" name="checklist[]" value="{{ $item_market->id }}"> {{ $item_market->title }}</label>
									@endforeach
								</div>
							</div>
							<div class="col-xs-12">
								<div class="checkbox">
									<p>Status:</p>
									<label><input type="checkbox" v-model="visible_select">  <span v-cloak>@{{ (visible_select) ? 'Ativo' : 'Inativo' }}</span></label>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="checkbox">
									<p>Guia de mercado:</p>
									<label><input type="checkbox" v-model="is_guia">  <span v-cloak>@{{ (is_guia) ? 'Ativo' : 'Inativo' }}</span></label>
								</div>
							</div>
							@foreach(['pt','en','es'] as $key=>$locale)
								<div class="col-md-12">
									<!-- DIRECT CHAT SUCCESS -->
									<div class="box {{($locale == 'pt' ? 'box-success' : ($locale == 'en' ? 'box-danger' : ($locale == 'es' ? 'box-warning' : ''))) }} direct-chat direct-chat-success {{ $key == 0 ? '' : 'collapsed-box'}}">
										<div class="box-header with-border">
											<h3 class="box-title"><span class="icon-language icon-{{$locale}}"></span> {{($locale == 'pt' ? 'Português' : ($locale == 'en' ? 'Inglês' : ($locale == 'es' ? 'Espanhol' : ''))) }}</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa {{ $key == 0 ? 'fa-minus' : 'fa-plus'}}"></i>
												</button>
											</div>
										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<div class="col-xs-12">
												<div class="form-group">
													<label>Título:</label>
													<input type="text" name="title_{{$locale}}" class="form-control">
												</div>
											</div>
										</div>
									</div>
									<!--/.direct-chat -->
								</div>
								<!-- /.col -->
							@endforeach
						</div>
						<div class="col-md-12">
							<input type="hidden" name="status" :value="status" />
							<input type="hidden" name="guia" :value="guia" />
							<button class="btn btn-primary btn-send pull-right">Salvar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
