<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}
                    </a>
                </div>
            </div>
    @endif

    <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('adminlte_lang::message.header') }}</li>
            <!-- Optionally, you can add icons to the links -->
            <li {{ Request::segment(2) == "home" ? 'class=active' : '' }}>
                <a href="{{ route('admin.home') }}">
                    <i class='fa fa-home'></i> <span>Página Inicial</span>
                </a>
            </li>
            <li class="treeview {{ Request::segment(2) == "empresa" ? 'active' : '' }}">
                <a href="#">
                    <i class='fa fa-building-o'></i> <span>Empresa</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ Request::segment(3) == "institucional" ? 'class=active' : '' }}><a
                                href="{{ route('admin.empresa.institucional') }}"><i class='fa fa-bank'></i>
                            Institucional</a></li>
                    <li {{ Request::segment(3) == "certificacoes" ? 'class=active' : '' }}><a
                                href="{{ route('admin.empresa.certificacoes') }}"><i class='fa fa-certificate'></i>
                            Certificações</a></li>
                    <li {{ Request::segment(3) == "responsabilidade_social" ? 'class=active' : '' }}><a
                                href="{{ route('admin.empresa.responsabilidade_social') }}"><i
                                    class='fa fa-recycle'></i> Responsabilidades Sociais</a></li>
                    <li {{ Request::segment(3) == "parcerias" ? 'class=active' : '' }}><a
                                href="{{ route('admin.empresa.parcerias') }}"><i class='fa fa-handshake-o'></i>
                            Parcerias</a></li>
                    <li {{ Request::segment(3) == "produtos_sob_medida" ? 'class=active' : '' }}><a
                                href="{{ route('admin.empresa.produtos_sob_medida') }}"><i class='fa fa-sliders'></i>
                            Produtos sob medida</a></li>
                </ul>
            </li>
            <li class="treeview {{ Request::segment(2) == "blog" ? 'active' : '' }}">
                <a href="#">
                    <i class='fa fa-commenting'></i> <span>Blog</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    {{--<li {{ Request::segment(3) == "categorias" ? 'class=active' : '' }}><a
                                href="{{ route('admin.blog.categorias') }}"><i class='fa fa-bookmark'></i>
                            Categorias</a></li>--}}
                    <li {{ Request::segment(3) == "posts" ? 'class=active' : '' }}><a
                                href="{{ route('admin.blog.posts') }}"><i class='fa fa-comments-o'></i>
                            Posts</a></li>
                </ul>
            </li>
            <li class="treeview {{ Request::segment(2) == "produtos" ? 'active' : '' }}">
                <a href="#">
                    <i class='fa fa-shopping-bag'></i> <span>Produtos</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ Request::segment(3) == "categorias" ? 'class=active' : '' }}><a
                                href="{{ route('admin.produtos.categorias') }}"><i class='fa fa-tag'></i>
                            Categorias(TIPO)</a></li>
                    <li {{ Request::segment(3) == "subcategorias" ? 'class=active' : '' }}><a
                                href="{{ route('admin.produtos.subcategorias') }}"><i class='fa fa-tags'></i>
                            Subcategorias(Família)</a></li>
                    <li {{ Request::segment(3) == "lista" ? 'class=active' : '' }}><a
                                href="{{ route('admin.produtos.lista') }}"><i class='fa fa-shopping-bag'></i>
                            Lista de produtos</a></li>
                    <li {{ Request::segment(3) == "arquivos" ? 'class=active' : '' }}><a
                                href="{{ route('admin.produtos.arquivos') }}"><i class='fa fa-archive'></i>
                            Arquivos</a></li>
                    <li {{ Request::segment(3) == "aplicacoes" ? 'class=active' : '' }}><a
                                href="{{ route('admin.produtos.aplicacoes') }}"><i class='fa fa-arrows-h'></i>
                            Aplicações</a></li>
                </ul>
            </li>
            <li class="treeview {{ Request::segment(2) == "mercados" ? 'active' : '' }}">
                <a href="#">
                    <i class='fa fa-briefcase'></i> <span>Mercados</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ Request::segment(3) == "categorias" ? 'class=active' : '' }}><a
                                href="{{ route('admin.mercados.categorias') }}"><i class='fa fa-tag'></i>
                            Categorias</a></li>
                    <li {{ Request::segment(3) == "arquivos" ? 'class=active' : '' }}><a
                                href="{{ route('admin.mercados.arquivos') }}"><i class='fa fa-archive'></i>
                            Arquivos</a></li>
                </ul>
            </li>
            <li class="treeview {{ Request::segment(2) == "marcas" ? 'active' : '' }}">
                <a href="#">
                    <i class='fa fa-flag'></i> <span>Nossas Marcas</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ Request::segment(3) == "categorias" ? 'class=active' : '' }}><a
                                href="{{ route('admin.marcas.lista') }}"><i class='fa fa-tag'></i>
                            Marcas</a></li>
                </ul>
            </li>

            <li class="treeview {{ Request::segment(2) == "catalogos" || Request::segment(2) == "banco_de_imagens" || Request::segment(2) == "faq"  ? 'active' : '' }}">
                <a href="#">
                    <i class='fa fa-flag'></i> <span>Serviços</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ Request::segment(2) == "catalogos" ? 'class=active' : '' }}>
                        <a href="{{ route('admin.catalogos') }}">
                            <i class='fa fa-book'></i> <span>Catálogos</span>
                        </a>
                    </li>
                    <li {{ Request::segment(2) == "banco_de_imagens" ? 'class=active' : '' }}>
                        <a href="{{ route('admin.banco_de_imagens') }}">
                            <i class='fa fa-image'></i> <span>Banco de Imagens</span>
                        </a>
                    </li>
                    <li {{ Request::segment(2) == "faq" ? 'class=active' : '' }}>
                        <a href="{{ route('admin.faq') }}">
                            <i class='fa fa-question-circle-o'></i> <span>Perguntas Frequentes</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li  class="treeview {{ Request::segment(2) == "textos" || Request::segment(2) == "configuracoes"  ? 'active' : '' }}">
                <a href="#">
                    <i class='fa fa-cogs'></i> <span>Dados gerais do site</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ Request::segment(2) == "sliders" ? 'class=active' : '' }}>
                        <a href="{{ route('admin.sliders') }}">
                            <i class='fa fa-picture-o'></i> <span>Slider Página inicial</span>
                        </a>
                    </li>
                    <li {{ Request::segment(2) == "banners" ? 'class=active' : '' }}>
                        <a href="{{ route('admin.banners') }}">
                            <i class='fa fa-picture-o'></i> <span>Banners</span>
                        </a>
                    </li>
                    <li {{ Request::segment(2) == "textos" ? 'class=active' : '' }}>
                        <a href="{{ route('admin.textos') }}">
                            <i class='fa fa-align-justify'></i> <span>Textos</span>
                        </a>
                    </li>
                    <li {{ Request::segment(2) == "configuracoes" ? 'class=active' : '' }}>
                        <a href="{{ route('admin.configuracoes') }}">
                            <i class='fa fa-comments'></i> <span>Midías, contatos e Seo</span>
                        </a>
                    </li>
                    <li {{ Request::segment(2) == "configuracoes" ? 'class=active' : '' }}>
                        <a href="{{ route('admin.newsletter') }}">
                            <i class='fa fa-envelope-o'></i> <span>Newsletter</span>
                        </a>
                    </li>
                    <li {{ Request::segment(2) == "clientes_download_arquivos" ? 'class=active' : '' }}>
                        <a href="{{ route('admin.clientes_download_arquivos') }}">
                            <i class='fa fa-envelope-o'></i> <span>Clientes que fizeram download</span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
