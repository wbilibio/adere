<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('admin/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>AD</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b> Adere </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                @if (Auth::guest())
                    <li><a href="{{ url('admin/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('admin/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                    @if(count($sharedData->get('items_post_comments')) > 0)
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <span class="label label-danger">{{count($sharedData->get('items_post_comments')) + count($sharedData->get('items_comment_answers'))}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">{{count($sharedData->get('items_post_comments'))}} comentários e {{count($sharedData->get('items_comment_answers'))}} respostas aguardando aprovação</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu ">
                                        @foreach($sharedData->get('items_post_comments') as $comment)
                                            <li class="list-comment"><!-- start message -->
                                                <a href="{{ url('/cms/blog/posts/'.$comment->post->id.'/comentarios/editar/'.$comment->id) }}">
                                                    <h4>
                                                        {{$comment->title}}
                                                        <small><i class="fa fa-clock-o"></i> {{ (\Carbon\Carbon::now()->diffInDays($comment->created_at)) > 0 ? \Carbon\Carbon::now()->diffInDays($comment->created_at).' dias atrás' : 'hoje' }}</small>
                                                    </h4>
                                                    <p>{{$comment->text}}</p>
                                                </a>
                                            </li>
                                        @endforeach
                                        @foreach($sharedData->get('items_comment_answers') as $comment_answer)
                                            <li class="list-answer"><!-- start message -->
                                                <a href="{{ url('/cms/blog/posts/'.$comment_answer->comment->post->id.'/comentarios/'.$comment_answer->post_comments_id.'/respostas/editar/'.$comment_answer->id) }}">
                                                    <h4>
                                                        <i class="fa fa-commenting"></i> {{$comment_answer->title}}
                                                        <small><i class="fa fa-clock-o"></i> {{ (\Carbon\Carbon::now()->diffInDays($comment_answer->created_at)) > 0 ? \Carbon\Carbon::now()->diffInDays($comment_answer->created_at).' dias atrás' : 'hoje' }}</small>
                                                    </h4>
                                                    <p>{{$comment_answer->text}}</p>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    @endif
                <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{ Gravatar::get($user->email) }}" class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image"/>
                                <p>
                                    {{ Auth::user()->name }}
                                    <small>Logado</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="{{ route('admin.logout') }}" class="btn btn-default btn-flat"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Sair
                                    </a>

                                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                        <input type="submit" value="logout" style="display: none;">
                                    </form>

                                </div>
                            </li>
                        </ul>
                    </li>
            @endif

            <!-- Control Sidebar Toggle Button -->
            </ul>
        </div>
    </nav>
</header>
