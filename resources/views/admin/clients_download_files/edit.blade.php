@extends('admin.layouts.app')

@section('htmlheader_title','Clientes download arquivos')
@section('contentheader_title','Clientes download arquivos')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li>Dados do cliente</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Dados do download</h3>
				</div>
				<ul class="list">
					@if(!empty($item->name)) <li><p><strong>Nome:</strong> {{$item->name}}</p></li> @endif
					@if(!empty($item->office)) <li><p><strong>Cargo:</strong> {{$item->office}}</p></li> @endif
					@if(!empty($item->company)) <li><p><strong>Empresa:</strong> {{$item->company}}</p></li> @endif
					@if(!empty($item->market)) <li><p><strong>Mercado de atuação:</strong> {{$item->market}}</p></li> @endif
					@if(!empty($item->email)) <li><p><strong>Email:</strong> {{$item->email}}</p></li> @endif
					@if(!empty($item->file_name)) <li><p><strong>Nome do arquivo:</strong> {{$item->file_name}}</p></li> @endif
				</ul>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
@endsection
