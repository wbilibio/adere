@extends('admin.layouts.app')

@section('htmlheader_title','Clientes download arquivos')
@section('contentheader_title','Clientes download arquivos')
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li>Clientes download arquivos</li>
	</ol>
@endsection

@section('main-content')
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">

					<div class="alert alert-success alert-dismissible" v-show="msgSuccessReorder" v-cloak>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check"></i> @{{ msgSuccess }}</h4>
					</div>

					<vue-table-list :items-list="items_list" urls='{"reorder": "false","image":"false","edit":"/cms/clientes_download_arquivos/editar/","delete":"/cms/clientes_download_arquivos/delete/"}'></vue-table-list>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
@endsection
