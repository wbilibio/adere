<table width="600" cellpadding="5" cellspacing="0">
    <tbody>
    <tr>
        <td>
            Ouvidoria
        </td>
    </tr>
    <tr>
        <td width="100" align="right">Nome:</td>
        <td>{{ $request->name }}</td>
    </tr>
    <tr>
        <td width="100" align="right">E-mail:</td>
        <td>{{ $request->email }}</td>
    </tr>
    <tr>
        <td width="100" align="right">Mensagem:</td>
        <td>{{ $request->text }}</td>
    </tr>
    </tbody>
</table>