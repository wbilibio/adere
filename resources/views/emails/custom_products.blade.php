<table width="600" cellpadding="5" cellspacing="0">
    <tbody>
        <tr>
            <td>
                Solicitação de solução.
            </td>
        </tr>
        <tr>
            <td width="100" align="right">Nome:</td>
            <td>{{ $request->name }}</td>
        </tr>
        <tr>
            <td width="100" align="right">Cargo:</td>
            <td>{{ $request->office }}</td>
        </tr>
        <tr>
            <td width="100" align="right">Empresa:</td>
            <td>{{ $request->company }}</td>
        </tr>
        <tr>
            <td width="100" align="right">Mercado de atuação:</td>
            <td>{{ $request->market_of_action }}</td>
        </tr>
        <tr>
            <td width="100" align="right">E-mail:</td>
            <td>{{ $request->email }}</td>
        </tr>
        <tr>
            <td width="100" align="right">Telefone:</td>
            <td>{{ $request->phone }}</td>
        </tr>
        <tr>
            <td width="100" align="right">Assunto:</td>
            <td>{{ $request->subject }}</td>
        </tr>
        <tr>
            <td width="100" align="right">Mensagem:</td>
            <td>{{ $request->text }}</td>
        </tr>
    </tbody>
</table>