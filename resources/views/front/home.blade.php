@extends('front.layout.master')

@section('header_title', trans('title.home'))

@section('main-content')
    <main id="main" class="page-home">
        <section class="home-banner">
            <div class="home-banner-list owl-carousel home-carousel">
                @foreach($items_slider as $item_slider)
                    <div style="background-image: url({{asset('_files/sliders/'.$item_slider->image_background)}})">
                        <div class="container">
                            @if(!empty($item_slider->link))
                                <a href="{{$item_slider->link}}">
                            @endif
                            @if(!empty($item_slider->image))
                            <figure class="banner-image">
                                <img src="{{asset('_files/sliders/'.$item_slider->image)}}" alt="">
                            </figure>
                            @endif
                            <span class="banner-text">
                                <span class="banner-text-content">
                                    <span class="pre">{{$item_slider->title}}</span>
                                    <span class="title">{{$item_slider->text}}</span>
                                    @if(!empty($item_slider->link))
                                        <span class="btn-border btn-white">Saiba Mais</span>
                                    @endif
                                </span>
                            </span>
                            @if(!empty($item_slider->link))
                                </a>
                            @endif
                        </div>

                    </div>
                @endforeach
            </div>
        </section>

        <section class="home-search">
            <form action="#" id="form-home-search" onsubmit="return false" class="home-search-form container">
                <div class="msg-error">
                    Não encontramos resultados para sua busca.
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label for="home-search-input">{{trans('title.buscar')}}</label>
                    </div>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <select name="select-mercado" id="form-search-type" class="select2 home-search-select" data-class="home-select2" data-searchmin="99999">
                                <option value="">{{ trans('title.tudo')}}</option>
                                @foreach($sharedData->get('markets') as $key => $market)
                                    <option value="{{ $market->slug }}">{{ $market->title }}</option>
                                @endforeach
                            </select>
                            <input type="text" id="home-search-input" placeholder="{{ trans('text.placeholder_buscar')}}">
                            <button type="submit"><span class="icon-search"></span></button>
                        </div>
                    </div>
                </div>

            </form>

        </section>

        <section class="home-atuacao">

            <div class="container">

                <header class="section-header">
                    <h2 class="section-title">{{ trans('title.mercado_de_atuacao')}}</h2>
                    @if(!empty($sharedData->get('texts')->text_market_of_action))
                        <p class="section-header-info">{{ $sharedData->get('texts')->text_market_of_action }}</p>
                    @endif
                    <div class="divisor d3x"></div>
                </header>

                <div class="section-content">

                    <div class="atuacao-tabs">
                        @foreach($sharedData->get('markets') as $key => $market)
                            <div class="atuacao-tab  {{$key == 0 ? 'active' : ''}}" data-id="{{$key+1}}">
                                @if(!empty($market->icon))
                                    <img src="{{ asset('_files/mercados/'.$market->icon)}}" alt="">
                                @endif
                                <p>{{(!empty($market->title)) ? $market->title : null}}</p>
                                <div class="divisor"></div>
                            </div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>

                    <div class="atuacao-tabs-content owl-carousel">
                        @foreach($sharedData->get('markets') as $key => $market)
                            <div class="row row-{{$key}}" data-id="{{$key+1}}">
                                <div class="col-sm-6 tab-image">
                                    <div>
                                        <figure>
                                            @if(!empty($market->metadata->image))
                                                <img src="{{ asset('_files/mercados/'.$market->metadata->image)}}" alt="">
                                            @endif
                                            @if(!empty($market->files->where('guia',1)->first()))
                                                <a href="{!! asset('_files/mercados_arquivos/'.$market->files->where('guia',1)->first()->file) !!}"><span class="btn btn-red-active active">Download do Guia <i class="fa fa-download"></i></span></a>
                                            @endif
                                        </figure>
                                    </div>
                                </div>
                                <div class="col-sm-1"></div>
                                <div class="col-sm-5 tab-content">
                                    <div class="tab-content-item">
                                        <h2 class="section-title">{{ (!empty($market->title)) ? $market->title : null  }}</h2>
                                        <div class="divisor d3x"></div>
                                        <p>{!! (!empty($market->description)) ? str_limit(strip_tags($market->description, 150)) : null  !!}</p>
                                        <a href="{!! (!empty($market->slug)) ? url($sharedData->get('lang_locale').'/mercados/'.$market->slug) : null !!}">Saiba Mais</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>

        <section class="home-brands">

            <div class="container">

                <header class="section-header">
                    <h2 class="section-title">{{ trans('title.nossas_marcas')}}</h2>
                    @if(!empty($sharedData->get('texts')->text_our_brands))
                        <p class="section-header-info">{{ $sharedData->get('texts')->text_our_brands }}</p>
                    @endif
                    <div class="divisor d3x"></div>
                </header>

                <div class="section-content">
                    <ul class="home-brand-list">
                        @foreach($sharedData->get('marks') as $item_mark)
                            <li>
                                <a href="{{url($sharedData->get('lang_locale').'/nossas_marcas/'.$item_mark->slug)}}">
                                    <img src="{{ asset('_files/marcas/'.$item_mark->image) }}" alt="">
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </section>

        <section class="home-products">

            <div class="container">

                <header class="section-header">
                    <h2 class="section-title">{{ trans('title.produtos_mais_buscados')}}</h2>
                    @if(!empty($sharedData->get('texts')->text_most_wanted_products))
                        <p class="section-header-info">{{ $sharedData->get('texts')->text_most_wanted_products }}</p>
                    @endif
                    <div class="divisor d3x"></div>
                </header>

                <div class="section-content">
                    <ul class="default-list">
                        @foreach($items_products as $item_product)
                            <li>
                                <figure>
                                    <a href="{{url($sharedData->get('lang_locale').'/solucoes/'.$item_product->family->type->slug.'/'. $item_product->family->slug . '/' .$item_product->slug )}}">
                                        @if(!empty($item_product->galleries) && count($item_product->galleries) > 0)
                                            <img src="{{asset('_files/produtos_galeria/'.$item_product->galleries->first()['image'])}}" alt="">
                                        @else
                                            <img src="{{asset('front/img/sem-foto.png')}}" alt="">
                                        @endif
                                    </a>
                                </figure>
                                <div class="title-container">
                                    <div class="title"><a href="{{url($sharedData->get('lang_locale').'/solucoes/'.$item_product->family->type->slug.'/'. $item_product->family->slug . '/' .$item_product->slug )}}">{{ !empty($item_product->title) ? $item_product->title : null }}</a></div>
                                </div>
                                @if(!empty($item_product->url_loja_virtual))
                                    <a href="{{$item_product->url_loja_virtual}}" class="btn btn-red">Comprar <i class="fa fa-shopping-cart"></i></a>
                                @endif
                            </li>
                         @endforeach
                    </ul>
                </div>
            </div>
        </section>

        <section class="home-banner-product">
            <div class="container">
                <div class="box">
                    <h2 class="title h1">{{ trans('title.produtos_sob_medida')}} ADERE 100%</h2>
                    @if(!empty($sharedData->get('texts')->text_custom_product))
                        <p>{{ $sharedData->get('texts')->text_custom_product }}</p>
                    @endif
                    <a href="{{ url($sharedData->get('lang_locale').'/produtos_sob_medida') }}" class="btn-border">Saiba Mais <i class="fa fa-chevron-right"></i></a>
                </div>
            </div>
            <img src="{{ asset('front/img/hom-sobmedida.png')}}" alt="" class="banner-product-img">
        </section>

        <section class="home-adere">
            <div class="container">
                <div class="row">
                    @if(!empty($banner_left))
                        <div class="col-lg-6">
                            @if(!empty($banner_left->link))
                                <a href="{{ $banner_left->link }}">
                            @endif
                                <div class="box">
                                    <img src="{{ asset('_files/banners/'.$banner_left->image)}}" alt="" />
                                    {{--<div class="title">{{ trans('title.50_anos_adere')}}</div>
                                    <p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de
                                        impressos</p>
                                    <a href="" class="btn btn-red-active">Saiba Mais <i class="fa fa-chevron-right"></i></a>--}}
                                </div>
                            @if(!empty($banner_left->link))
                                </a>
                            @endif
                        </div>
                    @endif
                    @if(!empty($banner_right))
                        <div class="col-lg-6">
                            @if(!empty($banner_right->link))
                                <a href="{{ $banner_right->link }}">
                            @endif
                                <div class="box">
                                <img src="{{ asset('_files/banners/'.$banner_right->image)}}" alt="" />
    {{--                            <div class="title">
                                    {{ trans('title.adere_em_numero')}}
                                </div>
                                <ul>
                                    <li>
                                        <strong>1500</strong>
                                        <span class="divisor"></span>
                                        <p>lorem ipsum</p>
                                    </li>
                                    <li>
                                        <strong>1500</strong>
                                        <span class="divisor"></span>
                                        <p>lorem ipsum</p>
                                    </li>
                                    <li>
                                        <strong>1500</strong>
                                        <span class="divisor"></span>
                                        <p>lorem ipsum</p>
                                    </li>
                                </ul>--}}
                            </div>
                            @if(!empty($banner_right->link))
                                </a>
                            @endif
                        </div>
                    @endif
                </div>
            </div>
        </section>


        <section class="home-location">

            <div class="container">
                <div class="adere-location-box row">
                    <div class="adere-location col-sm-8">
                        <img src="{{ asset('front/img/map.png')}}" alt="Mapa" class="map">

                        <img src="{{ asset('front/img/map-bullet.png')}}" alt="São Paulo" class="bullet sao-paulo">
                        <img src="{{ asset('front/img/map-bullet.png')}}" alt="São Paulo" class="bullet eua">
                        <img src="{{ asset('front/img/map-bullet.png')}}" alt="São Paulo" class="bullet africa1">
                        <img src="{{ asset('front/img/map-bullet.png')}}" alt="São Paulo" class="bullet africa2">
                    </div>
                    <div class="adere-location-info col-sm-4">
                        <strong>{{ trans('title.adere_no_mundo')}}</strong>
                        @if(!empty($sharedData->get('texts')->text_company_in_the_world))
                            <p>{{ $sharedData->get('texts')->text_company_in_the_world }}</p>
                        @endif
                        @if(!empty($sharedData->get('configs')->phone_world))
                            <div class="phone"><i class="fa fa-phone"></i> {{ $sharedData->get('configs')->phone_world }}</div>
                        @endif
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </section>


        <section class="home-posts">

            <div class="container">

                <header class="section-header">
                    <h2 class="section-title">{{ trans('title.ultimos_posts')}}</h2>
                    @if(!empty($sharedData->get('texts')->text_latest_posts))
                        <p class="section-header-info">{{ $sharedData->get('texts')->text_latest_posts }}</p>
                    @endif
                    <div class="divisor d3x"></div>
                </header>

                <div class="section-content">

                    <ul class="default-list default-article">
                        @foreach($items_posts as $item_post)
                            <li>
                                <figure>
                                    <a href="{{ url($sharedData->get('lang_locale').'/post/'.$item_post->slug) }}">
                                        <img src="{{ asset('_files/posts/'.$item_post->image) }}" alt="">
                                    </a>
                                </figure>
                                <div class="title-container">
                                    <div class="title">{{ !empty($item_post->title) ? $item_post->title : null }}</div>
                                </div>
                                {!! !empty($item_post->text) ? $item_post->text : null  !!}
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </section>

    </main>
@endsection