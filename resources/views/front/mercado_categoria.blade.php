@extends('front.layout.master')

@section('header_title', trans('title.mercados'))

@section('main-content')
    <main id="main" class="page-marca">
        <section id="page-header" class="red">
            <div class="container">
                <div class="breadcrumbs">
                    <a href="{{ url($sharedData->get('lang_locale')) }}">Home</a> <i class='fa fa-chevron-right'></i> <a href="" onclick="return false">Mercado</a>
                </div>
                <h1 class="page-title">
                    {{(!empty($get_item->title)) ? $get_item->title : null}}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">
                <div class="row">
                    @include('front.layout.partials.sidebar-mercados')
                    <div class="col-sm-9 content">

                        <div class="top-carousel owl-carousel">
                            @foreach($get_item->galleries as $item_gallery)
                                <div class="item">
                                    <img src="{{ asset('_files/mercados_galeria/'.$item_gallery->image) }}" alt="{{ $get_item->title }}">
                                </div>
                            @endforeach
                        </div>

                        <div class="box">
                            <h2 class="content-title h2">{{(!empty($get_item->title)) ? $get_item->title : null}}</h2>
                            {!! (!empty($get_item->description)) ? $get_item->description : null !!}
                        </div>
                        <p class="share-bar pull-right">
                            {{ trans('title.compartilhe') }}:
                            <a href="" class="share-item"><i class="fa fa-print"></i></a>
                            <a href="" class="share-item"><i class="fa fa-envelope"></i></a>
                            <a href="" class="share-item"><i class="fa fa-facebook"></i></a>
                            <a href="" class="share-item"><i class="fa fa-whatsapp"></i></a>
                        </p>
                        <div class="clearfix"></div>

                        {{--Arquivos Relacionados--}}
                        @include('front.layout.partials.archives')

                        {{--Conteúdos Relacionados--}}
                        @include('front.layout.partials.conteudos-relacionados')

                        {{--Produtos Relacionados--}}
                        @include('front.layout.partials.subcategoria_produtos_relacionados')

                        @include('front.layout.partials.banner-center')

                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->
@endsection