@extends('front.layout.master')

@section('header_title', trans('title.certificacoes'))

@section('main-content')
    <main id="main" class="page-responsabilidade">

        <section id="page-header" class="green">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i> <a href="">{{ trans('title.sobre_adere') }}</a>
                </div>
                <h1 class="page-title">
                    {{ trans('title.responsabilidade_social') }}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">

                <div class="row">
                    @include('front.layout.partials.sidebar-internal')
                    <div class="col-sm-9 content">

                        <div class="internal-carousel">
                            <div class="item">
                                <img src="{{ asset('_files/projetos_sociais/'.$item_project->image) }}" alt="Responsabilidade">
                            </div>
                        </div>

                        <div class="box">
                            <h2 class="content-title h2">{{ (!empty($item_project->title)) ? $item_project->title : null }}</h2>
                            {!! (!empty($item_project->text)) ? $item_project->text : null  !!}
                        </div>

                        <div class="box">
                            <h2 class="content-title h2">{{trans('title.videos')}}</h2>
                            <p class="content-title-extra">{!! (!empty($item_responsability->text_gallery)) ? $item_responsability->text_gallery : null  !!}</p>

                            <div class="row">
                                @foreach([1,2,3,4] as $i)
                                    @if(!empty($item_project['link_youtube_'.$i]))
                                        <div class="col-sm-6">
                                            <div data-href="{{$item_project['link_youtube_'.$i]}}" class="modal-play">
                                                <img src="https://img.youtube.com/vi/{{$item_project['link_youtube_'.$i]}}/0.jpg" alt="Video 1">
                                                <span class="overlay-play">
                                                    <i class="fa fa-youtube-play"></i>
                                                </span>
                                            </div>
                                        </div>
                                    @endif
                                 @endforeach
                            </div>
                            @include('front.layout.partials.modal-video')
                        </div>

                        <div class="box">
                            <div class="box">
                                <h2 class="content-title h2">{{ (!empty($item_responsability->title_projects)) ? $item_responsability->title_projects : null }}</h2>
                                <p class="content-title-extra">{{ (!empty($item_responsability->text_projects)) ? $item_responsability->text_projects : null }}</p>
                            </div>
                            @if(!empty($items_projects) && count($items_projects) > 0)
                                <ul class="default-list">
                                    @foreach($items_projects as $item_project)
                                        <li>
                                            <figure>
                                                <a href="{{ route('{locale}.responsabilidade_social.{slug}', array($sharedData->get('lang_locale'),$item_project->slug)) }}">
                                                    <img src="{{ asset('_files/projetos_sociais/'.$item_project->image)}}" alt="">
                                                </a>
                                            </figure>
                                            <div class="title-container">
                                                <div class="title">{{$item_project->title}}</div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->

@endsection