@extends('front.layout.master')

@section('header_title', trans('title.blog'))

@section('main-content')
    <main id="main" class="page-blog page-blog-item">

        <section id="page-header" class="blog">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="{{ url($sharedData->get('lang_locale')) }}">Home</a> <i class='fa fa-chevron-right'></i> <a href="{{ url($sharedData->get('lang_locale').'/blog/') }}">Blog</a>
                </div>
                <h1 class="page-title">
                    {{ !empty($item_post->title) ? $item_post->title : null }}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-9 content">
                        <section class="post">
                            <header class="post-header">
                                <h2 class="post-title h2">
                                    {{ !empty($item_post->title) ? $item_post->title : null }}
                                </h2>
                                <figure class="post-image">
                                    <img src="{{asset('_files/posts/'.$item_post->image)}}" alt="Institucional">
                                </figure>
                                <div class="post-info">
                                    <span class="post-date">{{Carbon\Carbon::parse($item_post->created_at)->format('d m Y')}}</span>
                                    @if(count($items_post_comments) > 0)
                                        / <a href="#comments" class="post-comments-count">{{ count($items_post_comments) }} Comentários</a>
                                    @endif
                                </div>
                            </header>
                            <article class="post-article post-excerpt">
                                {!! $item_post->text !!}
                            </article>
                        </section>

                        <div class="box">
                            <h2 class="content-section-title">
                                <span>{{ trans('title.conteudos_relacionados') }}</span>
                            </h2>
                            <ul class="default-list default-article">
                                @if( !empty($items_posts->whereNotIn('id', $item_post->id)->take(3)) && count($items_posts->whereNotIn('id', $item_post->id)->take(3)) > 0)
                                    @foreach($items_posts->whereNotIn('id', $item_post->id)->take(3) as $item_related)
                                        <li>
                                            <figure>
                                                <a href="{{ url($sharedData->get('locale').'/post/'.$item_related->slug) }}">
                                                    <img src="{{asset('_files/posts/'.$item_related->image)}}" alt="">
                                                </a>
                                            </figure>
                                            <div class="title-container">
                                                <div class="title">{{ $item_related->title }}</div>
                                            </div>
                                            {!! $item_related->text !!}
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                        <div class="box" id="comentarios">
                            <h2 class="content-section-title">
                                <span>{{ trans('title.comentarios') }}</span>
                            </h2>

                            <form action="{{ route('{locale}.post.{post_slug}.send_comment', array('pt',$item_post->slug)) }}" method="POST" class="form-content">
                                {!! csrf_field() !!}
                                @if(Session::has('success'))
                                    <div class="send-success" role="alert">{!! Session::get('success') !!}</div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="send-error" role="alert">{!! Session::get('error') !!}</div>
                                @endif
                                @if (count($errors) > 0)
                                    <div class="send-error" role="alert">Falha no Envio! Verifique os campos obrigatórios e tente novamente.</div>
                                @endif

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group  {{ $errors->has('title') ? 'form-error' : '' }}">
                                            <label for="name">{{trans('title.nome')}}
                                                @if($errors->has('title'))
                                                    <span class="required">{{ $errors->first('title') }}</span>
                                                @endif
                                            </label>
                                            <input type="text" name="title" value="{{ old('title') }}" id="name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group {{ $errors->has('email') ? 'form-error' : '' }}">
                                            <label for="email">E-mail
                                                @if($errors->has('email'))
                                                    <span class="required">{{ $errors->first('email') }}</span>
                                                @endif
                                            </label>
                                            <input type="text" id="email" value="{{ old('email') }}" name="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group {{ $errors->has('text') ? 'form-error' : '' }}">
                                            <label for="text">{{trans('title.mensagem')}}
                                                @if($errors->has('text'))
                                                    <span class="required">{{ $errors->first('text') }}</span>
                                                @endif
                                            </label>
                                            <textarea name="text" id="text" cols="30" rows="10">{{ old('text') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="status" value="0">
                                        <input type="hidden" name="post_id" value="{{ $item_post->id }}">
                                        <button type="submit" class="btn btn-red pull-right">{{trans('title.enviar')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="box comments" id="comments">
                            @foreach($items_post_comments as $comment)
                                <div class="comment-item">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="title">
                                                {{$comment->title}}
                                                <span class="pull-right">
                                                    {{ Carbon\Carbon::parse($comment->created_at)->format('d/m/Y') . ' às ' . Carbon\Carbon::parse($comment->created_at)->format('H:m')  }}
                                                </span>
                                            </div>
                                            <p class="comment-content">{{ $comment->text }}</p>
                                            <a href="" data-id="{{ $comment->id }}" class="btn-open-answer answer">Responder <i class="fa fa-chevron-right"></i></a>
                                        </div>
                                    </div>
                                    @foreach($comment->answers as $comment_answer)
                                        <div class="comment-answer">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="title">
                                                        {{$comment_answer->title}}
                                                        <span class="pull-right">
                                                    {{ Carbon\Carbon::parse($comment_answer->created_at)->format('d/m/Y') . ' às ' . Carbon\Carbon::parse($comment_answer->created_at)->format('H:m')  }}
                                                </span>
                                                    </div>
                                                    <p class="comment-content">{{ $comment_answer->text }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                            @endforeach
                        </div>
                        <div class="modal modal-download">
                            <div class="modal-data">
                                <div class="modal-content modal-content-white">
                                    <h2 class="modal-title">
                                        Escreva sua resposta abaixo
                                    </h2>
                                    <form action="{{ route('{locale}.post.{post_slug}.send_answer', array('pt',$item_post->slug)) }}" id="formAnswer" data-slug="{{ $item_post->slug }}" method="POST" class="form-content">
                                        {!! csrf_field() !!}
                                        <div class="msg"></div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="name">{{trans('title.nome')}}
                                                        <span class="required">{{ trans('title.obrigatorio') }}</span>
                                                    </label>
                                                    <input type="text" name="title" value="{{ old('title') }}" class="input-name" id="name">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="email">E-mail
                                                        <span class="required">{{ trans('title.obrigatorio') }}</span>
                                                    </label>
                                                    <input type="text" id="email" value="{{ old('email') }}" class="input-email" name="email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="text">{{trans('title.mensagem')}}
                                                        <span class="required">{{ trans('title.obrigatorio') }}</span>
                                                    </label>
                                                    <textarea name="text" id="text" cols="30" rows="10" class="input-text">{{ old('text') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="hidden" name="status" value="0">
                                                <input type="hidden" name="post_comments_id" class="post_comments_id" value="">
                                                <button type="submit" class="btn btn-red pull-right">{{trans('title.enviar')}}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="overlay"></div>
                        </div>
                    </div>
                    @include ('front.layout.partials.sidebar-blog')
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->

@endsection