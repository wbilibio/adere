@extends('front.layout.master')

@section('header_title', trans('title.percerias'))

@section('main-content')
    <main id="main" class="page-banco-imagens">
        <section id="page-header" class="red">
            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i> <a href="">Banco de Imagens</a> <i class='fa fa-chevron-right'></i>
                </div>
                <h1 class="page-title">
                    GALERIA {{ !empty($item_category->title) ? $item_category->title : null }}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">
                <div class="row">
                    @include('front.layout.partials.sidebar-single')
                    <div class="col-sm-9 content">
                        <div class="box">
                            <h2 class="content-title h2">GALERIA {{ !empty($item_category->title) ? $item_category->title : null }}</h2>
                            <ul class="default-list default-imagem">
                                @foreach($items_image as $item_image)
                                    <li>
                                        <figure>
                                            <a href="{{ asset('_files/banco_de_imagens/'.$item_category->id.'/'.$item_image->image) }}" download>
                                                <img src="{{ asset('_files/banco_de_imagens/'.$item_category->id.'/'.$item_image->image_thumb) }}" alt="">
                                            </a>
                                        </figure>
                                        <p>{{ $item_image->title }}</p>
                                        <p class="text-right">
                                            <a href="{{ asset('_files/banco_de_imagens/'.$item_category->id.'/'.$item_image->image) }}" class="pull-right btn btn-transparent red" download>
                                                Download <i class="fa fa-download"></i>
                                            </a>
                                        </p>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->

@endsection