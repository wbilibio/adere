@if(!empty($get_item->files) && count($get_item->files) > 0)
    <div class="box">
    <h2 class="content-section-title">
        <span>{{ trans('title.arquivos_relacionados') }}</span>
    </h2>
    @foreach($get_item->files as $item_file)
        <div class="row box-list">
            <a href="{{ asset($get_item->local_file.$item_file->file) }}" class="open-modal-download">
                <div class="col-sm-10  col-xs-9 title">
                    <span>{{  $item_file->title }}</span>
                </div>
                <div class="col-sm-2  col-xs-3 link-red">
                    <span>Download <i class="fa fa-download"></i></span>
                </div>
            </a>
        </div>
    @endforeach
    <div class="modal modal-download">
        <div class="modal-data">
            <div class="modal-content modal-content-white">
                <h2 class="modal-title">
                    {{ trans('text.preencha_formulario') }}
                </h2>
                <form action="#" id="formDownload" class="form-content form-accept-file">
                    <div class="msg"><p></p></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">
                                    {{ trans('title.nome') }}
                                    <span class="required">{{ trans('title.obrigatorio') }}</span>
                                </label>
                                <input type="text" class="input-name" name="name">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">
                                    {{ trans('title.cargo') }}
                                </label>
                                <input type="text" name="office">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">
                                    {{ trans('title.empresa') }}
                                </label>
                                <input type="text" name="company">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">
                                    {{ trans('title.mercado_de_atuacao') }}
                                </label>
                                <input type="text" name="market">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">
                                    E-mail
                                    <span class="required">{{ trans('title.obrigatorio') }}</span>
                                </label>
                                <input type="text" class="input-email" name="email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <a href="" class="btn-close"><i class="fa fa-remove"></i> {{ trans('title.cancelar') }}</a>
                            <button type="submit" class="btn btn-red pull-right">{{ trans('title.download') }} <i class="fa fa-download"></i></button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="overlay"></div>
    </div>
</div>
@endif