<aside class="col-sm-3 page-sidebar">

    @include('front.layout.partials.box-services')
    <div class="box">
        @if(!empty($sharedData->get('banner_about')))
            @if(!empty($sharedData->get('banner_about')->link))<a href="{{ $sharedData->get('banner_about')->link }}">@endif
                <div class="banner">
                    <img src="{{ asset('_files/banners/'.$sharedData->get('banner_about')->image)}}" alt="" />
                </div>
                @if(!empty($sharedData->get('banner_about')->link))</a>@endif
        @endif
    </div>

</aside>