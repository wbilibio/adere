@if(!empty($get_item->galleries) && count($get_item->galleries) > 0)
    <div class="top-carousel solucoes-carousel owl-carousel">
        @foreach($get_item->galleries as $item_gallery)
            <div class="item">
                <img src="{{ asset('_files/familias_galeria/'.$item_gallery->image) }}" alt="{{ $get_item->title }}">
            </div>
        @endforeach
    </div>
@endif