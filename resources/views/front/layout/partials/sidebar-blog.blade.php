
<aside class="col-sm-3 page-sidebar">

    <div class="box">
        <div class="sidebar-title">
            Buscar
        </div>
        <div class="divisor"></div>


        <form action="" class="form-search box-bordered">
            <input type="text" name="s" placeholder="Digite Aqui">
            <button type="submit">
                <i class="fa fa-search"></i>
            </button>
        </form>
    </div>

    <div class="box">
        <div class="sidebar-title">
            Categoria
        </div>
        <div class="divisor"></div>

        <div class="box-bordered">
            <ul>
                @foreach($items_families as $item_family)
                    @if($item_family->visible == 'true')
                        <li>
                            <a href="{{ url($sharedData->get('lang_locale').'/blog/'.$item_family->slug) }}">{{ $item_family->title }}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
    <div class="box">
        @if(!empty($sharedData->get('banner_blog')))
            @if(!empty($sharedData->get('banner_blog')->link))<a href="{{ $sharedData->get('banner_blog')->link }}">@endif
                <div class="banner">
                    <img src="{{ asset('_files/banners/'.$sharedData->get('banner_blog')->image)}}" alt="" />
                </div>
            @if(!empty($sharedData->get('banner_blog')->link))</a>@endif
        @endif
    </div>

    <div class="box">
        <div class="sidebar-title">
            Posts mais Vistos
        </div>
        <div class="divisor"></div>
        <div class="box-bordered sidebar-list">
            @foreach($items_posts_most_viewed as $item)
                <div class="sidebar-list-item">
                    <figure class="image">
                        <a href="{{ url($sharedData->get('lang_locale').'/blog/'.$item->slug) }}">
                            <img src="{{asset('_files/posts/'.$item->image)}}" alt="Imagem" width="231" height="145">
                        </a>
                    </figure>
                    <a href="{{ url($sharedData->get('lang_locale').'/blog/'.$item->slug) }}" class="title">
                        {{ $item->title }}
                    </a>
                </div>
            @endforeach
        </div>

    </div>

</aside>