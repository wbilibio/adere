@if( !empty($get_item->family->products->whereNotIn('id', $get_item->id)->take(3)) && count($get_item->family->products->whereNotIn('id', $get_item->id)->take(3)) > 0)
    <div class="box">
        <h2 class="content-section-title">
            <span>{{ trans('title.produtos_relacionados') }}</span>
        </h2>

        <ul class="default-list">
            @foreach($get_item->family->products->whereNotIn('id', $get_item->id)->take(3) as $item_product)
                <li>
                    <figure>
                        <a href="{{url($sharedData->get('lang_locale').'/'.Request::segment(2).'/'.$get_item->slug.'/'. $item_product->family->slug . '/' .$item_product->slug )}}">
                            @if(!empty($item_product->galleries) && count($item_product->galleries) > 0)
                                <img src="{{asset('_files/produtos_galeria/'.$item_product->galleries->first()['image'])}}" alt="">
                            @else
                                <img src="{{asset('front/img/sem-foto.png')}}" alt="">
                            @endif
                        </a>
                    </figure>
                    <div class="title-container">
                        <div class="title">{{ !empty($item_product->title) ? $item_product->title : null }}</div>
                    </div>
                    @if(!empty($item_product->url_loja_virtual))
                        <a href="{{$item_product->url_loja_virtual}}" class="btn btn-red">Comprar <i class="fa fa-shopping-cart"></i></a>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
@endif