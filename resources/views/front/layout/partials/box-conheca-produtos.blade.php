<div class="box">
    <h2 class="content-section-title">
        <span>{{ trans('title.conheca_produtos') }}</span>
    </h2>

    @foreach($get_item->products as $item_product)
        <div class="row box-list">
            <div class="{{ !empty($item_product->url_loja_virtual) ? 'col-sm-8  col-xs-6' : 'col-sm-10 col-xs-8' }} title">
                <a href="{{url($sharedData->get('lang_locale').'/'.Request::segment(2).'/'.Request::segment(3).'/'.Request::segment(4).'/'.$item_product->slug)}}">{{(!empty($item_product->title)) ? $item_product->title : null}}</a>
            </div>
            @if(!empty($item_product->url_loja_virtual))
            <div class="col-sm-2 col-xs-3 link-red">
                <a href="{{$item_product->url_loja_virtual}}">Compre Aqui <i class="fa fa-shopping-cart"></i></a>
            </div>
            @endif
            <div class="col-sm-2 col-xs-3 link-black">
                <a href="{{url($sharedData->get('lang_locale').'/'.Request::segment(2).'/'.Request::segment(3).'/'.Request::segment(4).'/'.$item_product->slug)}}">Saiba mais <i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
    @endforeach

</div>