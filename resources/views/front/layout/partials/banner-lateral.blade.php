<div class="box">
    @if(!empty($banner_lateral))
        @if(!empty($banner_lateral->link))<a href="{{ $banner_lateral->link }}">@endif
            <div class="banner">
                <img src="{{ asset('_files/banners/'.$banner_lateral->image)}}" alt="" />
            </div>
            @if(!empty($banner_lateral->link))</a>@endif
    @endif
</div>