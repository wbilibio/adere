@if( !empty($items_posts) && count($items_posts) > 0)
    <div class="box">
        <h2 class="content-section-title">
            <span>{{ trans('title.conteudos_relacionados') }}</span>
        </h2>
        <ul class="default-list default-article">
            @foreach($items_posts as $item_post)
                <li>
                    <figure>
                        <a href="{{ url($sharedData->get('lang_locale').'/post/'.$item_post->slug) }}">
                            <img src="{{ asset('_files/posts/'.$item_post->image) }}" alt="">
                        </a>
                    </figure>
                    <div class="title-container">
                        <div class="title">{{ !empty($item_post->title) ? $item_post->title : null }}</div>
                    </div>
                    {!! !empty($item_post->text) ? $item_post->text : null  !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif