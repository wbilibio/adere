@if(!empty($get_item->videos) && count($get_item->videos) > 0)
    <div class="box">
        <h2 class="content-section-title">
            <span>{{ trans('title.como_aplicar') }}</span>
        </h2>

        <div class="row">
            @foreach($get_item->videos as $video)
            <div class="col-sm-4">
                <a href="{{$video->link_youtube}}?rel=0&amp;controls=0&amp;showinfo=0&autoplay=1" class="modal-play modal-play-3col">
                    <img src="{{asset('_files/produtos_videos/'.$video->image)}}" alt="Video 1">
                    <span class="overlay-play">
                    <i class="fa fa-youtube-play"></i>
                    </span>
                </a>
            </div>
            @endforeach
        </div>
        @include('front.layout.partials.modal-video')
    </div>
@endif