@if(!empty($get_item->galleries) && count($get_item->galleries) > 0)
    <div class="top-carousel solucoes-carousel owl-carousel">
        @foreach($get_item->galleries as $item_gallery)
            <div class="item">
                <img src="{{ asset('_files/produtos_galeria/'.$item_gallery->image) }}" alt="{{(!empty($get_item->title)) ? $get_item->title : null}}">
            </div>
        @endforeach
    </div>
@endif