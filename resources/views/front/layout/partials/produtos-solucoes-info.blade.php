<div class="solucoes-info {{ !empty($get_item->galleries) && count($get_item->galleries) > 0 ? 'content-middle' : 'content-total' }}">
    @if(!empty($get_item->mark))
        <div class="marca-logo">
            <img src="{{asset('_files/marcas/'.$get_item->mark->image)}}" alt="Logo">
        </div>
    @endif
    <h2 class="content-sub-title">{{(!empty($get_item->title)) ? $get_item->title : null}}</h2>
    {!! (!empty($get_item->description)) ? $get_item->description : null !!}
    @if(!empty($get_item->markets) && count($get_item->markets) > 0)
        <h2 class="content-sub-title">{{trans('title.mercados')}}</h2>
        <div class="list-tags">
            @foreach($get_item->markets as $market)
                <a href="{{ url($sharedData->get('lang_locale').'/mercados/'.$market->slug) }}" class="tag">{{ $market->title }}</a>
            @endforeach
        </div>
    @endif
    @if(!empty($get_item->applications) && count($get_item->applications) > 0)
        <h2 class="content-sub-title">{{trans('title.aplicacao')}}</h2>
        <div class="list-tags">
            @foreach($get_item->applications as $application)
                <a href="" onclick="return false" class="tag">{{ $application->title }}</a>
            @endforeach
        </div>
    @endif
    @if(!empty($get_item->url_loja_virtual))
        <a href="{{$get_item->url_loja_virtual}}" class="btn btn-red">Compre Aqui <i class="fa fa-shopping-cart"></i></a>
    @endif
</div>