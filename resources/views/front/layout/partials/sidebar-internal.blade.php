
<aside class="col-sm-3 page-sidebar">
    <div class="box">
        <div class="sidebar-title">
            {{ trans('title.buscar') }}
        </div>
        <div class="divisor"></div>
        <div class="msg-no-result msg error"><p>Página não encontrada.</p></div>
        <form action="" class="form-search box-bordered" onsubmit="return false" >
            <input type="text" id="input-search-company" placeholder="Digite Aqui">
            <button type="submit">
                <i class="fa fa-search"></i>
            </button>
        </form>
    </div>

    <div class="box">
        <div class="sidebar-title">
            {{ trans('title.sobre_adere') }}
        </div>
        <div class="divisor"></div>
        <div class="box-bordered">
            <ul class="sidebar-nav">
                <li {{ Request::segment(2) == "institucional" ? 'class=active' : '' }}>
                    <a href="{{url($sharedData->get('lang_locale').'/institucional')}}">{{ trans('title.institucional') }}</a>
                </li>
                <li {{ Request::segment(2) == "certificacoes" ? 'class=active' : '' }}>
                    <a href="{{url($sharedData->get('lang_locale').'/certificacoes')}}">{{ trans('title.certificacoes') }}</a>
                </li>
                <li {{ Request::segment(2) == "responsabilidade_social" ? 'class=active' : '' }}>
                    <a href="{{url($sharedData->get('lang_locale').'/responsabilidade_social')}}">{{ trans('title.responsabilidade_social') }}</a>
                </li>
                <li {{ Request::segment(2) == "parceria" ? 'class=active' : '' }}>
                    <a href="{{url($sharedData->get('lang_locale').'/parceria')}}">{{ trans('title.parceria') }}</a>
                </li>
                <li {{ Request::segment(2) == "produtos_sob_medida" ? 'class=active' : '' }}>
                    <a href="{{url($sharedData->get('lang_locale').'/produtos_sob_medida')}}">{{ trans('title.produtos_sob_medida') }}</a>
                </li>
                <li class="has-sub {{ Request::segment(2) == "nossas_marcas" ? 'active' : '' }}">
                    <div class="link">
                        <a href="" onclick="return false;">{{ trans('title.nossas_marcas') }}</a>
                        <i class="fa fa-chevron-right icon-set"></i>
                        <i class="fa fa-chevron-down icon-set"></i>
                    </div>
                    <ul class="sub">
                        @foreach($sharedData->get('marks') as $item_mark)
                            <li {{ Request::segment(3) == $item_mark->slug ? 'class=active' : '' }}>
                                <a href="{{url($sharedData->get('lang_locale').'/nossas_marcas/'.$item_mark->slug)}}">{{$item_mark->title}}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li {{ Request::segment(2) == "nossos_50_anos" ? 'class=active' : '' }}>
                    <a href="{{url($sharedData->get('lang_locale').'/nossos_50_anos')}}">{{ trans('title.nossos_50_anos') }}</a>
                </li>
            </ul>
        </div>
    </div>
    @include('front.layout.partials.box-services')
    <div class="box">
        @if(!empty($sharedData->get('banner_about')))
            @if(!empty($sharedData->get('banner_about')->link))<a href="{{ $sharedData->get('banner_about')->link }}">@endif
                <div class="banner">
                    <img src="{{ asset('_files/banners/'.$sharedData->get('banner_about')->image)}}" alt="" />
                </div>
                @if(!empty($sharedData->get('banner_about')->link))</a>@endif
        @endif
    </div>

</aside>