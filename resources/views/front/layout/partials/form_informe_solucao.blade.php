<div class="box">
    <h2 class="content-title h2">{{ trans('text.informe_solucao') }}</h2>
    <p class="content-title-extra">{{ (!empty($item_custom->text_solution)) ? $item_custom->text_solution : null }} </p>
    <form action="{{ route('{locale}.produtos_sob_medida.send_solution', array('pt')) }}" method="POST" class="form-content">
        {!! csrf_field() !!}
        @if(Session::has('success'))
            <div class="send-success" role="alert">{!! Session::get('success') !!}</div>
        @endif
        @if(Session::has('error'))
            <div class="send-error" role="alert">{!! Session::get('error') !!}</div>
        @endif
        @if (count($errors) > 0)
            <div class="send-error" role="alert">Falha no Envio! Verifique os campos obrigatórios e tente novamente.</div>
        @endif
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group {{ $errors->has('name') ? 'form-error' : '' }}">
                    <label for="">{{ trans('title.nome') }}
                        @if($errors->has('name'))
                            <span class="required">{{ $errors->first('name') }}</span>
                        @endif
                    </label>
                    <input type="text" value="{{ old('name') }}" name="name">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="">
                        {{ trans('title.cargo') }}
                    </label>
                    <input type="text" value="{{ old('office') }}" name="office">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="">
                        {{ trans('title.empresa') }}
                    </label>
                    <input type="text" value="{{ old('company') }}" name="company">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="">
                        {{ trans('title.mercado_de_atuacao') }}
                    </label>
                    <input type="text" value="{{ old('market_of_action') }}" name="market_of_action">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9">
                <div class="form-group  {{ $errors->has('email') ? 'form-error' : '' }}">
                    <label for="">E-mail
                        @if($errors->has('email'))
                            <span class="required">{{ $errors->first('email') }}</span>
                        @endif
                    </label>

                    <input type="text" value="{{ old('email') }}" name="email">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group  {{ $errors->has('phone') ? 'form-error' : '' }}">
                    <label for="">{{ trans('title.telefone') }}
                        @if($errors->has('phone'))
                            <span class="required">{{ trans('title.obrigatorio') }}</span>
                        @endif
                    </label>

                    <input type="text" value="{{ old('phone') }}" name="phone">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group  {{ $errors->has('subject') ? 'form-error' : '' }}">
                    <label for="">{{ trans('title.assunto') }}
                        @if($errors->has('subject'))
                            <span class="required">{{ trans('title.obrigatorio') }}</span>
                        @endif
                    </label>
                    <input type="text" value="{{ old('subject') }}" name="subject">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group  {{ $errors->has('text') ? 'form-error' : '' }}">
                    <label for="">{{ trans('title.mensagem') }}
                        @if($errors->has('text'))
                            <span class="required">{{ trans('title.obrigatorio') }}</span>
                        @endif
                    </label>
                    <textarea name="text" id="" cols="30" rows="10">{{ old('text') }}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-red pull-right">{{ trans('title.enviar') }}</button>
            </div>
        </div>
    </form>
</div>