<header id="header">
    <div class="container">

        <h1 class="pull-left logo h1">
            <a href="{{ url($sharedData->get('lang_locale')) }}">
                <img src="{{ asset('front/img/logo.png')}}" alt="Adere - Colou, tá colado.">
            </a>
        </h1>
        <div class="pull-right nav-lang">
            <ul>
                <li class="padding-left nav-item lang {{ Request::segment(1) == "pt" ? 'active' : '' }}">
                    <a href="{{url('/pt')}}">PT</a>
                </li>
                <li class="nav-item lang {{ Request::segment(1) == "en" ? 'active' : '' }}">
                    <a href="{{url('/en')}}">EN</a>
                </li>
                <li class="nav-item lang {{ Request::segment(1) == "es" ? 'active' : '' }}">
                    <a href="{{url('/es')}}">ES</a>
                </li>
            </ul>
        </div>
        <nav class="pull-right nav">
            <ul class="menu">
                <li class="nav-item nav-about">
                    <a href="" onclick="return false;">{{ trans('title.sobre_adere') }}<i class="fa fa-chevron-right show-mobile"></i></a>

                    <ul class="nav-institucional">
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/institucional')}}">{{ trans('title.institucional') }}</a>
                        </li>
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/certificacoes')}}">{{ trans('title.certificacoes') }}</a>
                        </li>
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/responsabilidade_social')}}">{{ trans('title.responsabilidade_social') }}</a>
                        </li>
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/parceria')}}">{{ trans('title.parceria') }}</a>
                        </li>
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/produtos_sob_medida')}}">{{ trans('title.produtos_sob_medida') }}</a>
                        </li>
                        <li class="has-sub ">
                            <a href="" onclick="return false;">{{ trans('title.nossas_marcas') }} <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-down"></i></a><i class="fa fa-chevron-right show-mobile"></i>

                            <ul class="sub">
                                @foreach($sharedData->get('marks') as $item_mark)
                                    <li>
                                        <a href="{{url($sharedData->get('lang_locale').'/nossas_marcas/'.$item_mark->slug)}}">{{ $item_mark->title }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/nossos_50_anos')}}">{{ trans('title.nossos_50_anos') }}</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item nav-solutions">
                    <a href="" onclick="return false">{{ trans('title.solucoes') }} <i class="fa fa-chevron-right show-mobile"></i></a>
                    <ul class="nav-solutions-items row">
                        @foreach($sharedData->get('product_types') as $key => $item_category)
                            @if($key == 0 || $key == 4 || $key == 7 || $key == 9)<li class="has-sub col-sm-3">@endif
                                <a href="" onclick="return false" class="title">{{(!empty($item_category->title)) ? $item_category->title : null}} <i class="fa fa-chevron-right show-mobile"></i></a>
                                <ul class="sub">
                                    @foreach($item_category->families as $key_two=>$item_subcategory)
                                        <li>
                                            <a href="{{ url($sharedData->get('lang_locale').'/solucoes/'.$item_category->slug.'/'.$item_subcategory->slug) }}">{{(!empty($item_subcategory->title)) ? $item_subcategory->title : null}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @if($key == 3 || $key == 6 || $key == 8 || $key == 11)</li>@endif
                        @endforeach
                        <a href="{{url($sharedData->get('lang_locale').'/banco_imagens')}}" class="title">{{ trans('title.banco_imagens') }}</a>
                        <a href="{{url($sharedData->get('lang_locale').'/produtos_sob_medida')}}" class="title">{{ trans('title.produtos_sob_medida') }}</a>
                        <a href="{{url($sharedData->get('lang_locale').'/faq')}}" class="title">{{ trans('title.perguntas_frequentes') }}</a>
                        @if(!empty($sharedData->get('banner_menu_solution')))
                            @if(!empty($sharedData->get('banner_menu_solution')->link))<a href="{{ $sharedData->get('banner_menu_solution')->link }}">@endif
                                <div class="banner">
                                    <img src="{{ asset('_files/banners/'.$sharedData->get('banner_menu_solution')->image)}}" alt="" />
                                </div>
                            @if(!empty($sharedData->get('banner_menu_solution')->link))</a>@endif
                        @endif
                    </ul>
                </li>
                <li class="nav-item nav-market">
                    <a href="" onclick="return false">{{ trans('title.mercados') }} <i class="fa fa-chevron-right show-mobile"></i></a>
                    <div class="nav-market-items ">
                        <ul class="sub pull-left">
                            @foreach($sharedData->get('markets') as $key => $market)
                                <li class="sub-has-sub {{$key == 0 ? 'active' : ''}}">
                                    <a href="{{url($sharedData->get('lang_locale').'/mercados/'.$market->slug)}}">
                                        {{ $market->title }} <i class="fa fa-chevron-right"></i> <i class="fa fa-chevron-right show-mobile"></i>
                                    </a>
                                    <div class="sub-sub">
                                        <ul class="pull-left">
                                            @foreach($sharedData->get('product_families')->getByMarket($market->id) as $family)
                                                <li><a href="{{url($sharedData->get('lang_locale').'/mercados/'.$market->slug.'/'.$family->slug)}}">{!! (!empty($family->title)) ? $family->title : null  !!}</a></li>
                                            @endforeach
                                        </ul>
                                        <a href="{{url($sharedData->get('lang_locale').'/mercados/'.$market->slug)}}" class="read-more-market">
                                            @if(!empty($market->icon))
                                                <img src="{{ asset('_files/mercados/'.$market->icon)}}" alt="" />
                                            @endif
                                            {!! trans('text.saiba_mais_automotivo') !!} {{(!empty($market->title)) ? $market->title : null}}
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                            @endforeach
                            <li>
                                <a href="{{url($sharedData->get('lang_locale').'/banco_imagens')}}">{{ trans('title.banco_imagens') }}</a>
                            </li>
                            <li>
                                <a href="{{url($sharedData->get('lang_locale').'/nossas_solucoes')}}">{{ trans('title.solucoes') }}</a>
                            </li>
                            <li>
                                <a href="{{url($sharedData->get('lang_locale').'/faq')}}">{{ trans('title.perguntas_frequentes') }}</a>
                            </li>
                        </ul>
                        <div class="pull-right">
                            @if(!empty($sharedData->get('banner_menu_market')))
                                @if(!empty($sharedData->get('banner_menu_market')->link))<a href="{{ $sharedData->get('banner_menu_market')->link }}">@endif
                                    <div class="banner">
                                        <img src="{{ asset('_files/banners/'.$sharedData->get('banner_menu_market')->image)}}" alt="" />
                                    </div>
                                @if(!empty($sharedData->get('banner_menu_market')->link))</a>@endif
                            @endif
                        </div>
                    </div>
                </li>
                @if(!empty($sharedData->get('configs')->link_store))
                    <li class="nav-item">
                        <a href="{{$sharedData->get('configs')->link_store}}">{{ trans('title.loja') }}</a>
                    </li>
                @endif
                <li class="nav-item">
                    <a href="{{url($sharedData->get('lang_locale').'/blog')}}">{{ trans('title.blog') }}</a>
                </li>
                <li class="nav-item">
                    <a href="{{url($sharedData->get('lang_locale').'/contato')}}">{{ trans('title.contato') }}</a>
                </li>
            </ul>
        </nav>
        <div class=" mobile-nav">Menu <i class="fa fa-bars"></i></div>
        <div class="clearfix"></div>

    </div>
</header>