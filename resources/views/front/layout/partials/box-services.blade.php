<div class="box">
    <div class="sidebar-title">
        {{ trans('title.servicos') }}
    </div>
    <div class="divisor"></div>
    <div class="box-bordered">
        <ul>
            <li>
                <a href="{{ url($sharedData->get('lang_locale').'/banco_de_imagens') }}"><i class="fa fa-image"></i> {{ trans('title.banco_imagens') }}</a>
            </li>
            <li>
                <a href="{{ url($sharedData->get('lang_locale').'/catalogo') }}"><i class="fa  fa-address-card"></i> {{ trans('title.catalogo') }}</a>
            </li>
            <li>
                <a href="{{ url($sharedData->get('lang_locale').'/faq') }}"><i class="fa fa-question-circle"></i> {{ trans('title.perguntas_frequentes') }}</a>
            </li>
        </ul>
    </div>
</div>