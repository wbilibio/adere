
<aside class="col-sm-3 page-sidebar">

    <div class="box">
        <div class="sidebar-title">
            Buscar
        </div>
        <div class="divisor"></div>


        <form action="" class="form-search box-bordered">
            <input type="text" name="s" placeholder="Digite Aqui">
            <button type="submit">
                <i class="fa fa-search"></i>
            </button>
        </form>
    </div>

    <div class="box">
        <div class="sidebar-title">
            Mercados
        </div>
        <div class="divisor"></div>

        <div class="box-bordered">
            <ul class="sidebar-nav">
                @foreach($sharedData->get('markets') as $key => $market)
                    <li class="has-sub {{ Request::segment(3) == $market->slug ? 'active' : '' }}">
                        <div class="link">
                            <a href="{{ url($sharedData->get('lang_locale').'/mercados/'.$market->slug) }}">{{ $market->title }}</a>
                            <i class="fa fa-chevron-right icon-set"></i>
                            <i class="fa fa-chevron-down icon-set"></i>
                        </div>

                        <ul class="sub">
                            @foreach($sharedData->get('product_families')->getByMarket($market->id) as $family)
                                <li {{ Request::segment(4) == $family->slug ? 'class=active' : '' }}><a href="{{url($sharedData->get('lang_locale').'/mercados/'.$market->slug.'/'.$family->slug)}}">{!! (!empty($family->title)) ? $family->title : null  !!}</a></li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    @include('front.layout.partials.box-services')

    @include('front.layout.partials.banner-lateral')


</aside>