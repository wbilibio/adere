<div class="box">
    @if(!empty($banner_center))
        @if(!empty($banner_center->link))<a href="{{ $banner_center->link }}">@endif
            <div class="banner">
                <img src="{{ asset('_files/banners/'.$banner_center->image)}}" alt="" />
            </div>
            @if(!empty($banner_center->link))</a>@endif
    @endif
</div>