<section id="newsletter">
    <div class="container">
        <div class="title">
            {{ trans('text.cadastro_newsletter') }}
        </div>

        <form action="#" id="formNewsletter" class="row">
            <div class="msg"><p></p></div>
            <div class="col-sm-5 nopadding-left">
                <label for="newsletter-name">{{ trans('title.seu_nome') }}:</label>
                <input type="text" name="name" id="newsletter-name">

            </div>
            <div class="col-sm-5">
                <label for="newsletter-email">{{ trans('title.seu_email') }}:</label>
                <input type="email" name="email" class="input-email" id="newsletter-email">
                <div class="required">{{ trans('title.campo_obrigatorio') }}</div>
            </div>
            <div class="col-sm-2">
                <button class="btn btn-red-active">{{ trans('title.cadastrar') }}</button>
            </div>
        </form>
    </div>
</section>
<footer id="footer">
    <div class="container">
        <nav class="nav-footer">
            <ul class="footer-list row">
                <li class="col-sm-3 col-xs-6">
                    <span class="title">{{ trans('title.sobre_adere')}}</span>
                    <ul class="footer-list-links">
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/institucional')}}">{{ trans('title.institucional') }}</a>
                        </li>
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/certificacoes')}}">{{ trans('title.certificacoes') }}</a>
                        </li>
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/responsabilidade_social')}}">{{ trans('title.responsabilidade_social') }}</a>
                        </li>
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/parceria')}}">{{ trans('title.parceria') }}</a>
                        </li>
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/produtos_sob_medida')}}">{{ trans('title.produtos_sob_medida') }}</a>
                        </li>
{{--                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/nossos_50_anos')}}">{{ trans('title.nossos_50_anos') }}</a>
                        </li>--}}
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/blog')}}">{{ trans('title.blog')}}</a>
                        </li>
                        @if(!empty($sharedData->get('configs')->link_store))
                            <li>
                                <a href="{{$sharedData->get('configs')->link_store}}">{{ trans('title.loja')}}</a>
                            </li>
                        @endif
                    </ul>
                </li>
                <li class="col-sm-3 col-xs-6">
                    <span class="title">{{ trans('title.mercados')}}</span>
                    <ul class="footer-list-links">
                        @foreach($sharedData->get('markets') as $key => $market)
                            <li><a href="{{url($sharedData->get('lang_locale').'/mercados/'.$market->slug)}}">{{ $market->title }}</a></li>
                        @endforeach
                    </ul>
                </li>

                <li class="col-sm-3 col-xs-6">
                    <span class="title">{{ trans('title.servicos')}}</span>
                    <ul class="footer-list-links">
                        @if(!empty($sharedData->get('configs')->link_2_via_boletos))
                            <li><a href="{{$sharedData->get('configs')->link_2_via_boletos}}">{{ trans('title.2_via_boleto')}}</a></li>
                        @endif
                        <li><a href="{{url($sharedData->get('lang_locale').'/banco_de_imagens')}}">{{ trans('title.banco_imagens')}}</a></li>
                    </ul>
                    <span class="title">{{ trans('title.siga_adere')}}</span>
                    @if(!empty($sharedData->get('configs')->link_facebook))
                        <a href="{{ $sharedData->get('configs')->link_facebook }}" target="_blank" rel="me" class="icon icon-facebook">&nbsp;</a>
                    @endif
                    @if(!empty($sharedData->get('configs')->link_twitter))
                        <a href="{{ $sharedData->get('configs')->link_twitter }}" target="_blank" rel="me" class="icon icon-twitter">&nbsp;</a>
                    @endif
                    @if(!empty($sharedData->get('configs')->link_linkedin))
                        <a href="{{ $sharedData->get('configs')->link_linkedin }}" target="_blank" rel="me" class="icon icon-linkedin">&nbsp;</a>
                    @endif
                    @if(!empty($sharedData->get('configs')->link_google_plus))
                        <a href="{{ $sharedData->get('configs')->link_google_plus }}" target="_blank" rel="me" class="icon icon-plus">&nbsp;</a>
                    @endif
                    @if(!empty($sharedData->get('configs')->link_youtube))
                        <a href="{{ $sharedData->get('configs')->link_youtube }}" target="_blank" rel="me" class="icon icon-youtube">&nbsp;</a>
                    @endif
                </li>
                <li class="col-sm-3 col-xs-6">
                    <span class="title">{{ trans('title.fale_com_adere')}}</span>
                    <ul class="footer-list-links">
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/contato')}}">{{ trans('title.contato')}}</a>
                        </li>
                        <li>
                            <a href="{{url($sharedData->get('lang_locale').'/faq')}}">{{ trans('title.perguntas_frequentes')}}</a>
                        </li>
                    </ul>
                    <div class="sac">
                        SAC
                        @if(!empty($sharedData->get('configs')->phone_sac))
                            <div class="sac-phone">{{ $sharedData->get('configs')->phone_sac }}</div>
                        @endif
                        @if(!empty($sharedData->get('configs')->office_hours))
                            <div class="sac-hours">
                                {{ $sharedData->get('configs')->office_hours }}
                            </div>
                        @endif
                    </div>
                    <div class="sac">
                        PABX <br>
                        @if(!empty($sharedData->get('configs')->phone_pabx))
                            {{ $sharedData->get('configs')->phone_pabx }}
                        @endif
                    </div>
                    <div class="sac sac-2">
                        FAX <br>
                        @if(!empty($sharedData->get('configs')->phone_fax))
                            {{ $sharedData->get('configs')->phone_fax }}
                        @endif
                    </div>
                </li>
            </ul>
        </nav>
    </div>
</footer>
<section id="copy">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <a href="">{{ trans('title.politica_privacidade')}}</a>
            </div>
            <div class="col-sm-6">
                &copy; {{ Carbon\Carbon::now()->format('Y') .' '. trans('text.direitos_reservados')}}
            </div>
            <div class="col-sm-4">
                <img src="{{ asset('front/img/iso-9001.png') }}" alt="ISO 9001">
                <img src="{{ asset('front/img/iso-14001.png') }}" alt="ISO 14001">
                <img src="{{ asset('front/img/logo.png') }}" alt="">
            </div>
        </div>
    </div>
</section>