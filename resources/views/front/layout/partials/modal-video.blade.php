<div class="modal-video modal">
    <div class="modal-data">
        <div class="close"><i class="fa fa-remove"></i> {{trans('title.fechar')}}</div>
        <div class="modal-content">
            <iframe width="832" height="518" src="" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="overlay"></div>
</div>