
<aside class="col-sm-3 page-sidebar">

    <div class="box">
        <div class="sidebar-title">
            Telefones
        </div>
        <div class="divisor"></div>

        <div class="box-bordered">
            <div class="sac">
                SAC
                @if(!empty($sharedData->get('configs')->phone_sac))
                    <div class="sac-phone">{{ $sharedData->get('configs')->phone_sac }}</div>
                @endif
                @if(!empty($sharedData->get('configs')->office_hours))
                    <div class="sac-hours">
                        {{ $sharedData->get('configs')->office_hours }}
                    </div>
                @endif
            </div>
            <div class="sac sac-2">
                <strong>PABX</strong>
                @if(!empty($sharedData->get('configs')->phone_pabx))
                    {{ $sharedData->get('configs')->phone_pabx }}
                @endif
            </div>
            <div class="sac sac-2">
                <strong>FAX</strong>
                @if(!empty($sharedData->get('configs')->phone_fax))
                    {{ $sharedData->get('configs')->phone_fax }}
                @endif
            </div>
        </div>
    </div>

    <div class="box">
        <div class="sidebar-title">
            Endereço
        </div>
        <div class="divisor"></div>
        @if(!empty($sharedData->get('configs')->address))
            <div class="box-bordered t-contact">
                {!! $sharedData->get('configs')->address !!}
            </div>
        @endif

    </div>
    <div class="box">
        <div class="box-bordered">
            @if(!empty($sharedData->get('configs')->link_2_via_boletos))
                <a href="{{$sharedData->get('configs')->link_2_via_boletos}}" class="box-link">
                    <i class="fa fa-barcode"></i> {{ trans('title.2_via_boleto')}}
                </a>
            @endif
        </div>
    </div>
    {{--<div class="box">
        <div class="box-bordered">
            <a href="" class="box-link">
                <i class="fa fa-user"></i> Cadastre seu Currículo
            </a>
        </div>
    </div>--}}
</aside>