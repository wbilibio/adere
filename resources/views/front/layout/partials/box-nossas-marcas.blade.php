<div class="box">
    <h2 class="content-section-title">
        <span>{{ trans('title.nossas_marcas') }}</span>
    </h2>
    @if(!empty($sharedData->get('texts')->text_our_brands))
        <p class="content-title-extra">{{ $sharedData->get('texts')->text_our_brands }}</p>
    @endif
    <div class="brand-list">
        @foreach($sharedData->get('marks') as $item_mark)
            <div class="brand-item">
                <a href="{{url($sharedData->get('lang_locale').'/nossas_marcas/'.$item_mark->slug)}}">
                    <img src="{{ asset('_files/marcas/'.$item_mark->image) }}" alt="">
                </a>
            </div>
        @endforeach
    </div>
</div>