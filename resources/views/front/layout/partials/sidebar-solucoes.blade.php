<aside class="col-sm-3 page-sidebar">

    <div class="box">
        <div class="sidebar-title">
            Buscar
        </div>
        <div class="divisor"></div>


        <form action="" class="form-search box-bordered">
            <input type="text" name="s" placeholder="Digite Aqui">
            <button type="submit">
                <i class="fa fa-search"></i>
            </button>
        </form>
    </div>

    <div class="box">
        <div class="sidebar-title">
            Soluções
        </div>
        <div class="divisor"></div>

        <div class="box-bordered">
            <ul class="sidebar-nav">
                @foreach($sharedData->get('product_types') as $key => $item_category)
                    <li class="has-sub {{ Request::segment(3) == $item_category->slug ? 'active' : '' }}">
                        <div class="link">
                            <a href="" onclick="return false">{{(!empty($item_category->title)) ? $item_category->title : null}}</a>
                            <i class="fa fa-chevron-right icon-set"></i>
                            <i class="fa fa-chevron-down icon-set"></i>
                        </div>

                        <ul class="sub">
                            @foreach($item_category->families as $key_two=>$item_subcategory)
                                <li {{ Request::segment(4) == $item_subcategory->slug ? 'class=active' : '' }}>
                                    <a href="{{ url($sharedData->get('lang_locale').'/solucoes/'.$item_category->slug.'/'.$item_subcategory->slug) }}">{{(!empty($item_subcategory->title)) ? $item_subcategory->title : null}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    @include('front.layout.partials.box-services')

    @include('front.layout.partials.banner-lateral')

</aside>