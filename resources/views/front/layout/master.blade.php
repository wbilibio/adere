<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Adere - @yield('header_title','Titulo da página')</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('front/css/main.css?v='.\Carbon\Carbon::now()->format('YmdHis'))}}">

    <meta name="viewport" content="initial-scale=1.0, width=device-width, user-scalable=yes, minimum-scale=1.0, maximum-scale=2.0">

    <link rel="shortcut icon" href="{{ asset('front/img/favicon.gif') }}">

</head>
<body>
    @if(!empty($sharedData->get('configs')->google_analytics))
        {!! $sharedData->get('configs')->google_analytics !!}
    @endif

    @include('front.layout.partials.header')
    @yield('main-content')
    @include('front.layout.partials.footer')
    @section('scripts')
        @include('front.layout.partials.scripts')
    @show
</body>
</html>
