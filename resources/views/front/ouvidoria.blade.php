@extends('front.layout.master')

@section('header_title', trans('title.ouvidoria'))

@section('main-content')
    <main id="main" class="page-ouvidoria">

        <section id="page-header" class="red">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i> <a href="">{{ trans('title.sobre_adere') }}</a>
                </div>
                <h1 class="page-title">
                    {{ trans('title.ouvidoria') }}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">

                <div class="row">
                    @include('front.layout.partials.sidebar-contato')
                    <div class="col-sm-9 content">

                        <div class="box">
                            <div class="row">
                                <div class="col-sm-10">
                                    <h2 class="content-title h2">{{ trans('title.ouvidoria') }}</h2>
                                </div>
                                <div class="col-sm-2">
                                    <a href="{{url($sharedData->get('lang_locale').'/contato')}}" class="btn btn-full">{{ trans('title.contato') }}</a>
                                </div>
                            </div>
                            @if(!empty($sharedData->get('texts')->text_ombudsman))
                                <p>{{ $sharedData->get('texts')->text_ombudsman }}</p>
                            @endif
                            <form action="{{ route('{locale}.ouvidoria.send', array('pt')) }}" class="form-content">
                                {!! csrf_field() !!}
                                @if(Session::has('success'))
                                    <div class="send-success" role="alert">{!! Session::get('success') !!}</div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="send-error" role="alert">{!! Session::get('error') !!}</div>
                                @endif
                                @if (count($errors) > 0)
                                    <div class="send-error" role="alert">Falha no Envio! Verifique os campos obrigatórios e tente novamente.</div>
                                @endif
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group {{ $errors->has('name') ? 'form-error' : '' }}">
                                            <label for="">{{ trans('title.nome') }}
                                                @if($errors->has('name'))
                                                    <span class="required">{{ $errors->first('name') }}</span>
                                                @endif
                                            </label>
                                            <input type="text" value="{{ old('name') }}" name="name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group {{ $errors->has('email') ? 'form-error' : '' }}">
                                            <label for="">E-mail
                                                @if($errors->has('email'))
                                                    <span class="required">{{ $errors->first('email') }}</span>
                                                @endif
                                            </label>
                                            <input type="text" value="{{ old('email') }}" name="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group {{ $errors->has('text') ? 'form-error' : '' }}">
                                            <label for="">{{ trans('title.mensagem') }}
                                                @if($errors->has('text'))
                                                    <span class="required">{{ trans('title.obrigatorio') }}</span>
                                                @endif
                                            </label>
                                            <textarea name="text" id="" cols="30" rows="10">{{ old('text') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-red pull-right">Enviar</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->
@endsection