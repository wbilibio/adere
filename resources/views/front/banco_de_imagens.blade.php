@extends('front.layout.master')

@section('header_title', trans('title.banco_de_imagens'))

@section('main-content')
    <main id="main" class="page-banco-imagens">

        <section id="page-header" class="red">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i>
                </div>
                <h1 class="page-title">
                    Banco de imagens
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">
                <div class="row">
                    @include('front.layout.partials.sidebar-single')
                    <div class="col-sm-9 content">
                        <div class="box">
                            <h2 class="content-title h2">EFETUE DOWNLOAD DAS IMAGENS</h2>
                            <ul class="default-list default-imagem">
                                @foreach($items_image as $item_image)
                                    <li>
                                        <figure>
                                            <a href="{{ url($sharedData->get('lang_locale').'/banco_de_imagens/'.$item_image->slug) }}">
                                                <img src="{{ asset('_files/banco_de_imagens/'.$item_image->image) }}" alt="">
                                            </a>
                                        </figure>
                                        <p>{{ $item_image->title }}</p>
                                        <p class="text-center btn-galeria" style="">
                                            <a href="{{ url($sharedData->get('lang_locale').'/banco_de_imagens/'.$item_image->slug) }}" class="btn btn-red red">
                                                {{ trans('title.veja_galeria') }}
                                            </a>
                                        </p>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main><!-- /#main.page-institucional -->
@endsection