@extends('front.layout.master')

@section('header_title', trans('title.produtos_sob_medida'))

@section('main-content')
    <main id="main" class="page-institucional">
        <section id="page-header" class="red">
            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i> <a href="">{{ trans('title.sobre_adere') }}</a>
                </div>
                <h1 class="page-title">
                    {{ trans('title.produtos_sob_medida') }}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">
                <div class="row">
                    @include('front.layout.partials.sidebar-internal')
                    <div class="col-sm-9 content">
                        @if(!empty($items_gallery) && count($items_gallery) > 0)
                            <div class="top-carousel owl-carousel">
                                @foreach($items_gallery as $item_gallery)
                                    <div class="item">
                                        <img src="{{ asset('_files/produtos_sob_medida/'.$item_gallery->image) }}" alt="{{ trans('title.produtos_sob_medida') }}">
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        <div class="box">
                            <h2 class="content-title h2">{{ (!empty($item_custom->title)) ? $item_custom->title : null }}</h2>
                            {!! (!empty($item_custom->text)) ? $item_custom->text : null  !!}
                        </div>
                    </div>
                    @include('front.layout.partials.form_informe_solucao')
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->
@endsection