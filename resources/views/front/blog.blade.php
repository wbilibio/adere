@extends('front.layout.master')

@section('header_title', trans('title.blog'))

@section('main-content')
    <main id="main" class="page-blog">

        <section id="page-header" class="blog">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="{{ url($sharedData->get('lang_locale')) }}">Home</a>
                    @if(!empty($category))
                        <i class='fa fa-chevron-right'></i> <a href="{{ url($sharedData->get('lang_locale').'/blog/') }}">Blog</a>
                    @endif
                </div>
                <h1 class="page-title">
                    {{ !empty($category) ? $category->title : 'Blog' }}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-9 content">
                        @foreach($items_posts as $item_post)
                            <section class="post">
                                <header class="post-header">
                                    <figure class="post-image">
                                        <a href="{{ url($sharedData->get('lang_locale').'/post/'.$item_post->slug) }}">
                                            <img src="{{asset('_files/posts/'.$item_post->image)}}" alt="Institucional">
                                        </a>
                                    </figure>
                                    <div class="post-info">
                                        <span class="post-date">{{Carbon\Carbon::parse($item_post->created_at)->format('d m Y')}}</span> / <a href="{{ url($sharedData->get('locale').'/post/'.$item_post->slug) }}" class="post-comments-count">{{ count($item_post->comments->where('status',1)) }} Comments</a>
                                    </div>
                                    <h2 class="post-title h2">
                                        <a href="{{ url($sharedData->get('lang_locale').'/post/'.$item_post->slug) }}">
                                            {{ $item_post->title }}
                                        </a>
                                    </h2>
                                </header>
                                <article class="post-article post-excerpt">
                                    {!! $item_post->text !!}
                                    <a href="{{ url($sharedData->get('lang_locale').'/post/'.$item_post->slug) }}" class="post-link more">Continue lendo <i class="fa fa-chevron-right"></i></a>
                                </article>
                            </section>
                        @endforeach
                        {{--<div class="navigation">
                            <a href="" class="link-prev"><i class="fa fa-chevron-left"></i> Anterior</a>

                            <ul class="navigation-pages">
                                <li>
                                    <a href="" class="navigation-item">1</a>
                                </li>
                                <li>
                                    <a href="" class="navigation-item">2</a>
                                </li>
                                <li>
                                    <a href="" class="navigation-item">3</a>
                                </li>
                                <li>
                                    <a href="" class="navigation-item">4</a>
                                </li>
                                <li>
                                    <a href="" class="navigation-item active">5</a>
                                </li>
                                <li>
                                    <a href="" class="navigation-item">6</a>
                                </li>
                                <li>
                                    <a href="" class="navigation-item">7</a>
                                </li>
                                <li>
                                    <a href="" class="navigation-item">8</a>
                                </li>
                                <li>
                                    <a href="" class="navigation-item">9</a>
                                </li>
                            </ul>

                            <a href="" class="link-next">Próximo <i class="fa fa-chevron-right"></i></a>
                        </div>--}}
                    </div>

                    @include ('front.layout.partials.sidebar-blog')
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->
@endsection