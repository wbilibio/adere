@extends('front.layout.master')

@section('header_title', trans('title.certificacoes'))

@section('main-content')
    <main id="main" class="page-contato">

        <section id="page-header" class="red">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i> <a href="">{{ trans('title.sobre_adere') }}</a>
                </div>
                <h1 class="page-title">
                    {{ trans('title.contato') }}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">
                <div class="row">
                    @include('front/layout/partials/sidebar-contato')
                    <div class="col-sm-9 content">
                        <div class="box">
                            <div class="row">
                                <div class="col-sm-10">
                                    <h2 class="content-title h2">{{ trans('title.entre_em_contato') }}</h2>
                                    <p class="content-title-extra">{{ trans('text.text_contato') }} </p>
                                </div>
                                <div class="col-sm-2">
                                    <a href="{{url($sharedData->get('lang_locale').'/ouvidoria')}}" class="btn btn-full">Ouvidoria</a>
                                </div>
                            </div>
                            <form action="{{ route('{locale}.contato.send', array('pt')) }}" method="POST" action="#" class="form-content">
                                {!! csrf_field() !!}
                                @if(Session::has('success'))
                                    <div class="send-success" role="alert">{!! Session::get('success') !!}</div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="send-error" role="alert">{!! Session::get('error') !!}</div>
                                @endif
                                @if (count($errors) > 0)
                                    <div class="send-error" role="alert">Falha no Envio! Verifique os campos obrigatórios e tente novamente.</div>
                                @endif
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group {{ $errors->has('name') ? 'form-error' : '' }}">
                                            <label for="">{{ trans('title.nome') }}
                                                @if($errors->has('name'))
                                                    <span class="required">{{ $errors->first('name') }}</span>
                                                @endif
                                            </label>
                                            <input type="text" value="{{ old('name') }}" name="name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">
                                                {{ trans('title.cargo') }}
                                            </label>
                                            <input type="text" value="{{ old('office') }}" name="office">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="">
                                                {{ trans('title.empresa') }}
                                            </label>
                                            <input type="text" value="{{ old('company') }}" name="company">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="">
                                                {{ trans('title.setor') }}
                                            </label>
                                            <input name="sector" value="{{ old('sector') }}" type="text">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="">{{ trans('title.telefone') }}</label>

                                            <input type="text" value="{{ old('phone') }}" name="phone">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group {{ $errors->has('email') ? 'form-error' : '' }}">
                                            <label for="">E-mail
                                                @if($errors->has('email'))
                                                    <span class="required">{{ $errors->first('email') }}</span>
                                                @endif
                                            </label>
                                            <input type="text" value="{{ old('email') }}" name="email">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group {{ $errors->has('area') ? 'form-error' : '' }}">
                                            <label for="">
                                                {{ trans('title.selecione_area') }}
                                                @if($errors->has('area'))
                                                    <span class="required">{{ $errors->first('area') }}</span>
                                                @endif
                                            </label>
                                            <select name="area" id="" class="select2" data-class="form-select2" data-searchmin="99999">

                                                <option value="rh">RH</option>
                                                <option value="comercial">Comercial</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group  {{ $errors->has('subject') ? 'form-error' : '' }}">
                                            <label for="">{{ trans('title.assunto') }}
                                                @if($errors->has('subject'))
                                                    <span class="required">{{ $errors->first('subject') }}</span>
                                                @endif
                                            </label>
                                            <input type="text" value="{{ old('subject') }}" name="subject">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group  {{ $errors->has('text') ? 'form-error' : '' }}">
                                            <label for="">{{ trans('title.mensagem') }}
                                                @if($errors->has('text'))
                                                    <span class="required">{{ $errors->first('text') }}</span>
                                                @endif
                                            </label>
                                            <textarea name="text" id="" cols="30" rows="10">{{ old('text') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-red pull-right">{{ trans('title.enviar') }}</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->


@endsection