@extends('front.layout.master')

@section('header_title', trans('title.institucional'))

@section('main-content')
    <main id="main" class="page-institucional">

        <section id="page-header" class="red">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i> <a href="">{{ trans('title.sobre_adere') }}</a>
                </div>
                <h1 class="page-title">
                    {{ trans('title.institucional') }}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">

                <div class="row">
                    @include('front.layout.partials.sidebar-internal')
                    <div class="col-sm-9 content">
                        @if(!empty($items_gallery) && count($items_gallery) > 0)
                            <div class="top-carousel owl-carousel">
                                @foreach($items_gallery as $item_gallery)
                                    <div class="item">
                                        <img src="{{ asset('_files/institucional_galeria/'.$item_gallery->image) }}" alt="Institucional">
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        <div class="box">
                            <h2 class="content-title h2">{{(!empty($item_institucional->title)) ? $item_institucional->title : null}}</h2>
                            {!! (!empty($item_institucional->text)) ? $item_institucional->text : null  !!}
                            <div class="clearfix"></div>
                        </div>
                        @if(!empty($items_concepts))
                            <div class="box">
                                <h2 class="content-title h2">{{ trans('title.conceitos_estrategicos') }}</h2>
                                <table class="table-conceitos">
                                    @foreach($items_concepts as $key=>$item_concept)
                                        <tr>
                                            <td class="image">
                                                <img src="{{ asset('_files/institucional_conceitos/'.$item_concept->image) }}" alt="Negócio">
                                            </td>
                                            <td>
                                                <h4>{{ $item_concept->title }}</h4>
                                                <p>{!! $item_concept->text  !!}</p>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        @endif

                        <div class="box adere-location-box">
                            <div class="adere-location">
                                <img src="{{ asset('front/img/map.png') }}" alt="Mapa" class="map">

                                <img src="{{ asset('front/img/map-bullet.png') }}" alt="São Paulo" class="bullet sao-paulo">
                                <img src="{{ asset('front/img/map-bullet.png') }}" alt="São Paulo" class="bullet eua">
                                <img src="{{ asset('front/img/map-bullet.png') }}" alt="São Paulo" class="bullet africa1">
                                <img src="{{ asset('front/img/map-bullet.png') }}" alt="São Paulo" class="bullet africa2">
                            </div>
                            <div class="adere-location-info">
                                <strong>{{ trans('title.adere_no_mundo') }}</strong>
                                <p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipo e de impressos</p>

                                <div class="phone"><i class="fa fa-phone"></i> 0500.701.2903</div>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->
@endsection