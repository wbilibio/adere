@extends('front.layout.master')

@section('header_title', trans('title.certificacoes'))

@section('main-content')
    <main id="main" class="page-certificacoes">

        <section id="page-header" class="red">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i> <a href="">{{ trans('title.sobre_adere') }}</a>
                </div>
                <h1 class="page-title">
                    {{ trans('title.certificacoes') }}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">

                <div class="row">
                    @include('front.layout.partials.sidebar-internal')
                    <div class="col-sm-9 content">
                        <div class="box">
                            @foreach($items_certification as $item_certification)
                                <h2 class="content-title h2">{{ $item_certification->title }}</h2>
                                <table class="table-certificacoes">
                                    <tr>
                                        <td class="image">
                                            <img src="{{ asset('_files/certificacoes/'.$item_certification->image) }}" alt="{{ $item_certification->title }}">
                                        </td>
                                        <td>
                                            {!! $item_certification->text !!}
                                            <a href="{{ asset('_files/certificacoes/'.$item_certification->pdf) }}" class="btn" download>Download Certificado <i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                </table>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->

@endsection