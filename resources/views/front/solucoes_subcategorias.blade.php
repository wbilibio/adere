@extends('front.layout.master')

@section('header_title', trans('title.solucoes'))

@section('main-content')
    <main id="main" class="page-solucoes">

        <section id="page-header" class="red">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i> <a href="">{{ trans('title.solucoes') }}</a> <i class='fa fa-chevron-right'></i> <a href="">{{ !empty($get_item->type->title) ? $get_item->type->title : null }}</a>
                </div>
                <h1 class="page-title">
                    {{ !empty($get_item->title) ? $get_item->title : null }}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">

                <div class="row">
                    @include('front.layout.partials.sidebar-solucoes')
                    <div class="col-sm-9 content">
                        @include('front.layout.partials.galeria-subcategorias')
                        <div class="solucoes-info {{ !empty($get_item->galleries) && count($get_item->galleries) > 0 ? 'content-middle' : 'content-total' }}">
                            <h2 class="content-title h2">{{ !empty($get_item->title) ? $get_item->title : null }}</h2>
                            {!! !empty($get_item->description) ? $get_item->description : null  !!}

                            <p class="share-bar">
                                {{ trans('title.compartilhe') }}:
                                <a href="" class="share-item"><i class="fa fa-print"></i></a>
                                <a href="" class="share-item"><i class="fa fa-envelope"></i></a>
                                <a href="" class="share-item"><i class="fa fa-facebook"></i></a>
                                <a href="" class="share-item"><i class="fa fa-whatsapp"></i></a>
                            </p>
                        </div>


                        <div class="clearfix"></div>

                        @include('front.layout.partials.box-conheca-produtos')

                        @include('front.layout.partials.box-nossas-marcas')

                        @include('front.layout.partials.banner-center')

                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->
@endsection