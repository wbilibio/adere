@extends('front.layout.master')

@section('header_title', trans('title.solucoes'))

@section('main-content')
    <main id="main" class="page-solucoes page-solucoes-item">

        <section id="page-header" class="red">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i>
                    <a href="" onclick="return false;">Soluções</a> <i class='fa fa-chevron-right'></i>
                    <a href="" onclick="return false;">{{ !empty($get_item->family->type->title) ? $get_item->family->type->title : null }}</a> <i class='fa fa-chevron-right'></i>
                    <a href="{{ url($sharedData->get('lang_locale').'/solucoes/'.$get_item->family->type->slug.'/'.$get_item->family->slug) }}">{{ !empty($get_item->family->title) ? $get_item->family->title : null }}</a>
                </div>
                <h1 class="page-title">{{(!empty($get_item->title)) ? $get_item->title : null}}</h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">

                <div class="row">
                    @include('front.layout.partials.sidebar-solucoes')
                    <div class="col-sm-9 content">
                        @include('front.layout.partials.galeria-produtos')
                        @include('front.layout.partials.produtos-solucoes-info')
                        <div class="clearfix"></div>
                        <p class="share-bar">
                            {{ trans('title.compartilhe') }}:
                            <a href="" class="share-item"><i class="fa fa-print"></i></a>
                            <a href="" class="share-item"><i class="fa fa-envelope"></i></a>
                            <a href="" class="share-item"><i class="fa fa-facebook"></i></a>
                            <a href="" class="share-item"><i class="fa fa-whatsapp"></i></a>
                        </p>

                        @include('front.layout.partials.produtos-videos')

                        @include('front.layout.partials.archives')

                        @include('front.layout.partials.conteudos-relacionados')

                        @include('front.layout.partials.produtos-relacionados')

                        @include('front.layout.partials.banner-center')

                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
    </main><!-- /#main.page-institucional -->
@endsection