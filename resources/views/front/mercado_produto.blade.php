@extends('front.layout.master')

@section('header_title', trans('title.mercados'). '-' . (!empty($item_market_subcategory->title)) ? $item_market_subcategory->title : null . '-' . (!empty($get_item->title)) ? $get_item->title : null)

@section('main-content')
    <main id="main" class="page-solucoes  page-mercado-item">

        <section id="page-header" class="green">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i> <a href="">Mercado</a> <i class='fa fa-chevron-right'></i> <a href="">{{(!empty($item_market->title)) ? $item_market->title : null}}</a> <i class='fa fa-chevron-right'></i> <a href="">{{(!empty($item_market_subcategory->title)) ? $item_market_subcategory->title : null}}</a>
                </div>
                <h1 class="page-title">{{(!empty($get_item->title)) ? $get_item->title : null}}</h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">

                <div class="row">
                    @include('front.layout.partials.sidebar-mercados')
                    <div class="col-sm-9 content">
                        @include('front.layout.partials.galeria-produtos')
                        @include('front.layout.partials.produtos-solucoes-info')
                        <div class="clearfix"></div>
                        <p class="share-bar">
                            {{ trans('title.compartilhe') }}:
                            <a href="" class="share-item"><i class="fa fa-print"></i></a>
                            <a href="" class="share-item"><i class="fa fa-envelope"></i></a>
                            <a href="" class="share-item"><i class="fa fa-facebook"></i></a>
                            <a href="" class="share-item"><i class="fa fa-whatsapp"></i></a>
                        </p>

                        @include('front.layout.partials.produtos-videos')

                        @include('front.layout.partials.archives')

                        @include('front.layout.partials.conteudos-relacionados')

                        @include('front.layout.partials.produtos-relacionados')

                        @include('front.layout.partials.banner-center')

                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->

@endsection