@extends('front.layout.master')

@section('header_title', trans('title.institucional'))

@section('main-content')
    <main id="main" class="page-marca">

        <section id="page-header" class="green">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i> <a href="">{{trans('title.sobre_adere')}}</a>
                </div>
                <h1 class="page-title">
                    {{(!empty($item_mark->title)) ? $item_mark->title : null}}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">

                <div class="row">
                    @include('front.layout.partials.sidebar-internal')
                    <div class="col-sm-9 content">
                        @if(!empty($item_mark->galleries) && count($item_mark->galleries) > 0)
                            <div class="top-carousel owl-carousel">
                                @foreach($item_mark->galleries as $item_gallery)
                                    <div class="item">
                                        <img src="{{ asset('_files/marcas_galeria/'.$item_gallery->image) }}" alt="{{(!empty($item_mark->title)) ? $item_mark->title : null}}">
                                    </div>
                                @endforeach
                            </div>
                        @endif

                        <div class="box">
                            <h2 class="content-title h2">{{(!empty($item_mark->title)) ? $item_mark->title : null}} <img src="{{asset('_files/marcas/'.$item_mark->image)}}" alt="Logo" class="logo-marca pull-right"></h2>

                            {!! (!empty($item_mark->text)) ? $item_mark->text : null  !!}
                        </div>

                        <div class="box">
                            <h2 class="content-title h2">{{ trans('title.produtos_relacionados') }}</h2>

                            <ul class="default-list">
                                @foreach($item_mark->products as $item_product)
                                    <li>
                                        <figure>
                                            <a href="{{url($sharedData->get('lang_locale').'/solucoes/'.$item_product->family->type->slug.'/'. $item_product->family->slug . '/' .$item_product->slug )}}">
                                                @if(!empty($item_product->galleries) && count($item_product->galleries) > 0)
                                                    <img src="{{asset('_files/produtos_galeria/'.$item_product->galleries->first()['image'])}}" alt="">
                                                @else
                                                    <img src="{{asset('front/img/sem-foto.png')}}" alt="">
                                                @endif
                                            </a>
                                        </figure>
                                        <div class="title-container">
                                            <div class="title">{{ !empty($item_product->title) ? $item_product->title : null }}</div>
                                        </div>
                                        @if(!empty($item_product->url_loja_virtual))
                                            <a href="{{$item_product->url_loja_virtual}}" class="btn btn-red">Comprar <i class="fa fa-shopping-cart"></i></a>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->
@endsection