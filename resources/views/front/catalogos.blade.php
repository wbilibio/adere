@extends('front.layout.master')

@section('header_title', trans('title.percerias'))

@section('main-content')
    <main id="main" class="page-nossas-solucoes">

        <section id="page-header" class="red">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i>
                </div>
                <h1 class="page-title">
                    {{ trans('title.nossos_materiais') }}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">

                <div class="row">
                    @include('front.layout.partials.sidebar-single')
                    <div class="col-sm-9 content">
                        <div class="box">
                            <h2 class="content-title h2">{{ trans('title.nossos_materiais') }}</h2>
                            <ul class="default-list default-list-center">
                                @foreach($items_catalog as $item_catalog)
                                    <li>
                                        <figure>
                                            <a href="{{ asset('_files/catalogos/'.$item_catalog->file) }}" download>
                                                <img src="{{ asset('_files/catalogos/'.$item_catalog->image) }}" height="215" alt="">
                                            </a>
                                        </figure>
                                        <div class="title-container">
                                            <div class="title">{!! !empty($item_catalog->title) ? $item_catalog->title : null !!}</div>
                                        </div>
                                        {!! !empty($item_catalog->text) ? $item_catalog->text : null !!}
                                        <p class="">
                                            <a href="{{ asset('_files/catalogos/'.$item_catalog->file) }}" class="btn btn-transparent red" download>
                                                Download <i class="fa fa-download"></i>
                                            </a>
                                        </p>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        @include('front.layout.partials.banner-center')

                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->

@endsection