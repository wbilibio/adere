@extends('front.layout.master')

@section('header_title', trans('title.perguntas_frequentes'))

@section('main-content')
    <main id="main" class="page-faq">

        <section id="page-header" class="red">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i>
                </div>
                <h1 class="page-title">
                    {{trans('title.perguntas_frequentes')}}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container" id="vue-faq">

                <div class="row">
                    @include('front/layout/partials/sidebar-single')
                    <div class="col-sm-9 content">
                        <div class="">
                            <div class="row form-content faq-header">
                                <div class="col-sm-6 text-right">
                                    FILTROS
                                </div>
                                <div class="col-sm-3">

                                    <div class="form-group">
                                        <select name="" id="" data-searchmin="10" v-model="select_type">
                                            <option value="0">Soluções...</option>
                                            @foreach($items_product_types as $item_product_type)
                                                <option value="{{$item_product_type->id}}">{{$item_product_type->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <select name="" id="" data-searchmin="10" v-model="select_market">
                                            <option value="0">Mercado...</option>
                                            @foreach($items_markets as $item_market)
                                                <option value="{{$item_market->id}}">{{$item_market->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <h3 class="text-center" v-if="items_faq == false">Nenhum pergunta cadastrada.</h3>
                            <div class="faq-item" v-for="item in items_faq" v-else>
                                <div class="row">
                                    <div class="col-sm-10 faq-question">
                                        <span v-cloak>@{{ item.title }}</span>
                                    </div>
                                    <div class="col-sm-2 faq-operation">
                                        <span class="faq-open">Resposta <i class="fa fa-chevron-down"></i></span>
                                        <span class="faq-close">Fechar <i class="fa fa-remove"></i></span>
                                    </div>
                                </div>
                                <p class="faq-content">
                                    <span v-cloak>@{{ item.text }}</span>
                                </p>
                            </div>
                            <br>
                        </div>
                        @include('front.layout.partials.form_informe_solucao')

                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->

@endsection