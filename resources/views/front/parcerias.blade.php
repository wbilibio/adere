@extends('front.layout.master')

@section('header_title', trans('title.percerias'))

@section('main-content')
    <main id="main" class="page-parcerias">

        <section id="page-header" class="red">

            <div class="container">
                <div class="breadcrumbs">
                    <a href="">Home</a> <i class='fa fa-chevron-right'></i> <a href="">{{ trans('title.parceria') }}</a>
                </div>
                <h1 class="page-title">
                    {{ trans('title.parceria') }}
                </h1>
            </div>
        </section>
        <section id="page-content">
            <div class="container">

                <div class="row">
                    @include('front.layout.partials.sidebar-internal')
                    <div class="col-sm-9 content">

                        <div class="box">
                            <h2 class="content-title h2">{{ (!empty($item_partner->title)) ? $item_partner->title : null  }}</h2>
                            {!! (!empty($item_partner->text)) ? $item_partner->text : null  !!}
                        </div>
                        @if(!empty($items_gallery) && count($items_gallery) > 0)
                            <div class="box">
                                <h2 class="content-title h2">{{ trans('text.falam_adere') }}</h2>
                                <p class="content-title-extra">{!! (!empty($item_partner->text_gallery)) ? $item_partner->text_gallery : null  !!}</p>
                                <ul class="default-list">
                                    @foreach($items_gallery as $item_gallery)
                                        <li>
                                            <figure>
                                                <a href="{{$item_gallery->link_youtube}}?rel=0&amp;controls=0&amp;showinfo=0&autoplay=1"" class="modal-play">
                                                    <img src="{{ asset('_files/parcerias_galeria/'.$item_gallery->image)}}" alt="">
                                                    <span class="overlay-play">
                                                        <i class="fa fa-youtube-play"></i>
                                                    </span>
                                                </a>
                                            </figure>
                                            <div class="title-container">
                                                <div class="title"><strong>{{ $item_gallery->title }}</strong> <br>{{ $item_gallery->profession }}</div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="clearfix"></div>
                                @include('front.layout.partials.modal-video')
                            </div>
                        @endif
                        @if(!empty($items_testimonials) && count($items_testimonials) > 0)
                            <div class="box">
                                <h2 class="content-title h2">{{ trans('title.depoimentos') }}</h2>
                                <p class="content-title-extra">{!! (!empty($item_partner->text_testimonials)) ? $item_partner->text_testimonials : null  !!}</p>
                                <ul class="depoimentos-carousel top-carousel owl-carousel">
                                    @foreach($items_testimonials as $item_testimonials)
                                        <li class="item">
                                            {!! (!empty($item_testimonials->text)) ? $item_testimonials->text : null  !!}
                                            <table>
                                                <tr>
                                                    <td class="image">
                                                        <figure>
                                                            <img src="{{ asset('_files/parcerias_depoimentos/'.$item_testimonials->image)}}" alt="">
                                                        </figure>
                                                    </td>
                                                    <td>
                                                        <strong class="name">
                                                            {{ (!empty($item_testimonials->title)) ? $item_testimonials->title : null  }}
                                                        </strong>
                                                        <p>{{ (!empty($item_testimonials->company)) ? $item_testimonials->company : null  }}</p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>

    </main><!-- /#main.page-institucional -->
@endsection