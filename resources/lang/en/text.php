<?php

return [
    //Funções
    'placeholder_buscar' => 'YOUR RESEARCH HERE',
    'cadastro_newsletter' => 'REGISTER AND RECEIVE OUR NEWS IN YOUR E-MAIL!',
    'horario_atendimento' => 'FROM 09H TO 16H, VALID ONLY FOR FIXED TELEPHONE CONNECTIONS.',
    'direitos_reservados' => 'All rights reserved for ADERE Produtos Auto Adesivo Ltda.',
    'falam_adere' => 'See what they say about the Adere',
    'informe_solucao' => 'Tell me the solution you need',
    'saiba_mais_automotivo' => 'Learn more about the <br> Market ',
    'preencha_formulario' => 'FILL IN THE FORM BELOW TO DOWNLOAD',
];