<?php

return [
    //Funções
    'placeholder_buscar' => 'ENTER AQUÍ SU BÚSQUEDA',
    'cadastro_newsletter' => 'Registrarse y recibir nuestras noticias en tu e-mail!',
    'horario_atendimento' => 'DE LA 09H AS 16H, válida únicamente para las conexiones telefónicas fijas.',
    'direitos_reservados' => 'Todos los derechos reservados a ADERE Produtos Auto Adesivos Ltda.',
    'falam_adere' => 'Vea lo que dicen de la Adere',
    'informe_solucao' => 'Introduzca la solución que necesita',
    'saiba_mais_automotivo' => 'Más información sobre el <br>mercado del ',
    'preencha_formulario' => 'Complete el siguiente formulario para descargar',
];