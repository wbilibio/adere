<?php

return [
    // Titulos
    'home' => 'Página inicial',
    '50_anos_adere' => '50 anos Adere',
    'adere_em_numero' => 'Adere em Números',
    'adere_no_mundo' => 'Adere no mundo',
    'ultimos_posts' => 'ÚLTIMOS POSTS',
    'servicos' => 'Serviços',
    'siga_adere' => 'SIGA A ADERE',
    'fale_com_adere' => 'FALE COM A ADERE',
    'conceitos_estrategicos' => 'Conceitos Estratégicos',
    'videos' => 'Vídeos',
    'depoimentos' => 'Depoimentos',
    'fechar' => 'Fechar',
    'conteudos_relacionados' => 'Conteúdos Relacionados',
    'arquivos_relacionados' => 'Arquivos Relacionados',
    'compartilhe' => 'Compartilhe',
    'cancelar' => 'Cancelar',
    'download' => 'Download',
    'conheca_produtos' => 'Conheça nossos produtos',
    'ouvidoria' => 'Ouvidoria',


    // Novos
    'nossos_materiais' => 'Nossos Materiais',
    'aplicacao' => 'Aplicação',
    'como_aplicar' => 'Saiba como aplicar o produto',
    'produtos_relacionados' => 'Produtos Relacionados',
    'compre_aqui' => 'Compre Aqui',
    'veja_galeria' => 'Veja galeria',
    'setor' => 'Setor',
    'selecione_area' => 'Seleciona a área',
    'entre_em_contato' => 'Entre em Contato',
    'comentarios' => 'Comentários',




    // Links do Menu
    'sobre_adere' => 'Sobre Adere',
    'institucional' => 'Institucional',
    'certificacoes' => 'Certificações',
    'responsabilidade_social' => 'Responsabilidade Social',
    'parceria' => 'Parcerias',
    'produtos_sob_medida' => 'Produtos sob medida',
    'nossas_marcas' => 'Nossas Marcas',
    'nossos_50_anos' => 'Nossos 50 anos',
    'solucoes' => 'Soluções',
    'mercados' => 'Mercados',
    'loja' => 'Loja',
    'blog' => 'Blog',
    'contato' => 'Contato',
    '2_via_boleto' => '2º via de boletos',
    'banco_imagens' => 'Banco de Imagens',
    'perguntas_frequentes' => 'Perguntas Frequentes',
    'politica_privacidade' => 'Política de Privacidade',
    'catalogo' => 'Catálogos',
    
    //Funções
    'buscar' => 'Buscar',
    'tudo' => 'Tudo',
    'mercado_de_atuacao' => 'Mercado de Atuação',
    'produtos_mais_buscados' => 'Produtos Mais Buscados',

    // Labels
    'seu_nome' => 'Seu Nome',
    'seu_email' => 'Seu E-mail',
    'campo_obrigatorio' => 'Campo obrigatório',
    'cadastrar' => 'Cadastrar',
    'nome' => 'Nome',
    'cargo' => 'Cargo',
    'empresa' => 'Empresa',
    'obrigatorio' => 'Obrigatório',
    'telefone' => 'Telefone',
    'assunto' => 'Assunto',
    'mensagem' => 'Mensagem',
    'enviar' => 'Enviar',
];