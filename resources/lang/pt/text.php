<?php

return [
    //Funções
    'placeholder_buscar' => 'DIGITE AQUI SUA PESQUISA',
    'cadastro_newsletter' => 'CADASTRE-SE E RECEBA NOSSAS NOVIDADES NO SEU E-MAIL!',
    'horario_atendimento' => 'DAS 09H AS 16H, VÁLIDO APENAS PARA LIGAÇÕES DE TELEFONES FIXOS.',
    'direitos_reservados' => 'Todos os direitos reservados para ADERE Produtos Auto Adesivos Ltda.',
    'falam_adere' => 'Veja o que falam da Adere',
    'informe_solucao' => 'Informe a solução que precisa',
    'saiba_mais_automotivo' => 'Saiba mais sobre o <br> Mercado ',
    'preencha_formulario' => 'PREENCHA O FORMULÁRIO ABAIXO PARA EFETUAR O DOWNLOAD',
    
    
    //noveos
    'text_contato' => 'Peencha o formulário abaixo. Responderemos em breve'

];