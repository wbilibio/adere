require('./bootstrap');
require('./plugins.min');

if($('#vue-faq').length > 0){
	const VueFaq = new Vue({
		el: '#vue-faq',
		data () {
			return {
				items_faq: (window.items_faq_data == undefined) ? 'false' : items_faq_data,
				select_market: 0,
				select_type: 0,
			}
		},
		watch: {
			select_market: function(){
				var url = window.location.href+'/get_list_market/'+this.select_market;
				$.get(url, function(data) {
					VueFaq.items_faq = data;
				});

			},
			select_type: function(){
				var url = window.location.href+'/get_list_type/'+this.select_type;
				$.get(url, function(data) {
					VueFaq.items_faq = data;
				});

			}
		}
	});
}


require('./main');