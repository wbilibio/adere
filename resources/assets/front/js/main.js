jQuery(document).ready(function($) {
	// ADD NEWSLETTER
	function doEmail(pStr) {
		if (/^[\w-]+(\.[\w-]+)*@(([A-Za-z\d][A-Za-z\d-]{0,61}[A-Za-z\d]\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/.test(pStr)) {
			return true;
		} else {
			return false;
		}
	}

	$('.btn-open-answer').click(function(e){
		e.preventDefault();
		var comment_id = $(this).data('id');
		$('.modal').show();
		$('.modal').find('.form-content .post_comments_id').val(comment_id);
	});
	/*** FORM ANSWERS ***/
	jQuery('#formAnswer').submit(function(){
		var form = $(this);
		var email = form.find('.input-email');
		var name = form.find('.input-name');
		var text = form.find('.input-text');
		var slug = form.data('slug');
		var send = false;

		function error(input,msg){
			input.parent().find('.required').show();
			input.parent().addClass('form-error');
			form.find('.msg').addClass('send-error').show();
			if(msg) form.find('.msg').text(msg);
			else form.find('.msg').text('Preencha os campos obrigatórios!');
			send = false;
		}
		function success(input){
			input.parent().find('.required').hide();
			input.parent().removeClass('form-error');
			form.find('.msg').hide();
			send = true;
		}
		if(name.val().length <= 0) error(name,false);
		else success(name);
		if (!doEmail(email.val())) error(email,false);
		else success(email);
		if(text.val().length <= 0) error(text,false);
		else success(text);
		if(send) {
			$.ajax({
				url: window.location.origin + '/pt/post/'+slug+'/send_answer',
				type: "POST",
				headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
				data: form.serialize(),
				success: function(data){
					if(data == '1') {
						form.find('.msg').removeClass('send-error').addClass('send-success').show();
						form.find('.msg').text('Resposta enviada com sucesso! Aguardando aprovação.');
						setTimeout(function(){
							$('.modal.modal-download').hide();
							form[0].reset();
						},3000);
					} else {
						form.find('.msg').addClass('send-error').show();
						form.find('.msg').text('Cadastro não realizado, favor entrar em contato conosco.');

					}
				}
			});

		}

		return false;
	});



	/*** FORM NEWSLETTER ***/
	jQuery('#formNewsletter').submit(function(){
		var form = $(this);
		var valido = false;
		var email = form.find('.input-email');

		if (!doEmail(email.val())) {
			email.parent().find('.required').show();
			email.addClass('input-required');
			form.find('.msg').addClass('error').show();
			form.find('.msg.error > p').text('Preencha os campos obrigatórios!');
		} else {
			$.ajax({
				url: window.location.origin + '/pt/newsletter',
				type: "POST",
				headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
				data: form.serialize(),
				success: function(data){
					if(data == '1') {
						form.find('.msg').addClass('success').show();
						form.find('.msg > p').text('Preencha os campos obrigatórios!');
					} else if(data == '2') {
						form.find('.msg').addClass('error').show();
						form.find('.msg.error > p').text('Seu e-mail foi cadastrado com sucesso!');
					} else if(data == '3') {
						form.find('.msg').addClass('error').show();
						form.find('.msg.error > p').text('E-mail já cadastrado!');
					}
				}
			});
		}
		return valido;
	});

	/*** FORM DOWNLOAD ***/
	jQuery('#formDownload').submit(function(){
		var form = $(this);
		var email = form.find('.input-email');
		var name = form.find('.input-name');
		var download = false;

		function error(input,msg){
			input.parent().find('.required').show();
			input.parent().addClass('form-error');
			form.find('.msg').addClass('error').show();
			if(msg) form.find('.msg.error > p').text(msg);
			else form.find('.msg.error > p').text('Preencha os campos obrigatórios!');
			download = false;
		}
		function success(input){
			input.parent().find('.required').hide();
			input.parent().removeClass('form-error');
			form.find('.msg').hide();
			download = true;
		}
		if(name.val().length <= 0) error(name,false);
		else success(name);
		if (!doEmail(email.val())) error(email,false);
		else success(email);
		if(download) {
			$.ajax({
				url: window.location.origin + '/pt/save_user_download_file',
				type: "POST",
				headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
				data: form.serialize() + '&file_name=' + form.attr('action'),
				success: function(data){
					if(data == '1') {
						window.open(form.attr('action'),'_blank');
						$('.modal.modal-download').hide();
						form[0].reset();
					} else {
						form.find('.msg').addClass('error').show();
						form.find('.msg > p').text('Cadastro não realizado, favor entrar em contato conosco.');

					}
				}
			});

		}

		return false;
	});

	/*** OWL Carousel 2 ***/
	var carousel = $(".home-carousel");

	carousel.on('initialized.owl.carousel', function( event ){
		setTimeout(function() {
			var div = $('<div class="owl-dots-content" />');
			div.append(carousel.find('.owl-dots'));
			carousel.append(div);
		}, 500)
	}); 
	carousel.owlCarousel({ 
		items: 1,
		loop: true,
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		dots: true
	});

	$('.atuacao-tabs-content').owlCarousel({ 
		items: 1,
		loop: true,
		nav: true,
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		navText: [
        "<i class='icon-chevron-left'></i>",
        "<i class='icon-chevron-right'></i>"],
	});

	$('.top-carousel').owlCarousel({
		items: 1,
		loop: true,
		dots: true,
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
	});
	if ($('.internal-carousel .item').length > 1) {
		$('.internal-carousel').owlCarousel({
			items: 1,
			loop: true,
			dots: true,
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
		});
	}
	
	if ($(window).width() < 600) {
		$('.page-marca').find('.box .default-list').addClass('owl-carousel').owlCarousel({ 
			items: 1,
			loop: true,
			dots: true,
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			responsive : {
				0: 		{ items:1 },
				600: 	{ items:2 },
			},
		});
	}
	if ($(window).width() < 500) {
		$('.page-home .home-brand-list').addClass('owl-carousel').owlCarousel({ 
			items: 1,
			loop: true,
			dots: true,
			autoplay: 1,
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			responsive : {
				0: 		{ items:1 },
				480: 	{ items:2 },
			},
		});
	}


    /*** Busca para mercados ***/
	var markets_items = [];
	if(window.markets_data != undefined){
		window.markets_data.filter(function(item){
			var market_item = { label: item.pt_title, slug: item.slug };
			markets_items.push(market_item);
		});
	}
	if ($("#home-search-input")[0] && typeof $("#home-search-input").autocomplete != 'undefined') {
	    var slug_form;
	    var input_search_market = $("#home-search-input");
	    input_search_market.autocomplete({
			source: markets_items,
			minLength: 1,
	        autoFocus:true,
			select: function(event, ui) {
				window.location.href = 'pt/mercados/'+ui.item.slug;
			},
	        search: function() {
	            // custom minLength
	            slug_form = this.value;
	        }
		});
	    input_search_market.keydown(function(event){
	        if(event.keyCode == 13) {
	            if(input_search_market.autocomplete("instance").selectedItem != null){
	                window.location.href = 'pt/mercados/'+input_search_market.autocomplete("instance").selectedItem.slug;
	            } else {
	                $('#form-home-search').find('.msg-error').show();
	            }

	        }
	    });
    }
	
    $('.home-search-select').on('change', function() {
        window.location.href = 'pt/mercados/'+this.value;
    });

	/*** Busca para Empresa ***/

	var result_company = [
		{
			label: "Institucional",
			slug: "institucional"
		},
		{
			label: "Certificações",
			slug: "certificacoes"
		},
		{
			label: "Responsabilidade Social",
			slug: "responsabilidade_social"
		},
		{
			label: "Parcerias",
			slug: "parceria"
		},
		{
			label: "Produtos Sob Medida",
			slug: "produtos_sob_medida"
		}
	];
	if(window.marks_data != undefined){
		window.marks_data.filter(function(item){

			var mark_item = { label: item.title, slug: 'nossas_marcas/'+item.slug };
			result_company.push(mark_item);
		});
	}
	var search_company = $("#input-search-company");
	search_company.autocomplete({
		source: result_company,
		minLength: 1,
		autoFocus:true,
		select: function(event, ui) {
			window.location.href = window.location.origin = '/pt/' + ui.item.slug;
		},
		search: function() {
			// custom minLength
			slug_form = this.value;
		}
	});
	search_company.keydown(function(event){
		if(event.keyCode == 13) {
			if(search_company.autocomplete("instance").selectedItem != null){
				window.location.href = window.location.origin = '/pt/' + search_company.autocomplete("instance").selectedItem.slug;
			} else {
				$('.msg-no-result').show();
			}

		}
	});

	// DROP down menu lateral
    $('.sidebar-nav > li > .link').find('.fa-chevron-right.icon-set').click(function(){
        if($(this).parent().parent().hasClass('active')) return false;
        $('.sidebar-nav > li').removeClass('active');
        $(this).parent().parent().find('.sub').slideDown(700);
        $(this).parent().parent().addClass('active');
    });

	$('.sidebar-nav > li > .link').find('.fa-chevron-down.icon-set').click(function(){
		$('.sidebar-nav > li').removeClass('active');
		$(this).parent().parent().find('.sub').slideUp(700);
	});

    setTimeout(function(){
        anim = true
        return anim;
    },900);

	/*** Modal ***/
	$('.open-modal-download').click(function(event) {

		// $('.modal-download iframe').attr('src',event.currentTarget.href);
		$('.modal-download').show();

		$('.form-accept-file').attr('action',$(this).attr('href'));
		
		if ($(window).width() > 767)
		$('.modal-download .modal-data').css('top', ($(window).height()/2)-($('.modal-download .modal-data').height()/2))

		return false;
	});

	$('.modal-play').click(function(event) {
		$('.modal-video iframe').attr('src','http://www.youtube.com/embed/'+$(this).data('href')+'?rel=0&amp;controls=0&amp;showinfo=0&autoplay=1');
		$('.modal-video').show();

		$('.modal-video .modal-data').css('top', ($(window).height()/2)-($('.modal-video .modal-data').height()/2));
	});
	$('.modal .close, .modal .overlay, .modal .btn-close').click(function(event) {
		$('.modal').hide();
		$('.modal iframe').attr('src','');
		return false;
	});

	/*** Tabs ***/

	$('.atuacao-tabs-content').on('changed.owl.carousel', function( event ){
		$('.atuacao-tab').removeClass('active');
		$('.atuacao-tab[data-id='+$('.atuacao-tabs-content').find(".owl-item").eq(event.property.value).find('.row').attr('data-id')+']').addClass('active')
	}); 
	$('.atuacao-tab').click(function(event) {
		$('.atuacao-tab').removeClass('active');
		$(this).addClass('active');

		$(".atuacao-tabs-content").trigger("to.owl.carousel", [this.getAttribute('data-id')-1, 1, true])

	});

	/*** FAQ ***/
	$('.faq-open').click(function(event) {
		
		$('.faq-content').slideUp(300);
		$('.faq-item').removeClass('open');
		$(this).parents('.faq-item').find('.faq-content').stop().slideDown(400);
		$(this).parents('.faq-item').addClass('open');
	});
	$('.faq-close').click(function(event) {
		
		$('.faq-content').slideUp(300);
		$('.faq-item').removeClass('open');
	});

	/*** FORM ***/
	$('.select2').each(function(index, el) {
		var a = $(el).select2({ minimumResultsForSearch: $(el).attr('data-searchmin') });	

		$(el).on('select2:open', function(ev) {
			$('.select2-container').addClass($(ev.currentTarget).attr('data-class'))
		})
	});

	/*** MENU ***/
	$('.nav-market-items .sub-has-sub').hover(function() {
		$('.nav-market-items .sub-has-sub').removeClass('active')
		$(this).addClass('active')

	});
	$('.nav-institucional .has-sub').click(function() {
		$(this).toggleClass('active');
	});

	/*** MOBILE ***/
	
	$('.mobile-nav').click(function(event) {
		$('.nav').toggle()
	});
	$('.show-mobile').click(function(event) {
		
		if ($(event.currentTarget).closest('li').hasClass('has-sub col-sm-3')) {
			$(event.currentTarget).closest('ul').find('ul').stop().slideUp();
			$(event.currentTarget).closest('ul').find('.show-mobile').removeClass('fa-chevron-down').addClass('fa-chevron-right')
			if ($(event.currentTarget).hasClass('active'))
				$(event.currentTarget).removeClass('fa-chevron-down active').addClass('fa-chevron-right')
			else
				$(event.currentTarget).toggleClass('fa-chevron-right fa-chevron-down active')
			$(event.currentTarget).closest('a').next().stop().slideToggle();
		} else {
			$(event.currentTarget).closest('ul').find('ul').stop().slideUp(300,function() { $(this).css('height', 'auto') });
			$(event.currentTarget).closest('ul').find('.show-mobile').removeClass('fa-chevron-down').addClass('fa-chevron-right')

			if ($(event.currentTarget).hasClass('active'))
				$(event.currentTarget).removeClass('fa-chevron-down active').addClass('fa-chevron-right')
			else
				$(event.currentTarget).toggleClass('fa-chevron-right fa-chevron-down active')
			$(event.currentTarget).closest('li').find(' > ul,  > div > ul').stop().slideToggle();
		}

		return false;
	});
	setTimeout(function(){
		if ($(window).width() > 991 && $('#page-content .page-sidebar')[0] && $('#page-content .page-sidebar').height() > $('#page-content .content').height())
			$('#page-content .content').css('min-height',$('#page-content .page-sidebar').height() + 100);
	},1200);



	if ($(window).width() > 991 && $('#page-content .page-sidebar')[0] && $('#page-content .page-sidebar').height() > $('#page-content .content').height())
		$('#page-content .content').css('min-height',$('#page-content .page-sidebar').innerHeight() + 100);



	
	if ($(window).width() >= 991) {
		// Stick menu
		var didScroll;
		var lastScrollTop = 0;
		var delta = 3;
		var navbarHeight = 50; //$('#header').outerHeight();

		$(window).scroll(function(event){
		    didScroll = true;
		});

		setInterval(function() {
		    if (didScroll) {
		        hasScrolled();
		        didScroll = false;
		    }
		}, 250);

		function hasScrolled() {
		    var st = $(this).scrollTop();

		    if (st <= 30) $('#header').removeClass('has-top');
		    else $('#header').addClass('has-top');
		    
		    // Make sure they scroll more than delta
		    if(Math.abs(lastScrollTop - st) <= delta)
		        return;
		    
		    // If they scrolled down and are past the navbar, add class .nav-up.
		    // This is necessary so you never see what is "behind" the navbar.
		    if (st > lastScrollTop && st > navbarHeight){
		        // Scroll Down
		        $('#header').removeClass('nav-down').addClass('nav-up');
		    } else {
		        // Scroll Up
		        if(st + $(window).height() < $(document).height()) {
		            $('#header').removeClass('nav-up').addClass('nav-down');
		        }
		    }
		    
		    lastScrollTop = st;
		}
	}
});	