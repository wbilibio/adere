import {ServerTable, ClientTable, Event} from 'vue-tables-2';

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

Vue.use(ClientTable, {
    perPage: 10,
    texts:{
        count:'Mostrando {from} de {to} para {count} itens|{count} itens|Um item',
        filter:'Pesquisar por: ',
        filterPlaceholder:'...',
        limit:'Itens:',
        noResults:'Sem itens cadastrados',
        page:'Página:', // for dropdown pagination
        filterBy: 'Filtrado por {column}', // Placeholder for search fields when filtering by column
        loading:'Carregando...', // First request to server
        defaultOption:'Selecione {column}' // default option for list filters
    }
});

//SubCategorias
Vue.component('action_template_subcategory',{
    props:['data'],
    template:'<div class="btn-group"><a v-bind:href="urlEdit()" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a><a v-bind:href="urlGallery()" class="btn btn-sm btn-success"><i class="fa fa-picture-o"></i></a><a v-bind:href="urlDelete()" data-toggle="tooltip" title="deletar" class="btn btn-danger btn-sm delete-btn"><i class="fa fa-trash"></i></a></div>',
    methods:{
        urlGallery: function(){
            var url_edit = '/cms/produtos/subcategorias/galeria/'+this.data.id;
            return url_edit;
        },
        urlEdit: function(){
            var url_edit = '/cms/produtos/subcategorias/editar/'+this.data.id;
            return url_edit;
        },
        urlDelete: function() {
            var url_delete = '/cms/produtos/subcategorias/delete/'+this.data.id;
            return url_delete;
        }
    }
});
//Produtos
Vue.component('action_template_products',{
    props:['data'],
    template:'<div class="btn-group"><a v-bind:href="urlEdit()" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a><a v-bind:href="urlVideos()" class="btn btn-sm btn-warning"><i class="fa fa-video-camera"></i></a><a v-bind:href="urlGallery()" class="btn btn-sm btn-success"><i class="fa fa-picture-o"></i></a><a v-bind:href="urlDelete()" data-toggle="tooltip" title="deletar" class="btn btn-danger btn-sm delete-btn"><i class="fa fa-trash"></i></a></div>',
    methods:{
        urlVideos: function(){
            var url_edit = '/cms/produtos/videos/'+this.data.id;
            return url_edit;
        },
        urlGallery: function(){
            var url_edit = '/cms/produtos/galeria/'+this.data.id;
            return url_edit;
        },
        urlEdit: function(){
            var url_edit = '/cms/produtos/lista/editar/'+this.data.id;
            return url_edit;
        },
        urlDelete: function() {
            var url_delete = '/cms/produtos/lista/delete/'+this.data.id;
            return url_delete;
        }
    }
});

//Blog
Vue.component('action_template_blog',{
    props:['data'],
    template:'<div class="btn-group"><a v-bind:href="urlEdit()" data-toggle="tooltip" title="galeria" class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></a><a v-bind:href="urlComments()" data-toggle="tooltip" title="deletar" class="btn btn-sm btn-primary"><i class="fa fa-commenting"></i></a><a v-bind:href="urlDelete()" data-toggle="tooltip" title="deletar" class="btn btn-danger btn-sm delete-btn"><i class="fa fa-trash"></i></a></div>',
    methods:{
        urlEdit: function(){
            var url_edit = '/cms/blog/posts/editar/'+this.data.id;
            return url_edit;
        },
        urlComments: function(){
            var url_comments = '/cms/blog/posts/'+this.data.id+'/comentarios';
            return url_comments;
        },
        urlDelete: function() {
            var url_delete = '/cms/blog/posts/delete/'+this.data.id;
            return url_delete;
        }
    }
});

Vue.component('image_template_blog',{
    props:['data'],
    template:'<img class="image-table" :src="image()" v-if="this.data.image != null" /><div class="sem-foto" v-else />',
    methods:{
        image: function() {
            return '../../_files/posts/'+this.data.image;
        }
    }
});
const Admin = new Vue({
    el: '#app',
    data () {
        return {
            items_gallery: (window.items_gallery_data == undefined) ? 'false' : items_gallery_data,
            items_list: (window.items_list_data == undefined) ? 'false' : items_list_data,
            items_list_two: (window.items_list_data_two == undefined) ? 'false' : items_list_data_two,
            visible_select: (window.status_data == undefined) ? true : (status_data == 1) ? true : false,
            is_guia: (window.guia_data == undefined) ? false : (guia_data == 1) ? true : false,
            status:(window.status_data == undefined) ? 1 : status_data,
            guia:0,
            msgSuccessGallery: false,
            msgErrorGallery: false,
            msgSuccessReorder: false,
            msgSuccess: null,
            msgError: null,
            urlHash: false,
            names: [],
            productsData: (window.products_data == undefined) ? 'false' : products_data,
            productsColumns: ['pt_title','subcategory','action'],
            productsOptions: {
                headings: {
                    pt_title: 'Título',
                    subcategory:'Subcategoria',
                    action: 'Ações'
                },
                templates: {
                    action: 'action_template_products'
                },
                sortable: ['subcategory','pt_title'],
                orderBy:{
                    column:'subcategory',
                    ascending:true
                }
            },
            subcategoryData: (window.subcategory_data == undefined) ? 'false' : subcategory_data,
            subcategoryColumns: ['pt_title','category','action'],
            subcategoryOptions: {
                headings: {
                    pt_title: 'Título',
                    category:'Categoria',
                    action: 'Ações'
                },
                templates: {
                    action: 'action_template_subcategory'
                },
                sortable: ['category','pt_title'],
                orderBy:{
                    column:'category',
                    ascending:true
                }
            },
            blogData: (window.posts_data == undefined) ? 'false' : posts_data,
            blogColumns: ['image', 'title','action'],
            blogOptions: {
                headings: {
                    image: 'Imagem',
                    title: 'Título',
                    action: 'Ações'
                },
                templates: {
                    image: 'image_template_blog',
                    action: 'action_template_blog'
                },
                sortable: ['title'],
                orderBy:{
                    column:'created_at',
                    ascending:true
                }
            },
            input_tags: null
        }
    },
    methods: {
        imageGalleryDelete: function(){
            this.msgSuccessGallery = true;
            this.msgSuccess = 'Item excluido com sucesso!';
        },
        updateGallery: function(){
            var url = this.correctUrl()+'/get_items_gallery';
            $.get(url, function(data) {
                Admin.items_gallery = data;
                Admin.msgSuccessGallery = true;
                Admin.msgSuccess = 'Imagens carregadas com sucesso!';
            });
        },
        correctUrl: function(){
            var urlOrigin = window.location.pathname;
            var invertUrl = urlOrigin.split("").reverse();
            if (invertUrl[0] == '/') {
                var urlRemove = invertUrl.join("").substr(1);
                var urlFinal = urlRemove.split("").reverse().join("");
                return urlFinal;
            } else return urlOrigin;

        },
        isHash: function isHash(tab_id){
            if(this.urlHash){
                if (this.urlHash == tab_id) return 'active';
            } else {
                if(tab_id == "#tab_1") return 'active';
            }
        },
        callbackMethod: function(value){
            this.input_tags = value;
        }
    },
    watch: {
        visible_select: function(){
            if(this.visible_select) this.status = 1;
            else this.status = 0;
        },
        is_guia: function(){
            if(this.is_guia) this.guia = 1;
            else this.guia = 0;
        }
    },
    computed: {
        getTags: function () {
            var arr_tags = [];
             $.each( window.tags_data, function( key, value ) {
                 arr_tags.push(value.title);
            });
            return arr_tags;

        }
    },
    components: {
        'vue-table-gallery': require('./components/TableGallery.vue'),
        'vue-table-list': require('./components/TableList.vue'),
        'tags-input-field': require('./components/TagsInputField.vue')
    },
    created: function(){
        this.urlHash = window.location.hash;
    }
});
var App = App || {};
// Dropzone
var Dropzone = require('dropzone');

if($('.multiple-upload').length > 0) {
    Dropzone.options.myDropzone = {
        paramName: "file",
        dictDefaultMessage: 'Clique aqui para subir seus arquivos',
        parallelUploads:1,
        uploadMultiple:false,
        success: function(file, response){
            if(response == 3){
                Admin.msgErrorGallery = true;
                Admin.msgError = 'Imagem excedeu o tamanho máximo permitido! (Limite 3000px de largura)';
                setTimeout(function(){
                    window.location.reload();
                },3000);
            } else {
                Admin.updateGallery();
            }
        },
        complete: function(){
            if(Admin.items_gallery.length == 0){
                setTimeout(function(){
                    App.sortableGrid();
                },2000);
            }
        }
    };
}

App.editorTinymce = function() {
    tinymce.init({
        selector: '.tinymce',
        height: 200,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor imageupload',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | imageupload | fullscreen',
        skin: false,
        relative_urls: false
    });
    // Abrir modal do editor
    $('.content-editor').find('.open-editor').click(function(){
        var editor = $(this).parent();
        editor.find('.modal-editor').addClass('editor-show');
    });

    // Fechar modal do editor
    $('.content-editor').find('.close-editor').click(function(){
        var editor = $(this).parent().parent();
        editor.removeClass('editor-show');
        editor.parent().find('.btn').removeClass('btn-primary').addClass('btn-success').text('Visualizar texto');
    });
    $('.modal-editor').find('.overlay-editor').click(function(){
        var editor = $(this).parent();
        editor.removeClass('editor-show');
        editor.parent().find('.btn').removeClass('btn-primary').addClass('btn-success').text('Visualizar texto');
    });

    tinymce.PluginManager.add('imageupload', function(editor, url) {
        editor.addButton('imageupload', {
            tooltip: 'Upload an image',
            icon : 'image',
            text: 'Upload',
            onclick: function() {
                editor.windowManager.open({
                    title: 'Upload an image',
                    //change this route to the one that returns the '_image-dialog.blade.php' file as a view
                    file : window.location.origin+'/cms/image_editor/upload',
                    width : 360,
                    height: 310,
                    buttons: [
                        {
                            text: 'Close',
                            onclick: 'close'
                        }]
                });

            }
        });
    });
};
App.deleteBtn = function() {
    $('.delete-btn').on('click', function(e){
        e.preventDefault();
        var redirect = $(this).attr('href');
        return swal({
            title: 'Deseja realmente remover este item?',
            text: 'Essa operação não pode ser revertida.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Sim, excluir!',
            closeOnConfirm: false
        },
        function(){
            window.location.href = redirect;
        });
    });
};
App.sortableGrid = function() {
    $('.sortable').each(function(idx,el){
        var sortable = $(el);
        var obj = {};
        var url = sortable.attr('data-url');
        sortable.sortable().on('sortupdate', function(event, ui) {
            var items = [];
            sortable.find('.ui-state-default').each(function(i){
                var order = sortable.find('.ui-state-default').length - i;
                obj = {'id': $(this).data('id'), 'order': order};
                items.push(obj);
            });
            App.reorder(items,url);
        });

        sortable.disableSelection();
    });
};

App.reorder = function(items,url) {
    if(items.length > 0){
        $.ajax({
            url: url,
            headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
            type: "POST",
            data: { items: items },
            success: function(data){
                if(data == 1) Admin.msgSuccessGallery = true;
                else if(data == 2) Admin.msgSuccessReorder = true;
                Admin.msgSuccess = 'Item reordenado com sucesso!';
            }
        });
    }
};

App.deleteBtn();
if ($('.tinymce').length > 0) App.editorTinymce();
if ($('.sortable').length > 0) App.sortableGrid();
if ($('.sortable-category').length > 0) App.sortableCategoryGrid();
if ($('.sortable-level').length > 0) App.sortableLevelGrid();

// Gambiarra Input File
jQuery('input[type="file"]').change(function () {
    var value = $(this).val();
    var text = value.substring(value.lastIndexOf("\\") + 1, value.length);
    jQuery(this).parent().find('.btn').removeClass('btn-primary').addClass('btn-success').text(text);
});