## Adere

**Homologação**:
[![build status](https://gitlab.com/nextidea/adere-site/badges/homolog/build.svg)](https://gitlab.com/nextidea/adere-site/commits/homolog)
[![coverage report](https://gitlab.com/nextidea/adere-site/badges/homolog/coverage.svg)](https://gitlab.com/nextidea/adere-site/commits/homolog)

**Produção**:
[![build status](https://gitlab.com/nextidea/adere-site/badges/production/build.svg)](https://gitlab.com/nextidea/adere-site/commits/production)
[![coverage report](https://gitlab.com/nextidea/adere-site/badges/production/coverage.svg)](https://gitlab.com/nextidea/adere-site/commits/production)


Para trabalhar no projeto, clone-o e execute:

 ```
 composer install
 npm install            # ou yarn
 cp .env.example .env
 php artisan key:generaate
 ```


## Ferramentas

A lista a seguir apresenta as ferramentas utilizadas/recomendadas para o desenvolvimento do projeto:

- Laravel Framework _(backend)_
- Docker _(ambiente de desenvolvimento)_
- Valet _(opcional, utilizado para acessar o site)_

Ao utilizar o Docker, copie o arquivo `docker-compose.example.yml` para `docker-compose.yml` e altere conforme necessário.

## Contribuição

O _workflow_ utilizado para a gestão do código fonte é baseado no [**GitLab Flow**](https://about.gitlab.com/2014/09/29/gitlab-flow/).

**NUNCA** deve ser realizado um _merge_ com as _topic branches_ (features, hotfixes e releases),
mas sim criado um **merge request** para a branch **develop** (também conhecido como _pull request_ por usuários do Github).

### Branches

O desenvolvedor deve começar as suas topic branches sempre a partir da branch **develop**.
Quando uma topic branch é finalizada, o desenvolvedor abre um merge request para a **develop**.

---

Topic branches são as branches que o desenvolvedor deve trabalhar. O nome da branch deve ser
baseado no _issue_ criado no _Gitlab_:

- **{issue_id}-{issue-name}**

Para isso, basta acessar a issue do _Gitlab_ e clicar em **New Branch**.

---

As branches a seguir não podem ser alteradas pelo desenvolvedor, sendo utilizadas apenas
para merges de homologação e produção, pois o servidor realiza deploy automático.

- **homolog**
- **master**


---

O fluxo básico é:

- **Criar/acessar um issue no Gitlab**
- **Criar uma topic branch a partir da develop**: Clicar em _New branch_ na tela da issue (ou git checkout -b start _{issue-id}-{issue-name}_).
- **Comitar as alterações**: git commit ...
- **Enviar a topic branch**: git push origin _{issue-id}-{issue-name}_
- **Abrir um merge request para develop**: Pela UI do GitLab.

Com o _merge request_ efetuado, o código será testado, verificado e, quando necessário, alterado,
para então ser adicionado ao branch _develop_.


## Assets

Os arquivos estáticos **NÃO** devem ser salvos diretamente na pasta public, mas
sim na pasta _resources/assets_, para que sejam compilados e copiados devidamente
e não gerem conflitos do git.
 
## Coding Patterns

O desenvolvedor PHP deve seguir os _patterns_ do PSR-4 e do Laravel Best Practices.

## Banco de dados

O banco de dados **NUNCA** deve ser alterado através de ferramentas de gerenciamento
de banco de dados, como _MySqlWorkbench_ ou _phpMyadmin_, pois o framework Laravel
possui funcionalidade de _migrations_, com controle de versão das alterações.