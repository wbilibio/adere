const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
/*----------
 CMS
  */
mix.js('resources/assets/admin/js/app.js', 'public/admin/js')
   .sass('resources/assets/admin/sass/app.scss', 'public/admin/css')
   .less('node_modules/bootstrap-less/bootstrap/bootstrap.less', 'public/admin/css/bootstrap.css')
   .less('resources/assets/admin/less/adminlte-app.less','public/admin/css/adminlte-app.css')
   .less('node_modules/toastr/toastr.less','public/admin/css/toastr.css')
   .combine([
       'public/admin/css/app.css',
       'node_modules/admin-lte/dist/css/skins/_all-skins.css',
       'public/admin/css/adminlte-app.css',
       'node_modules/icheck/skins/square/blue.css',
       'public/admin/css/toastr.css'
   ], 'public/admin/css/all.css')
   //APP RESOURCES
   .copy('resources/assets/admin/images','public/admin/images')
   //VENDOR RESOURCES
   .copy('node_modules/font-awesome/fonts/*.*','public/admin/fonts/')
   .copy('node_modules/ionicons/dist/fonts/*.*','public/admin/fonts/')
   .copy('node_modules/admin-lte/bootstrap/fonts/*.*','public/admin/fonts/bootstrap')
   .copy('node_modules/admin-lte/dist/css/skins/*.*','public/admin/css/skins')
   .copy('node_modules/admin-lte/dist/img','public/admin/img')
   .copy('node_modules/admin-lte/plugins','public/admin/plugins')
   .copy('node_modules/icheck/skins/square/blue.png','public/admin/css')
   .copy('node_modules/icheck/skins/square/blue@2x.png','public/admin/css');

/*----------
 FRONT END
 */
mix.less('resources/assets/front/css/main.less', 'public/front/css/main.css')
    .js('resources/assets/front/js/app.js', 'public/front/js')
    .copy('resources/assets/front/img','public/front/img')
    .copy('resources/assets/front/img/atuacao','public/front/img/atuacao')
    .copy('resources/assets/front/img/banner','public/front/img/banner')
    .copy('resources/assets/front/img/institucional','public/front/img/institucional')
    .copy('resources/assets/front/img/mercado','public/front/img/mercado')
    .copy('resources/assets/front/img/responsabilidade','public/front/img/responsabilidade');
