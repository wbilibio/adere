<?php

namespace App\Http\ViewComposers;

use App\Entities\Banners\Banners;
use App\Entities\Blog\PostCommentAnswers;
use App\Entities\Blog\PostComments;
use App\Entities\Configs\Configs;
use App\Entities\Markets\Market;
use App\Entities\Marks\Marks;
use App\Entities\Products\ProductFamily;
use App\Entities\Products\ProductType;
use App\Entities\Texts\Texts;
use Illuminate\View\View;

use JavaScript;

class ServiceComposer
{
    public function __construct()
    {
        //
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $lang_locale = app()->getLocale();

        // Obtém todos os mercados
        $markets = Market::where('status',1)->get();

        // Familias
        $families = new ProductFamily();

        // Pega todas Marcas Visíveis
        $marks = Marks::orderBy('order','desc')->where('status',1)->get();

        // Pega todos tipos (categoria) de produtos
        $types = ProductType::where('status',1)->get();

        // Pega todos banners
        $banner_menu_solution = Banners::where('type', 'menu-solution')->first();
        $banner_menu_market = Banners::where('type', 'menu-market')->first();
        $banner_about = Banners::where('type', 'about')->first();
        $banner_blog = Banners::where('type', 'blog')->first();

        // Pega todos textos do site
        $texts = Texts::first();

        $configs = Configs::first();

        $items_post_comments = PostComments::where('status',0)->orderBy('created_at','desc')->get();

        $items_comment_answers = PostCommentAnswers::where('status',0)->orderBy('created_at','desc')->get();

        $sharedData = collect([
            'markets' => $markets,
            'product_types' => $types,
            'lang_locale' => $lang_locale,
            'product_families' => $families,
            'marks' => $marks,
            'banner_menu_solution' => $banner_menu_solution,
            'banner_menu_market' => $banner_menu_market,
            'banner_about' => $banner_about,
            'banner_blog' => $banner_blog,
            'texts' => $texts,
            'configs' => $configs,
            'items_post_comments' => $items_post_comments,
            'items_comment_answers' => $items_comment_answers,
        ]);

        JavaScript::put([
            'markets_data' => $markets,
            'marks_data' => $marks
        ]);

        $view->with('sharedData', $sharedData);
    }
}
