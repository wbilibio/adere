<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;
use Session;
use Redirect;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class OuvidoriaController extends Controller
{
    public function index($locale)
    {
        app()->setLocale($locale);

        return view('front.ouvidoria');
    }
    public function messages()
    {
        return [
            'name.required' => 'Campo obrigatório',
            'name.alpha' => 'Nome não pode ter números',
            'email.required'  => 'Campo obrigatório',
            'email.email' => 'Formato de e-mail inválido',
            'text.required' => 'Campo obrigatório'
        ];
    }
    /**
     * @param Request $request
     * @return mixed
     */
    public function send(Request $request,$locale) {
        $this->validate($request, [
            'name' => 'required|alpha|max:255',
            'email' => 'required|email|max:255',
            'text' => 'required|max:255',
        ], $this->messages());

        $data['request'] = $request;
        Mail::send('emails.ouvidoria', $data, function($m){
            $m->to('contato@adere.com.br','Adere')->subject('Ouvidoria');
        });

        // check for failures
        if (count(Mail::failures()) > 0) {
            Session::flash('error', 'Falha ao enviar a mensagem, tente novamente mais tarde!');
            return Redirect::to($locale.'/ouvidoria/');
        } else {
            Session::flash('success', 'Sua mensagem foi enviada com sucesso!');
            return Redirect::to($locale.'/ouvidoria/');
        }
    }
}