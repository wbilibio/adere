<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Banners\Banners;
use App\Entities\Blog\Posts;
use App\Entities\Newsletter\Newsletter;
use App\Entities\Products\Product;
use App\Entities\Sliders\Sliders;
use App\Http\Requests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{

    public function index($locale)
    {
        app()->setLocale($locale);

        $items_slider = Sliders::orderBy('order', 'desc')->get();

        $items_products = Product::orderBy('viewed', 'desc')->get()->take(4);

        $items_posts = Posts::orderBy('created_at', 'desc')->get()->take(4);

        $banner_left = Banners::where('type', 'home-left')->first();
        $banner_right = Banners::where('type', 'home-right')->first();
        
        return view('front.home', compact('items_slider','items_products','items_posts', 'banner_left', 'banner_right'));
    }
    
    public function saveEmailNewsletter(Request $request){
        $email_existente = Newsletter::where('email', $request['email'])->get()->first();
        if(empty($email_existente)){
            $ultimo_item = Newsletter::orderBy('order','desc')->first();
            $request['order'] = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;
            if(Newsletter::create($request->all())) {
                return 1;
            } else {
                return 2;
            }
        } else {
            return 3;
        };
    }
}