<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Blog\Posts;
use App\Entities\Products\ProductFamily;
use App\Http\Requests;
use JavaScript;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class BlogController extends Controller
{
    public function index($locale,$slug=false)
    {
        app()->setLocale($locale);

        if($slug == false) $items_posts = Posts::all();
        else {
            $category = ProductFamily::where('slug',$slug)->first();
            $items_posts = $category->posts;
        }
        $items_posts_most_viewed = Posts::orderBy('viewed', 'desc')->get()->take(3);

        $items_families = ProductFamily::where('status',1)->get();
        $items_families->map(function($item_family){
            if(!empty($item_family->posts) && count($item_family->posts) > 0) $item_family->visible = true;
            else $item_family->visible = false;
        });

        JavaScript::put([
            'posts_data' => $items_posts
        ]);

        return view('front.blog', compact('items_families','items_posts','items_posts_most_viewed','category'));
    }
}