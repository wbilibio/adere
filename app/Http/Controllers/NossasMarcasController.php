<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Marks\Marks;
use App\Http\Requests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class NossasMarcasController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index($locale,$slug)
    {
        app()->setLocale($locale);

        $item_mark = Marks::where('slug',$slug)->where('status',1)->first();

        return view('front.nossas_marcas',compact('item_mark'));
    }
}