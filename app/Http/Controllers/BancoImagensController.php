<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\ImageBank\ImageBank;
use App\Http\Requests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class BancoImagensController extends Controller
{
    public function index($locale)
    {
        app()->setLocale($locale);

        $items_image = ImageBank::orderBy('order','desc')->where('status',1)->get();

        return view('front.banco_de_imagens',compact('items_image'));
    }
    public function list($locale,$slug)
    {
        app()->setLocale($locale);

        $item_category = ImageBank::where('slug',$slug)->first();
        $items_image = ImageBank::find($item_category->id)->images()->where('status',1)->get();
        return view('front.banco_de_imagens_lista',compact('items_image','item_category'));
    }
}