<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Entities\Blog\PostCommentAnswers;
use App\Entities\Blog\PostComments;
use App\Entities\Blog\Posts;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class CommentAnswersController extends Controller
{
    /**
     * @var PostCommentAnswers
     */
    protected $repository;
    /**
     * @var PostComments
     */
    protected $comments;
    /**
     * @var Posts
     */
    protected $posts;
    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(PostCommentAnswers $repository,PostComments $comments, Posts $posts, StandardService $standard)
    {
        $this->repository = $repository;
        $this->comments = $comments;
        $this->posts = $posts;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($post_id,$comment_id)
    {
        $item_post = $this->posts->find($post_id);
        $item_comment = $this->comments->find($comment_id);
        $items_list = $this->repository->where('post_comments_id',$comment_id)->get();

        $items_list->map(function ($item_list) {
            if(!empty($item_list->title)) $item_list->title_pt = $item_list->title;
            else $item_list->title_pt = 'sem título';
        });

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.blog.answers.index', compact('item_post','item_comment'));
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($post_id,$comment_id,$id)
    {
        $item = $this->repository->find($id);
        $item_post = $this->posts->find($post_id);
        $item_comment = $this->comments->find($comment_id);

        JavaScript::put([
            'status_data' => $item->status
        ]);

        return view('admin.blog.answers.edit', compact('item','item_post','item_comment'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request,$post_id,$comment_id,$id=0)
    {
        try {
            $item = $this->repository->find($id);

            $item->status = $request->input('status');

            $item->save();

            Session::flash('success', 'Resposta atualizada com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/blog/posts/'.$post_id.'/comentarios/'.$comment_id.'/respostas');
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($post_id,$id)
    {
        $item = $this->repository->find($id);

        $item->delete();

        Session::flash('success', 'Comentário excluído com sucesso!');
        return Redirect::to('/cms/blog/posts/'.$post_id.'/comentarios');
    }


}
