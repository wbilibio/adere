<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Entities\Blog\Posts;
use App\Entities\Blog\PostsCategories;
use App\Entities\Blog\PostsProductFamilies;
use App\Entities\Blog\PostTags;
use App\Entities\Products\ProductFamily;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class PostsController extends Controller
{

    /**
     * @var Posts
     */
    protected $repository;

    /**
     * @var ProductFamily
     */
    protected $families;

    /**
     * @var PostsCategories
     */
    protected $categories;

    /**
     * @var PostsProductFamilies
     */
    protected $postsProductFamilies;

    /**
     * @var PostTags
     */
    protected $postTags;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Posts $repository, ProductFamily $families,PostsProductFamilies $postsProductFamilies, PostTags $postTags, StandardService $standard)
    {
        $this->repository = $repository;
        $this->families = $families;
        $this->postsProductFamilies = $postsProductFamilies;
        $this->postTags = $postTags;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items_list = $this->repository->orderBy('created_at','desc')->get();

        $items_families = $this->families->orderBy('order','desc')->get();

        JavaScript::put([
            'posts_data' => $items_list
        ]);

        return view('admin.blog.posts.index', compact('items_families'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        try {
            $item = new $this->repository;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            $name_image = $this->standard->doUpload($request->file('file_image'),'_files/posts/',false,false);
            $item->image = $name_image;

            if($item->save()){
                $arrayTags = explode(",", $request->input('tags'));
                $collectTags = collect($arrayTags);
                if(count($collectTags) > 0){
                    $collectTags->map(function ($item_tag) use ($item){
                        $post_tags = new $this->postTags;
                        $post_tags->post_id = $item->id;
                        $post_tags->title = $item_tag;
                        $post_tags->save();
                    });

                }
                if(!empty($request->input('checklist'))){
                    $collectFamilies = collect($request->input('checklist'));
                    $collectFamilies->map(function ($item_family) use ($item){
                        $item_related = new $this->postsProductFamilies;
                        $item_related->posts_id = $item->id;
                        $item_related->product_families_id = $item_family;

                        $item_related->save();
                    });
                }
            }

            Session::flash('success', 'Post criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/blog/posts');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id=0)
    {
        $item = $this->repository->find($id);
        $items_families = $this->families->orderBy('order','desc')->get();
        $items_families->map(function($item_family) use ($id){
            $item_select = $this->postsProductFamilies->where('product_families_id',$item_family->id)->where('posts_id',$id)->first();
            if(!empty($item_select)) $item_family->select = true;
        });


        JavaScript::put([
            'tags_data' => $item->tags
        ]);
        
        return view('admin.blog.posts.edit', compact('item','items_families'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id=0)
    {
        try {
            $item = $this->repository->find($id);

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            if (!empty($request->file('file_image'))) {
                File::delete('_files/posts/' . $item->image);
                $image_name = $this->standard->doUpload($request->file('file_image'),'_files/posts/',false,false);
                $item->image = $image_name;
            }

            if($item->save()){
                if(!empty($request->input('tags'))){
                    $arrayTags = explode(",", $request->input('tags'));
                    $collectTags = collect($arrayTags);
                    $item->tags->map(function($tag_exist){
                        $tag_exist->delete();
                    });
                    $collectTags->map(function ($item_tag) use ($item){
                        $post_tags = new $this->postTags;
                        $post_tags->post_id = $item->id;
                        $post_tags->title = $item_tag;
                        $post_tags->save();
                    });
                }
                $this->postsProductFamilies->where('posts_id',$id)->delete();
                if(!empty($request->input('checklist'))){
                    $collectFamilies = collect($request->input('checklist'));
                    $collectFamilies->map(function ($item_family) use ($item){
                        $item_related = new $this->postsProductFamilies;
                        $item_related->posts_id = $item->id;
                        $item_related->product_families_id = $item_family;

                        $item_related->save();
                    });
                }
            }

            Session::flash('success', 'Post atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/blog/posts');
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $item = $this->repository->find($id);

        if(!empty($item->image)) File::delete('_files/posts/'.$item->image);

        $item->delete();

        Session::flash('success', 'Post excluído com sucesso!');
        return Redirect::to('/cms/blog/posts');
    }


}
