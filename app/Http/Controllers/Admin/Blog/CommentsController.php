<?php

namespace App\Http\Controllers\Admin\Blog;


use App\Entities\Blog\PostComments;
use App\Entities\Blog\Posts;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class CommentsController extends Controller
{
    /**
     * @var PostComments
     */
    protected $repository;
    /**
     * @var Posts
     */
    protected $posts;
    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(PostComments $repository, Posts $posts, StandardService $standard)
    {
        $this->repository = $repository;
        $this->posts = $posts;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($post_id)
    {
        $item_post = $this->posts->find($post_id);
        $items_list = $item_post->comments()->orderBy('created_at','desc')->get();

        $items_list->map(function ($item_list) {
            if(!empty($item_list->title)) $item_list->title_pt = $item_list->title;
            else $item_list->title_pt = 'sem título';
        });

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.blog.comments.index', compact('item_post'));
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($post_id,$id)
    {
        $item = $this->repository->find($id);
        $item_post = $this->posts->find($post_id);
        JavaScript::put([
            'status_data' => $item->status
        ]);
        return view('admin.blog.comments.edit', compact('item','item_post'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request,$post_id, $id=0)
    {
        try {
            $item = $this->repository->find($id);

            $item->status = $request->input('status');

            $item->save();

            Session::flash('success', 'Comentário atualizada com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/blog/posts/'.$post_id.'/comentarios');
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($post_id,$id)
    {
        $item = $this->repository->find($id);

        $item->delete();

        Session::flash('success', 'Comentário excluído com sucesso!');
        return Redirect::to('/cms/blog/posts/'.$post_id.'/comentarios');
    }


}
