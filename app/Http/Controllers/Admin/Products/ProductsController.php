<?php

namespace App\Http\Controllers\Admin\Products;

use App\Entities\Marks\Marks;
use App\Entities\Products\Product;
use App\Entities\Products\ProductFamily;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class ProductsController extends Controller
{

    /**
     * @var Product
     */
    protected $repository;

    /**
     * @var ProductFamily
     */
    protected $productFamily;

    /**
     * @var Marks
     */
    protected $marks;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Product $repository, ProductFamily $productFamily, Marks $marks, StandardService $standard)
    {
        $this->repository = $repository;
        $this->productFamily = $productFamily;
        $this->marks = $marks;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items_list = $this->repository->orderBy('order','desc')->get();
        $items_families = $this->productFamily->all();
        $items_marks = $this->marks->all();

        $items_list->map(function ($item_list) {
            $item_list->subcategory = $this->productFamily->find($item_list->product_family_id)->title;
        });

        JavaScript::put([
            'products_data' => $items_list
        ]);

        return view('admin.products.index', compact('items_families','items_marks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request)
    {
        try {
            $item = new $this->repository;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item[$locale.'_title'] = $request->input($locale.'_title');
                $item[$locale.'_description'] = $request->input($locale.'_description');
                $item[$locale.'_application'] = $request->input($locale.'_application');
            }

            $ultimo_item = $this->repository->orderBy('order','desc')->first();
            $item->order = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

            $item->product_family_id = $request->input('product_family_id');
            $item->mark_id = $request->input('mark_id');
            $item->url_loja_virtual = $request->input('url_loja_virtual');
            $item->status = $request->input('status');
            $item->save();

            Session::flash('success', 'Produto adicionado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/produtos/lista');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id=0)
    {
        $item = $this->repository->find($id);
        JavaScript::put([
            'status_data' => $item->status
        ]);
        $items_families = $this->productFamily->all();
        $items_marks = $this->marks->all();
        return view('admin.products.edit', compact('item','items_families','items_marks'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id=0)
    {
        try {
            $item = $this->repository->find($id);

            foreach (['pt', 'en', 'es'] as $locale) {
                $item[$locale.'_title'] = $request->input($locale.'_title');
                $item[$locale.'_description'] = $request->input($locale.'_description');
                $item[$locale.'_application'] = $request->input($locale.'_application');
            }

            $item->product_family_id = $request->input('product_family_id');
            $item->mark_id = $request->input('mark_id');
            $item->url_loja_virtual = $request->input('url_loja_virtual');
            $item->status = $request->input('status');
            $item->save();

            Session::flash('success', 'Produto atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/produtos/lista');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $item = $this->repository->find($id);

        $item->delete();

        Session::flash('success', 'Produto excluído com sucesso!');
        return Redirect::to('/cms/produtos');
    }


}
