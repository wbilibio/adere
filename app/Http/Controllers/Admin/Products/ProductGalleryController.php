<?php

namespace App\Http\Controllers\Admin\Products;

use App\Entities\Products\Product;
use App\Entities\Products\ProductGallery;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class ProductGalleryController extends Controller
{
    /**
     * @var ProductGallery
     */
    protected $repository;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(ProductGallery $repository, Product $product, StandardService $standard)
    {
        $this->repository = $repository;
        $this->product = $product;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=0)
    {
        $item = $this->product->find($id);
        $items_gallery = $this->repository->where('product_id',$id)->orderBy('order','desc')->get();
        
        JavaScript::put([
            'items_gallery_data' => $items_gallery
        ]);

        return view('admin.products.gallery.index', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO IMAGENS NA GALERIA
    public function store(Request $request, $id=0)
    {
        if ($request->file('file')->isValid()) {
            list($width, $height) = getimagesize($request->file('file'));

            if($width < 3000){
                if($width > 800) $resize = 800;
                else $resize = false;
            } else {
                return 3;
            }
        } else {
            return 3;
        }
        $name_image = $this->standard->doUpload($request->file('file'),'_files/produtos_galeria/',$resize,false);
        $request['image'] = $name_image;

        $ultimo_item = $this->repository->orderBy('order','desc')->first();
        $request['order'] = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

        $request['product_id'] = $id;

        if($this->repository->create($request->all())){
            return 1;
        };

        return Response::json(['success' => true, 'files' => asset('_files/produtos_galeria/'.$name_image)]);
    }

    // Reordenando utilizando Jquery-ui Sortable
    public function sortable(Request $request,$id)
    {
        $this->doReorder($request->input('items'), true,$id);
        return 1;
    }

    protected function doReorder($items = [], $grid = false,$id) {
        if(count($items) == 0) $items = $this->repository->where('product_id',$id)->get();

        $order = count($items);
        foreach($items as $item){
            $item = $this->repository->find($grid ? $item['id'] : $item->id);
            $item->order = $order;
            $item->save();
            $order--;
        }
    }
    public function getItemsGallery($id)
    {
        $items_gallery = $this->repository->where('product_id',$id)->get();
        return $items_gallery;
    }
    public function destroy($id)
    {
        $item = $this->repository->find($id);

        if(!empty($item->image))
            File::delete('_files/produtos_galeria/'.$item->image);

        if($this->repository->destroy($id)){
            return 1;
        } else {
            return 2;
        }
    }


}
