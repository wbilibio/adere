<?php

namespace App\Http\Controllers\Admin\Products;

use App\Entities\Products\ProductType;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class ProductTypeController extends Controller
{

    /**
     * @var ProductType
     */
    protected $repository;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(ProductType $repository, StandardService $standard)
    {
        $this->repository = $repository;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items_list = $this->repository->orderBy('order','desc')->get();

        $items_list->map(function ($item_list) {
            if(!empty($item_list->title)) $item_list->title_pt = $item_list->title;
            else $item_list->title_pt = 'sem título';
        });

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.products.category.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request)
    {
        try {
            $item = new $this->repository;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item[$locale.'_title'] = $request->input($locale.'_title');
            }

            $ultimo_item = $this->repository->orderBy('order','desc')->first();
            $item->order = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

            $item->status = $request->input('status');

            $item->save();

            Session::flash('success', 'Categoria criada com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/produtos/categorias');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id=0)
    {
        $item = $this->repository->find($id);
        JavaScript::put([
            'status_data' => $item->status
        ]);
        return view('admin.products.category.edit', compact('item'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id=0)
    {
        try {
            $item = $this->repository->find($id);

            foreach (['pt', 'en', 'es'] as $locale) {
                $item[$locale.'_title'] = $request->input($locale.'_title');
            }

            $item->status = $request->input('status');

            $item->save();

            Session::flash('success', 'Categoria atualizada com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/produtos/categorias');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $item = $this->repository->find($id);

        $item->delete();

        Session::flash('success', 'Categoria excluída com sucesso!');
        return Redirect::to('/cms/produtos/categorias');
    }


}
