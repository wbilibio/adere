<?php

namespace App\Http\Controllers\Admin\Products;

use App\Entities\Products\Product;
use App\Entities\Products\Applications;
use App\Entities\Products\ProductsRelatedApplications;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class ProductsApplicationsController extends Controller
{

    /**
     * @var Applications
     */
    protected $repository;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ProductsRelatedApplications
     */
    protected $products_related;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Applications $repository, Product $product, ProductsRelatedApplications $products_related, StandardService $standard)
    {
        $this->repository = $repository;
        $this->product = $product;
        $this->products_related = $products_related;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items_list = $this->repository->all();
        $items_products = $this->product->all();
        $items_list->map(function ($item_list) {
            if(!empty($item_list->getTranslation('pt')->title)) $item_list->title_pt = $item_list->getTranslation('pt')->title;
            else $item_list->title_pt = 'sem título';
        });

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.products.applications.index', compact('items_products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request)
    {
        try {
            $item = new $this->repository;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
            }

            $item->status = $request->input('status');

            if($item->save()){
                if(!empty($request->input('checklist'))){
                    $collectProducts = collect($request->input('checklist'));
                    $collectProducts->map(function ($item_product) use ($item){
                        $item_related = new $this->products_related;
                        $item_related->product_id = $item_product;
                        $item_related->application_id = $item->id;

                        $item_related->save();
                    });
                }
            };

            Session::flash('success', 'Item criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/produtos/aplicacoes');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id=0)
    {
        $item = $this->repository->find($id);
        JavaScript::put([
            'status_data' => $item->status
        ]);
        $items_products = $this->product->all();
        $items_products->map(function($item_product) use ($id){
            $item_select = $this->products_related->where('product_id',$item_product->id)->where('application_id',$id)->first();
            if(!empty($item_select)) $item_product->select = true;
        });
        return view('admin.products.applications.edit', compact('item','items_products'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id=0)
    {
        try {
            $item = $this->repository->find($id);

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
            }

            $item->status = $request->input('status');
            if($item->save()){
                $this->products_related->where('application_id',$id)->delete();
                if(!empty($request->input('checklist'))){
                    $collectProducts = collect($request->input('checklist'));
                    $collectProducts->map(function ($item_product) use ($item){
                        $item_related = new $this->products_related;
                        $item_related->product_id = $item_product;
                        $item_related->application_id = $item->id;

                        $item_related->save();
                    });
                }
            };

            Session::flash('success', 'Arquivo atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/produtos/aplicacoes');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $item = $this->repository->find($id);

        $item->delete();

        Session::flash('success', 'Arquivo excluído com sucesso!');
        return Redirect::to('/cms/produtos/aplicacoes');
    }


}
