<?php

namespace App\Http\Controllers\Admin\Products;

use App\Entities\Products\Product;
use App\Entities\Products\ProductsVideos;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class ProductsVideosController extends Controller
{

    /**
     * @var ProductsVideos
     */
    protected $repository;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(ProductsVideos $repository,Product $product, StandardService $standard)
    {
        $this->repository = $repository;
        $this->product = $product;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=0)
    {
        $item = $this->product->find($id);
        $items_list = $this->repository->where('product_id',$id)->orderBy('order','desc')->get();

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.products.videos.index',compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request, $id=0)
    {
        try {
            $item = new $this->repository;

            $image = $this->standard->doUpload($request->file('file_image'),'_files/produtos_videos/',false,false);
            $item->image = $image;

            $ultimo_item = $this->repository->orderBy('order','desc')->first();
            $item->order = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

            $item->link_youtube = $request->input('link_youtube');

            $item->product_id = $id;

            $item->save();

            Session::flash('success', 'Video criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/produtos/videos/'. $id);
    }

    /**
     * @param int $product_id
     * @param int $id
     * @return mixed
     */
    public function edit($product_id,$id)
    {
        $item = $this->repository->find($id);
        $item_product = $this->product->find($product_id);
        return view('admin.products.videos.edit', compact('item','item_product'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @param int $product_id
     * @return mixed
     */
    public function update(Request $request, $product_id,$id)
    {
        try {
            $item = $this->repository->find($id);

            if (!empty($request->file('file_image'))) {
                File::delete('_files/produtos_videos/' . $item->image);
                $image_name = $this->standard->doUpload($request->file('file_image'),'_files/produtos_videos/',false,false);
                $item->image = $image_name;
            }

            $item->link_youtube =  $request->input('link_youtube');
            $item->save();

            Session::flash('success', 'Video atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/produtos/videos/'.$product_id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @param $product_id
     * @return int
     */
    public function destroy($product_id,$id)
    {
        $item = $this->repository->find($id);

        if(!empty($item->image)) File::delete('_files/produtos_videos/'.$item->image);

        $item->delete();

        Session::flash('success', 'Item excluído com sucesso!');
        return Redirect::to('/cms/produtos/videos/'.$product_id);
    }


}
