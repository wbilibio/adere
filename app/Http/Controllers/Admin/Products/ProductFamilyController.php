<?php

namespace App\Http\Controllers\Admin\Products;

use App\Entities\Products\ProductFamily;
use App\Entities\Products\ProductType;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class ProductFamilyController extends Controller
{

    /**
     * @var ProductFamily
     */
    protected $repository;

    /**
     * @var ProductType
     */
    protected $productType;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(ProductFamily $repository, ProductType $productType, StandardService $standard)
    {
        $this->repository = $repository;
        $this->productType = $productType;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items_list = $this->repository->orderBy('order','desc')->get();
        $items_types = $this->productType->all();

        $items_list->map(function ($item_list) {
            $item_list->category = $this->productType->find($item_list->product_type_id)->title;
        });

        JavaScript::put([
            'subcategory_data' => $items_list
        ]);

        return view('admin.products.subcategory.index', compact('items_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request)
    {
        try {
            $item = new $this->repository;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item[$locale.'_title'] = $request->input($locale.'_title');
                $item[$locale.'_description'] = $request->input($locale.'_description');
            }

            $ultimo_item = $this->repository->orderBy('order','desc')->first();
            $item->order = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

            $item->product_type_id = $request->input('product_type_id');

            $item->status = $request->input('status');
            $item->save();

            Session::flash('success', 'Subcategoria criada com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/produtos/subcategorias');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id=0)
    {
        $item = $this->repository->find($id);
        JavaScript::put([
            'status_data' => $item->status
        ]);
        $items_types = $this->productType->all();
        return view('admin.products.subcategory.edit', compact('item','items_types'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id=0)
    {
        try {
            $item = $this->repository->find($id);

            foreach (['pt', 'en', 'es'] as $locale) {
                $item[$locale.'_title'] = $request->input($locale.'_title');
                $item[$locale.'_description'] = $request->input($locale.'_description');
            }

            $item->product_type_id = $request->input('product_type_id');
            $item->status = $request->input('status');
            $item->save();

            Session::flash('success', 'Subcategoria atualizada com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/produtos/subcategorias');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $item = $this->repository->find($id);

        $item->delete();

        Session::flash('success', 'Subategoria excluída com sucesso!');
        return Redirect::to('/cms/produtos/subcategorias');
    }


}
