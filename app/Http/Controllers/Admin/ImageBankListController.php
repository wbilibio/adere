<?php

namespace App\Http\Controllers\Admin;

use App\Entities\ImageBank\ImageBank;
use App\Entities\ImageBank\ImageBankList;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class ImageBankListController extends Controller
{
    /**
     * @var ImageBankList
     */
    protected $repository;

    /**
     * @var ImageBank
     */
    protected $category;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(ImageBankList $repository,ImageBank $category, StandardService $standard)
    {
        $this->repository = $repository;
        $this->category = $category;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category_id=0)
    {

        $items_list = $this->repository->orderBy('order','desc')->where('image_bank_id',$category_id)->get();
        $item_category = $this->category->find($category_id);

        $items_list->map(function ($item_list) {
            if(!empty($item_list->getTranslation('pt')->title)) $item_list->title_pt = $item_list->getTranslation('pt')->title;
            else $item_list->title_pt = 'sem título';
        });

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.image_bank.list.index',compact('item_category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request,$id=0)
    {
        try {
            $item = new $this->repository;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
            }
            $name_image = $this->standard->doUpload($request->file('file_image'),'_files/banco_de_imagens/'.$id.'/',false,false);
            $item->image = $name_image;

            $name_thumb = $this->standard->doUpload($request->file('file_image'),'_files/banco_de_imagens/'.$id.'/',225,false);
            $item->image_thumb = $name_thumb;

            $item->image_bank_id = $request->input('image_bank_id');
            $item->status = $request->input('status');

            $ultimo_item = $this->repository->orderBy('order','desc')->first();
            $item->order = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

            $item->save();

            Session::flash('success', 'Item criada com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/banco_de_imagens/lista/'.$id);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($category_id,$id=0)
    {
        $item = $this->repository->find($id);
        $item_category = $this->category->find($category_id);

        JavaScript::put([
            'status_data' => $item->status
        ]);
        return view('admin.image_bank.list.edit', compact('item','item_category'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request,$category_id,$id=0)
    {
        try {
            $item = $this->repository->find($id);

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
            }

            if (!empty($request->file('file_image'))) {
                File::delete('_files/banco_de_imagens/'.$category_id.'/'.$item->image);
                $name_image = $this->standard->doUpload($request->file('file_image'),'_files/banco_de_imagens/'.$category_id.'/',false,false);
                $item->image = $name_image;
                File::delete('_files/banco_de_imagens/'.$category_id.'/'.$item->image_thumb);
                $name_thumb = $this->standard->doUpload($request->file('file_image'),'_files/banco_de_imagens/'.$category_id.'/',225,false);
                $item->image_thumb = $name_thumb;
            }

            $item->image_bank_id = $request->input('image_bank_id');
            $item->status = $request->input('status');

            $item->save();

            Session::flash('success', 'Item atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/banco_de_imagens/lista/'.$category_id);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request,$id)
    {
        $this->doReorder($request->input('items'), true, $id);
        return 2;
    }


    protected function doReorder($items = [], $grid = false,$id) {
        if(count($items) == 0) $items = $this->repository->where('image_bank_id',$id)->get();

        $order = count($items);
        foreach($items as $item){
            $item = $this->repository->find($grid ? $item['id'] : $item->id);
            $item->order = $order;
            $item->save();
            $order--;
        }
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($category_id,$id)
    {
        $item = $this->repository->find($id);

        if(!empty($item->image)) File::delete('_files/banco_de_imagens/'.$category_id.'/'.$item->image);
        if(!empty($item->image_thumb)) File::delete('_files/banco_de_imagens/'.$category_id.'/'.$item->image_thumb);

        $item->delete();

        Session::flash('success', 'Item excluído com sucesso!');
        return Redirect::to('/cms/banco_de_imagens/lista/'.$category_id);
    }


}
