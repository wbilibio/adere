<?php

namespace App\Http\Controllers\Admin;

use App\Entities\ImageUpload\ImageUpload;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;
class ImageUploadController extends Controller
{

    /**
     * @var ImageUpload
     */
    protected $repository;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(ImageUpload $repository, StandardService $standard)
    {
        $this->repository = $repository;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.upload_image.image-dialog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function imageUpload(Request $request)
    {
        $item = new $this->repository;

        $file = $request->file('imagefile');
        $name = uniqid().$this->standard->createNameFile($request->file('imagefile'));
        $file->move('_files/upload_image/',$name);
        $file_path = url(asset('_files/upload_image/'.$name));

        $item->image = $name;
        $item->save();

        return view('admin.upload_image.image-upload', compact('file_path'));
    }

}
