<?php

namespace App\Http\Controllers\Admin;


use App\Entities\Configs\UsersDownloadFile;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use JavaScript;
use Response;
use Redirect;
use Session;

class ClientsDownloadFilesController extends Controller
{

    /**
     * @var UsersDownloadFile
     */
    protected $repository;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(UsersDownloadFile $repository, StandardService $standard)
    {
        $this->repository = $repository;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items_list = $this->repository->all();

        $items_list->map(function ($item_list) {
            $item_list->title_pt = $item_list->email;
        });

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.clients_download_files.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id=0)
    {
        $item = $this->repository->find($id);
        return view('admin.clients_download_files.edit',compact('item'));
    }



    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $item = $this->repository->find($id);

        $item->delete();

        Session::flash('success', 'Item excluído com sucesso!');
        return Redirect::to('/cms/clientes_download_arquivos');
    }
}
