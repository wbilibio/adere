<?php

namespace App\Http\Controllers\Admin\Markets;

use App\Entities\Markets\Market;
use App\Entities\Markets\MarketsFiles;
use App\Entities\Markets\MarketsRelatedFiles;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class MarketsFilesController extends Controller
{

    /**
     * @var MarketsFiles
     */
    protected $repository;

    /**
     * @var Market
     */
    protected $market;

    /**
     * @var MarketsRelatedFiles
     */
    protected $marketsRelated;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(MarketsFiles $repository, Market $market, MarketsRelatedFiles $marketsRelated, StandardService $standard)
    {
        $this->repository = $repository;
        $this->market = $market;
        $this->marketRelated = $marketsRelated;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items_list = $this->repository->all();
        $items_markets = $this->market->all();
        $items_list->map(function ($item_list) {
            if(!empty($item_list->getTranslation('pt')->title)) $item_list->title_pt = $item_list->getTranslation('pt')->title;
            else $item_list->title_pt = 'sem título';
        });

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.markets.archive.index', compact('items_markets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request)
    {
        try {
            $item = new $this->repository;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
            }

            $name = $this->standard->anyFile($request->file('file_archive'),'_files/mercados_arquivos/',$this->standard->createNameFile($request->file('file_archive')));
            $item->file = $name;

            $item->status = $request->input('status');
            $item->guia = $request->input('guia');

            if($item->save()){
                if(!empty($request->input('checklist'))){
                    $collectMarkets = collect($request->input('checklist'));
                    $collectMarkets->map(function ($item_market) use ($item){
                        $item_related = new $this->marketRelated;
                        $item_related->market_id = $item_market;
                        $item_related->file_id = $item->id;

                        $item_related->save();
                    });
                }
            };

            Session::flash('success', 'Item criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/mercados/arquivos');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id=0)
    {
        $item = $this->repository->find($id);
        JavaScript::put([
            'status_data' => $item->status,
            'guia_data' => $item->guia
        ]);
        $items_markets = $this->market->all();
        $items_markets->map(function($item_market) use ($id){
            $item_select = $this->marketRelated->where('market_id',$item_market->id)->where('file_id',$id)->first();
            if(!empty($item_select)) $item_market->select = true;
        });
        return view('admin.markets.archive.edit', compact('item','items_markets'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id=0)
    {
        try {
            $item = $this->repository->find($id);

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
            }

            if (!empty($request->file('file_archive'))) {
                File::delete('_files/mercados_arquivos/' . $item->file);
                $name = $this->standard->anyFile($request->file('file_archive'),'_files/mercados_arquivos/',$this->standard->createNameFile($request->file('file_archive')));
                $item->file = $name;
            }
            $item->status = $request->input('status');
            $item->guia = $request->input('guia');

            if($item->save()){
                $this->marketRelated->where('file_id',$id)->delete();
                if(!empty($request->input('checklist'))){
                    $collectMarkets = collect($request->input('checklist'));
                    $collectMarkets->map(function ($item_market) use ($item){
                        $item_related = new $this->marketRelated;
                        $item_related->market_id = $item_market;
                        $item_related->file_id = $item->id;

                        $item_related->save();
                    });
                }
            };

            Session::flash('success', 'Arquivo atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/mercados/arquivos');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $item = $this->repository->find($id);

        if(!empty($item->file)) File::delete('_files/mercados_arquivos/'.$item->file);

        $item->delete();

        Session::flash('success', 'Arquivo excluído com sucesso!');
        return Redirect::to('/cms/mercados/arquivos');
    }


}
