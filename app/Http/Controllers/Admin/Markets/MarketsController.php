<?php

namespace App\Http\Controllers\Admin\Markets;

use App\Entities\Markets\Market;
use App\Entities\Markets\MarketProduct;
use App\Entities\Products\Product;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class MarketsController extends Controller
{

    /**
     * @var Market
     */
    protected $repository;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var MarketProduct
     */
    protected $products_related;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Market $repository,Product $product, MarketProduct $products_related, StandardService $standard)
    {
        $this->repository = $repository;
        $this->product = $product;
        $this->products_related = $products_related;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items_list = $this->repository->orderBy('order','desc')->get();
        $items_products = $this->product->all();

        $items_list->map(function ($item_list) {
            if(!empty($item_list->title)) $item_list->title_pt = $item_list->title;
            else $item_list->title_pt = 'sem título';
        });

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.markets.index', compact('items_products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request)
    {
        try {
            $item = new $this->repository;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item[$locale.'_title'] = $request->input($locale.'_title');
                $item[$locale.'_description'] = $request->input($locale.'_description');
            }
            $name_icon = $this->standard->doUpload($request->file('file_icon'),'_files/mercados/',false,false);
            $item->icon = $name_icon;

            $name_image = $this->standard->doUpload($request->file('file_image'),'_files/mercados/',false,false);
            $metadata['image'] = $name_image;

            $item->metadata = $metadata;

            $ultimo_item = $this->repository->orderBy('order','desc')->first();
            $item->order = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

            $item->status = $request->input('status');

            if($item->save()){
                if(!empty($request->input('checklist'))){
                    $collectProducts = collect($request->input('checklist'));
                    $collectProducts->map(function ($item_product) use ($item){
                        $item_related = new $this->products_related;
                        $item_related->product_id = $item_product;
                        $item_related->market_id = $item->id;

                        $item_related->save();
                    });
                }
            };

            Session::flash('success', 'Categoria criada com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/mercados/categorias');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id=0)
    {
        $item = $this->repository->find($id);
        JavaScript::put([
            'status_data' => $item->status
        ]);
        $items_products = $this->product->all();
        $items_products->map(function($item_product) use ($id){
            $item_select = $this->products_related->where('product_id',$item_product->id)->where('market_id',$id)->first();
            if(!empty($item_select)) $item_product->select = true;
        });
        return view('admin.markets.edit', compact('item','items_products'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id=0)
    {
        try {
            $item = $this->repository->find($id);

            foreach (['pt', 'en', 'es'] as $locale) {
                $item[$locale.'_title'] = $request->input($locale.'_title');
                $item[$locale.'_description'] = $request->input($locale.'_description');
            }

            if (!empty($request->file('file_icon'))) {
                File::delete('_files/mercados/' . $item->icon);
                $name_icon = $this->standard->doUpload($request->file('file_icon'), '_files/mercados/', false, false);
                $item->icon = $name_icon;
            }

            if (!empty($request->file('file_image'))) {
                if(!empty($item->metadata->image)) File::delete('_files/mercados/' . $item->metadata->image);
                $name_image = $this->standard->doUpload($request->file('file_image'),'_files/mercados/',false,false);
                $metadata['image'] = $name_image;

                $item->metadata = $metadata;
            }

            $item->status = $request->input('status');
            if($item->save()){
                $this->products_related->where('market_id',$id)->delete();
                if(!empty($request->input('checklist'))){
                    $collectProducts = collect($request->input('checklist'));
                    $collectProducts->map(function ($item_product) use ($item){
                        $item_related = new $this->products_related;
                        $item_related->product_id = $item_product;
                        $item_related->market_id = $item->id;

                        $item_related->save();
                    });
                }
            };

            Session::flash('success', 'Categoria atualizada com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/mercados/categorias');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $item = $this->repository->find($id);
        if(!empty($item->icon)) File::delete('_files/mercados/'.$item->icon);
        if(!empty($item->metadata->image)) File::delete('_files/mercados/'.$item->metadata->image);


        $item->delete();

        Session::flash('success', 'Categoria excluída com sucesso!');
        return Redirect::to('/cms/mercados/categorias');
    }


}
