<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Faq\Faq;
use App\Entities\Faq\FaqMarkets;
use App\Entities\Faq\FaqProductTypes;
use App\Entities\Markets\Market;
use App\Entities\Products\ProductType;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class FaqController extends Controller
{

    /**
     * @var Faq
     */
    protected $repository;

    /**
     * @var ProductType
     */
    protected $product_types;

    /**
     * @var Market
     */
    protected $markets;

    /**
     * @var FaqProductTypes
     */
    protected $faqProductTypes;

    /**
     * @var FaqMarkets
     */
    protected $faqMarkets;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Faq $repository, ProductType $product_types, FaqProductTypes $faqProductTypes, FaqMarkets $faqMarkets, Market $markets, StandardService $standard)
    {
        $this->repository = $repository;
        $this->product_types = $product_types;
        $this->markets = $markets;

        $this->faqProductTypes = $faqProductTypes;
        $this->faqMarkets = $faqMarkets;

        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items_list = $this->repository->orderBy('order','desc')->get();

        $items_product_types = $this->product_types->orderBy('order','desc')->get();

        $items_markets = $this->markets->orderBy('order','desc')->get();

        $items_list->map(function ($item_list) {
            if(!empty($item_list->getTranslation('pt')->title)) $item_list->title_pt = $item_list->getTranslation('pt')->title;
            else $item_list->title_pt = 'sem título';
        });

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.faq.index',compact('items_product_types','items_markets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request)
    {
        try {
            $item = new $this->repository;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            $ultimo_item = $this->repository->orderBy('order','desc')->first();
            $item->order = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

            $item->status = $request->input('status');
            if($item->save()) {
                if (!empty($request->input('list_types'))) {
                    $collectProductTypes = collect($request->input('list_types'));
                    $collectProductTypes->map(function ($item_type) use ($item) {
                        $item_related = new $this->faqProductTypes;
                        $item_related->faq_id = $item->id;
                        $item_related->product_types_id = $item_type;
                        $item_related->save();
                    });
                }
                if (!empty($request->input('list_markets'))) {
                    $collectMarkets = collect($request->input('list_markets'));
                    $collectMarkets->map(function ($item_market) use ($item) {
                        $item_related_markets = new $this->faqMarkets;
                        $item_related_markets->faq_id = $item->id;
                        $item_related_markets->markets_id = $item_market;
                        $item_related_markets->save();
                    });
                }
            }

            Session::flash('success', 'Item criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/faq');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id=0)
    {
        $item = $this->repository->find($id);

        $items_product_types = $this->product_types->orderBy('order','desc')->get();
        $items_product_types->map(function($item_product_type) use ($id){
            $item_select = $this->faqProductTypes->where('product_types_id',$item_product_type->id)->where('faq_id',$id)->first();
            if(!empty($item_select)) $item_product_type->select = true;
        });

        $items_markets = $this->markets->orderBy('order','desc')->get();
        $items_markets->map(function($item_market) use ($id){
            $item_select = $this->faqMarkets->where('markets_id',$item_market->id)->where('faq_id',$id)->first();
            if(!empty($item_select)) $item_market->select = true;
        });

        return view('admin.faq.edit', compact('item','items_product_types','items_markets'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id=0)
    {
        try {
            $item = $this->repository->find($id);

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            $item->status = $request->input('status');
            if($item->save()) {
                $this->faqProductTypes->where('faq_id', $id)->delete();
                if (!empty($request->input('list_types'))) {
                    $collectProductTypes = collect($request->input('list_types'));
                    $collectProductTypes->map(function ($item_type) use ($item) {
                        $item_related = new $this->faqProductTypes;
                        $item_related->faq_id = $item->id;
                        $item_related->product_types_id = $item_type;
                        $item_related->save();
                    });
                }
                $this->faqMarkets->where('faq_id', $id)->delete();
                if (!empty($request->input('list_markets'))) {
                    $collectMarkets = collect($request->input('list_markets'));
                    $collectMarkets->map(function ($item_market) use ($item) {
                        $item_related_markets = new $this->faqMarkets;
                        $item_related_markets->faq_id = $item->id;
                        $item_related_markets->markets_id = $item_market;
                        $item_related_markets->save();
                    });
                }
            }

            Session::flash('success', 'Pergunta atualizada com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/faq');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $item = $this->repository->find($id);

        if(!empty($item->image)) File::delete('_files/sliders/'.$item->image);
        if(!empty($item->image_background)) File::delete('_files/sliders/'.$item->image_background);

        $item->delete();

        Session::flash('success', 'Item excluído com sucesso!');
        return Redirect::to('/cms/sliders');
    }


}
