<?php

namespace App\Http\Controllers\Admin\Company;


use App\Entities\Company\About\Institutional;
use App\Entities\Company\About\InstitutionalGallery;
use App\Entities\Company\About\AboutConcept;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class InstitutionalController extends Controller
{
    /**
     * @var Institutional
     */
    protected $repository;

    /**
     * @var InstitutionalGallery
     */
    protected $gallery;

    /**
     * @var AboutConcept
     */
    protected $concepts;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Institutional $repository, InstitutionalGallery $gallery, AboutConcept $concepts, StandardService $standard)
    {
        $this->repository = $repository;
        $this->gallery = $gallery;
        $this->concepts = $concepts;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = $this->repository->first();
        $items_gallery = $this->gallery->orderBy('order','desc')->get();
        $items_list = $this->concepts->orderBy('order','desc')->get();

        $items_list->map(function ($item_list) {
            if(!empty($item_list->getTranslation('pt')->title)) $item_list->title_pt = $item_list->getTranslation('pt')->title;
            else $item_list->title_pt = 'sem título';
        });
        
        JavaScript::put([
            'items_gallery_data' => $items_gallery,
            'items_list_data' => $items_list
        ]);
        if (request()->wantsJson()) {
            return response()->json([
                'data' => $item,
            ]);
        }

        return view('admin.company.institutional.form', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO E ATUALIZANDO TEXTOS
    public function store(Request $request,$id=0)
    {
        try {
            if($id == 0){
                $item = new Institutional();
            } else {
                $item = Institutional::find($id);
            }

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            $item->save();

            Session::flash('success', 'Edição nos textos concluída!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/empresa/institucional');
    }

    /**
     GALERIA DE IMAGEM
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO IMAGENS NA GALERIA INSTITUCIONAL
    public function storeGallery(Request $request)
    {
        $image = $this->standard->doUpload($request->file('file'),'_files/institucional_galeria/',false,false);
        $request['image'] = $image;

        $ultimo_item = $this->gallery->orderBy('order','desc')->first();
        $request['order'] = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

        $this->gallery->create($request->all());

        return Response::json(['success' => true, 'files' => asset('_files/institucional_galeria/'.$image)]);
    }
    // Reordenando utilizando Jquery-ui Sortable
    public function sortableGallery(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->gallery);
        return 1;
    }


    public function getItemsGallery()
    {
        $items_gallery = $this->gallery->orderBy('order','desc')->get();

        return $items_gallery;
    }
    public function destroyItemGallery($id)
    {
        $item = $this->gallery->find($id);

        if(!empty($item->image))
            File::delete('_files/institucional_galeria/'.$item->image);

        if($this->gallery->destroy($id)){
            return 1;
        } else {
            return 2;
        }
    }


    /**
    CONCEITOS ESTRATEGICOS
     */

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeConcept(Request $request)
    {
        try {
            $item = new AboutConcept();

            $image = $this->standard->doUpload($request->file('file_image'),'_files/institucional_conceitos/',false,false);
            $item->image = $image;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            $item->save();

            Session::flash('success', 'Edição nos textos concluída!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/empresa/institucional#tab_3');

    }
    public function editConcept($id=0)
    {
        $item = $this->concepts->find($id);
        return view('admin.company.institutional.edit_concept', compact('item'));
    }
    public function updateConcept(Request $request, $id)
    {
        $item = $this->concepts->find($id);

        if (!empty($request->file('file_image'))) {
            File::delete('_files/institucional_conceitos/' . $item->image);
            $image = $this->standard->doUpload($request->file('file_image'),'_files/institucional_conceitos/',false,false);
            $item->image = $image;
        }

        foreach (['pt', 'en', 'es'] as $locale) {
            $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
            $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
        }

        $item->save();

        Session::flash('success', 'Item editado com sucesso!');

        return Redirect::to('/cms/empresa/institucional#tab_3');
    }

    public function destroyConcept($id)
    {
        $item = $this->concepts->find($id);

        if(!empty($item->image))
            File::delete('_files/institucional_conceitos/'.$item->image);

        $item->delete();

        Session::flash('success', 'Item excluído com sucesso!');
        return Redirect::to('/cms/empresa/institucional#tab_3');
    }
    // Reordenando utilizando Jquery-ui Sortable
    public function sortableConcept(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->concepts);
        return 2;
    }
}
