<?php

namespace App\Http\Controllers\Admin\Company;

use App\Entities\Company\SocialResponsability\SocialProjects;
use App\Entities\Company\SocialResponsability\Responsability;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class SocialResponsabilityController extends Controller
{

    /**
     * @var Responsability
     */
    protected $repository;

    /**
     * @var SocialProjects
     */
    protected $social;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Responsability $repository, SocialProjects $social, StandardService $standard)
    {
        $this->repository = $repository;
        $this->social = $social;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = $this->repository->first();
        $items_list = $this->social->orderBy('order','desc')->get();

        $items_list->map(function ($item_list) {
            if(!empty($item_list->getTranslation('pt')->title)) $item_list->title_pt = $item_list->getTranslation('pt')->title;
            else $item_list->title_pt = 'sem título';
        });
        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.company.responsability.index',compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request,$id=0)
    {
        try {
            if($id == 0){
                $item = new $this->repository;
                //image
                $image = $this->standard->doUpload($request->file('file_image'),'_files/responsabilidade_social/',false,false);
                $item->image = $image;

            } else {
                $item = $this->repository->find($id);
                if (!empty($request->file('file_image'))) {
                    File::delete('_files/responsabilidade_social/' . $item->image);
                    $image = $this->standard->doUpload($request->file('file_image'),'_files/responsabilidade_social/',false,false);
                    $item->image = $image;
                }
            }

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->title_projects = $request->input('title_projects_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
                $item->translateOrNew($locale)->text_projects = $request->input('text_projects_'.$locale);
                $item->translateOrNew($locale)->text_gallery = $request->input('text_gallery_'.$locale);
            }

            $item->save();

            Session::flash('success', 'Item salvo com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/empresa/responsabilidade_social');
    }


    public function getId($url) {
        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );

        if(!empty($my_array_of_vars['v'])) return $my_array_of_vars['v'];
        else return $url;
    }
    /**
     * PROJETOS
    */
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeProjects(Request $request)
    {
        try {

            $item = new $this->social;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            foreach ([1, 2, 3, 4] as $i) {
                $item['link_youtube_'.$i] = $this->getId($request->input('link_youtube_'.$i));
            }

            $name_image = $this->standard->doUpload($request->file('file_image'),'_files/projetos_sociais/',false,false);
            $item->image = $name_image;

            $ultimo_item = $item->orderBy('order','desc')->first();
            $item->order = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;
            
            $item->save();

            Session::flash('success', 'Projeto social criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/empresa/responsabilidade_social#tab_2');

    }
    public function editProjects($id=0)
    {
        $item = $this->social->find($id);
        return view('admin.company.responsability.edit_projects', compact('item'));
    }
    public function updateProjects(Request $request, $id)
    {
        $item = $this->social->find($id);

        foreach (['pt', 'en', 'es'] as $locale) {
            $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
            $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
        }

        foreach ([1, 2, 3, 4] as $i) {
            $item['link_youtube_'.$i] = $this->getId($request->input('link_youtube_'.$i));
            
        }

        if (!empty($request->file('file_image'))) {
            File::delete('_files/projetos_sociais/' . $item->image);
            $image = $this->standard->doUpload($request->file('file_image'),'_files/projetos_sociais/',false,false);
            $item->image = $image;
        }
        $item->save();

        Session::flash('success', 'Projeto editado com sucesso!');

        return Redirect::to('/cms/empresa/responsabilidade_social#tab_2');
    }

    public function destroyProjects($id)
    {
        $item = $this->social->find($id);

        if(!empty($item->image))
            File::delete('_files/projetos_sociais/'.$item->image);

        $item->delete();

        Session::flash('success', 'Projeto excluído com sucesso!');
        return Redirect::to('/cms/empresa/responsabilidade_social#tab_2');
    }
    // Reordenando utilizando Jquery-ui Sortable
    public function sortableProjects(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->social);
        return 2;
    }
}
