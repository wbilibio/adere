<?php

namespace App\Http\Controllers\Admin\Company;

use App\Entities\Company\Partners\PartnerGallery;
use App\Entities\Company\Partners\Partners;
use App\Entities\Company\Partners\Testimonials;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class PartnersController extends Controller
{
    /**
     * @var Partners
     */
    protected $repository;

    /**
     * @var PartnerGallery
     */
    protected $gallery;

    /**
     * @var Testimonials
     */
    protected $testimonials;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Partners $repository, PartnerGallery $gallery, Testimonials $testimonials, StandardService $standard)
    {
        $this->repository = $repository;
        $this->gallery = $gallery;
        $this->testimonials = $testimonials;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = $this->repository->first();
        $items_gallery = $this->gallery->orderBy('order','desc')->get();
        $items_list = $this->testimonials->orderBy('order','desc')->get();

        $items_gallery->map(function ($item_gallery) {
            if(!empty($item_gallery->getTranslation('pt')->title)) $item_gallery->title_pt = $item_gallery->getTranslation('pt')->title;
            else $item_gallery->title_pt = 'sem título';
        });

        $items_list->map(function ($item_list) {
            if(!empty($item_list->getTranslation('pt')->title)) $item_list->title_pt = $item_list->getTranslation('pt')->title;
            else $item_list->title_pt = 'sem título';
        });

        JavaScript::put([
            'items_list_data' => $items_gallery,
            'items_list_data_two' => $items_list
        ]);

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $item,
            ]);
        }

        return view('admin.company.partners.index', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO E ATUALIZANDO TEXTOS
    public function store(Request $request,$id=0)
    {
        try {
            if($id == 0){
                $item = new $this->repository;
            } else {
                $item = $this->repository->find($id);
            }

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
                $item->translateOrNew($locale)->text_gallery = $request->input('text_gallery_'.$locale);
                $item->translateOrNew($locale)->text_testimonials = $request->input('text_testimonials_'.$locale);
            }

            $item->save();

            Session::flash('success', 'Edição nos textos concluída!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/empresa/parcerias');
    }


    /**
    GALERIA DE VIDEOS
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeGallery(Request $request)
    {
        try {
            $item = new $this->gallery;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->profession = $request->input('profession_'.$locale);
            }

            $name_image = $this->standard->doUpload($request->file('file_image'),'_files/parcerias_galeria/',false,false);
            $item->image = $name_image;

            $item->link_youtube = $request->input('link_youtube');

            $ultimo_item = $this->gallery->orderBy('order','desc')->first();
            $item->order = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

            $item->save();

            Session::flash('success', 'Item salvo com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/empresa/parcerias#tab_2');

    }
    public function editGallery($id=0)
    {
        $item = $this->gallery->find($id);
        return view('admin.company.partners.edit_gallery', compact('item'));
    }
    public function updateGallery(Request $request, $id)
    {
        $item = $this->gallery->find($id);

        foreach (['pt', 'en', 'es'] as $locale) {
            $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
            $item->translateOrNew($locale)->profession = $request->input('profession_'.$locale);
        }

        if (!empty($request->file('file_image'))) {
            File::delete('_files/parcerias_galeria/' . $item->image);
            $name_image = $this->standard->doUpload($request->file('file_image'),'_files/parcerias_galeria/',false,false);
            $item->image = $name_image;
        }

        $item->link_youtube = $request->input('link_youtube');

        $item->save();

        Session::flash('success', 'Item atualizado com sucesso!');

        return Redirect::to('/cms/empresa/parcerias#tab_2');
    }

    public function destroyGallery($id)
    {
        $item = $this->gallery->find($id);

        $item->delete();

        Session::flash('success', 'Item excluído com sucesso!');
        return Redirect::to('/cms/empresa/parcerias#tab_2');
    }

    // Reordenação utilizando Jquery-ui Sortable
    public function sortableGallery(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->gallery);
        return 2;
    }


    /**
    DEPOIMENTOS
     */

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeTestimonials(Request $request)
    {
        try {
            $item = new $this->testimonials;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->company = $request->input('company_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            $name_image = $this->standard->doUpload($request->file('file_image'),'_files/parcerias_depoimentos/',false,false);
            $item->image = $name_image;

            $ultimo_item = $this->testimonials->orderBy('order','desc')->first();
            $item->order = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

            $item->save();

            Session::flash('success', 'Depoimento criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/empresa/parcerias#tab_3');

    }
    public function editTestimonials($id=0)
    {
        $item = $this->testimonials->find($id);
        return view('admin.company.partners.edit_testimonial', compact('item'));
    }
    public function updateTestimonials(Request $request, $id)
    {
        $item = $this->testimonials->find($id);

        foreach (['pt', 'en', 'es'] as $locale) {
            $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
            $item->translateOrNew($locale)->company = $request->input('company_'.$locale);
            $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
        }

        if (!empty($request->file('file_image'))) {
            File::delete('_files/parcerias_depoimentos/' . $item->image);
            $image = $this->standard->doUpload($request->file('file_image'),'_files/parcerias_depoimentos/',false,false);
            $item->image = $image;
        }

        $item->save();

        Session::flash('success', 'Depoimento editado com sucesso!');

        return Redirect::to('/cms/empresa/parcerias#tab_3');
    }

    public function destroyTestimonials($id)
    {
        $item = $this->testimonials->find($id);

        if(!empty($item->image))
            File::delete('_files/parcerias_depoimentos/'.$item->image);

        $item->delete();

        Session::flash('success', 'Depoimento excluído com sucesso!');
        return Redirect::to('/cms/empresa/parcerias#tab_3');
    }
    // Reordenando utilizando Jquery-ui Sortable
    public function sortableTestimonials(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->testimonials);
        return 2;
    }
}
