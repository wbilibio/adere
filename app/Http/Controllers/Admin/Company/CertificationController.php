<?php

namespace App\Http\Controllers\Admin\Company;

use App\Entities\Company\Certification\Certification;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class CertificationController extends Controller
{

    /**
     * @var Certification
     */
    protected $repository;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Certification $repository, StandardService $standard)
    {
        $this->repository = $repository;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items_list = $this->repository->orderBy('order','desc')->get();

        $items_list->map(function ($item_list) {
            if(!empty($item_list->getTranslation('pt')->title)) $item_list->title_pt = $item_list->getTranslation('pt')->title;
            else $item_list->title_pt = 'sem título';
        });

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.company.certification.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request)
    {
        try {
            $item = new $this->repository;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            $image = $this->standard->doUpload($request->file('file_image'),'_files/certificacoes/',228,false);
            $item->image = $image;

            $pdf_name = $this->standard->anyFile($request->file('file_pdf'),'_files/certificacoes/',$request->file('file_pdf')->getClientOriginalName());
            $item->pdf = $pdf_name;

            $ultimo_item = $this->repository->orderBy('order','desc')->first();
            $item->order = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

            $item->save();

            Session::flash('success', 'Certificado criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/empresa/certificacoes');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id=0)
    {
        try {
            $item = $this->repository->find($id);

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            if (!empty($request->file('file_image'))) {
                File::delete('_files/certificacoes/' . $item->image);
                $image_name = $this->standard->doUpload($request->file('file_image'),'_files/certificacoes/',228,false);
                $item->image = $image_name;
            }

            if (!empty($request->file('file_pdf'))) {
                File::delete('_files/certificacoes/' . $item->pdf);
                $pdf_name = $this->standard->anyFile($request->file('file_pdf'),'_files/certificacoes/',$request->file('file_pdf')->getClientOriginalName());
                $item->pdf = $pdf_name;
            }

            $item->save();

            Session::flash('success', 'Certificado atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/empresa/certificacoes');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $item = $this->repository->find($id);

        if(!empty($item->image)) File::delete('_files/certificacoes/'.$item->image);

        if(!empty($item->pdf)) File::delete('_files/certificacoes/'.$item->pdf);

        $item->delete();

        Session::flash('success', 'Item excluído com sucesso!');
        return Redirect::to('/cms/empresa/certificacoes');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id=0)
    {
        $item = $this->repository->find($id);
        return view('admin.company.certification.edit', compact('item'));
    }
}
