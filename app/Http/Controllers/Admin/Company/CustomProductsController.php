<?php

namespace App\Http\Controllers\Admin\Company;

use App\Entities\Company\CustomProducts\CustomProducts;
use App\Entities\Company\CustomProducts\CustomProductsGallery;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class CustomProductsController extends Controller
{
    /**
     * @var CustomProducts
     */
    protected $repository;

    /**
     * @var CustomProductsGallery
     */
    protected $gallery;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(CustomProducts $repository, CustomProductsGallery $gallery, StandardService $standard)
    {
        $this->repository = $repository;
        $this->gallery = $gallery;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = $this->repository->first();
        $items_gallery = $this->gallery->orderBy('order','desc')->get();
        
        JavaScript::put([
            'items_gallery_data' => $items_gallery
        ]);
        if (request()->wantsJson()) {
            return response()->json([
                'data' => $item,
            ]);
        }

        return view('admin.company.custom_products.index', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO E ATUALIZANDO TEXTOS
    public function store(Request $request,$id=0)
    {
        try {
            if($id == 0){
                $item = new $this->repository;
            } else {
                $item = $this->repository->find($id);
            }

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
                $item->translateOrNew($locale)->text_solution = $request->input('text_solution_'.$locale);
            }

            $item->save();

            Session::flash('success', 'Edição nos textos concluída!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/empresa/produtos_sob_medida');
    }

    /**
     GALERIA DE IMAGEM
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO IMAGENS NA GALERIA INSTITUCIONAL
    public function storeGallery(Request $request)
    {
        $image = $this->standard->doUpload($request->file('file'),'_files/produtos_sob_medida/',false,false);
        $request['image'] = $image;

        $ultimo_item = $this->gallery->orderBy('order','desc')->first();
        $request['order'] = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

        $this->gallery->create($request->all());

        return Response::json(['success' => true, 'files' => asset('_files/produtos_sob_medida/'.$image)]);
    }
    // Reordenando utilizando Jquery-ui Sortable
    public function sortableGallery(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->gallery);
        return 1;
    }
    public function getItemsGallery()
    {
        $items_gallery = $this->gallery->orderBy('order','desc')->get();

        return $items_gallery;
    }
    public function destroyItemGallery($id)
    {
        $item = $this->gallery->find($id);

        if(!empty($item->image))
            File::delete('_files/produtos_sob_medida/'.$item->image);

        if($this->gallery->destroy($id)){
            return 1;
        } else {
            return 2;
        }
    }
}
