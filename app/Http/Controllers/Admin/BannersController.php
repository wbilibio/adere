<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Banners\Banners;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class BannersController extends Controller
{

    /**
     * @var Banners
     */
    protected $repository;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Banners $repository, StandardService $standard)
    {
        $this->repository = $repository;
        $this->standard = $standard;
    }

    static function changeType($value)
    {
        if($value == 'home-left') return 'Home - lado esquerdo';
        if($value == 'home-right') return 'Home - lado direito';
        if($value == 'about') return 'Sobre a empresa - lateral';
        if($value == 'solution-left') return 'Solução Subcategoria - esquerdo';
        if($value == 'solution-center') return 'Solução Subcategoria - centro';
        if($value == 'product-left') return 'Produto - esquerdo';
        if($value == 'product-center') return 'Produto - centro';
        if($value == 'menu-solution') return 'Menu solução';
        if($value == 'menu-market') return 'Menu mercado';
        if($value == 'market-left') return 'Mercado - lateral';
        if($value == 'market-center') return 'Mercado - centro';
        if($value == 'blog') return 'Blog';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items_list = $this->repository->all();

        $items_list->map(function ($item_list) {
            if(!empty($item_list->type)) $item_list->title_pt = $this->changeType($item_list->type);
            else $item_list->title_pt = 'sem título';
        });

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.banners.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request)
    {
        try {
            $item = new $this->repository;

            $image = $this->standard->doUpload($request->file('file_image'),'_files/banners/',false,false);
            $item->image = $image;

            $item->link = $request->input('link');

            $item->type = $request->input('type');

            $item->save();

            Session::flash('success', 'Item criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/banners');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id=0)
    {
        $item = $this->repository->find($id);
        return view('admin.banners.edit', compact('item'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id=0)
    {
        try {
            $item = $this->repository->find($id);

            if (!empty($request->file('file_image'))) {
                File::delete('_files/banners/' . $item->image);
                $image_name = $this->standard->doUpload($request->file('file_image'),'_files/banners/',false,false);
                $item->image = $image_name;
            }

            $item->link =  $request->input('link');

            $item->type =  $request->input('type');

            $item->save();

            Session::flash('success', 'Item atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/banners');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $item = $this->repository->find($id);

        if(!empty($item->image)) File::delete('_files/banners/'.$item->image);

        $item->delete();

        Session::flash('success', 'Item excluído com sucesso!');
        return Redirect::to('/cms/banners');
    }


}
