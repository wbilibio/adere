<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Sliders\Sliders;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class SlidersController extends Controller
{

    /**
     * @var Sliders
     */
    protected $repository;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Sliders $repository, StandardService $standard)
    {
        $this->repository = $repository;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items_list = $this->repository->orderBy('order','desc')->get();

        $items_list->map(function ($item_list) {
            if(!empty($item_list->getTranslation('pt')->title)) $item_list->title_pt = $item_list->getTranslation('pt')->title;
            else $item_list->title_pt = 'sem título';
        });

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.sliders.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request)
    {
        try {
            $item = new $this->repository;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            $image = $this->standard->doUpload($request->file('file_image'),'_files/sliders/',false,false);
            $item->image = $image;

            $image_background = $this->standard->doUpload($request->file('file_image_background'),'_files/sliders/',false,false);
            $item->image_background = $image_background;

            $ultimo_item = $this->repository->orderBy('order','desc')->first();
            $item->order = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

            $item->link = $request->input('link');

            $item->save();

            Session::flash('success', 'Item criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/sliders');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id=0)
    {
        $item = $this->repository->find($id);
        return view('admin.sliders.edit', compact('item'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id=0)
    {
        try {
            $item = $this->repository->find($id);

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            if (!empty($request->file('file_image'))) {
                File::delete('_files/sliders/' . $item->image);
                $image_name = $this->standard->doUpload($request->file('file_image'),'_files/sliders/',false,false);
                $item->image = $image_name;
            }

            if (!empty($request->file('file_image_background'))) {
                File::delete('_files/sliders/' . $item->image_background);
                $image_background = $this->standard->doUpload($request->file('file_image_background'),'_files/sliders/',false,false);
                $item->image_background = $image_background;
            }

            $item->link =  $request->input('link');
            $item->save();

            Session::flash('success', 'Slider atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/sliders');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $item = $this->repository->find($id);

        if(!empty($item->image)) File::delete('_files/sliders/'.$item->image);
        if(!empty($item->image_background)) File::delete('_files/sliders/'.$item->image_background);

        $item->delete();

        Session::flash('success', 'Item excluído com sucesso!');
        return Redirect::to('/cms/sliders');
    }


}
