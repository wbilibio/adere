<?php

namespace App\Http\Controllers\Admin;


use App\Entities\Configs\Configs;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class ConfigsController extends Controller
{
    /**
     * @var Configs
     */
    protected $repository;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Configs $repository, StandardService $standard)
    {
        $this->repository = $repository;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = $this->repository->first();

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $item,
            ]);
        }

        return view('admin.configs.index', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO E ATUALIZANDO TEXTOS
    public function store(Request $request,$id=0)
    {
        try {
            if($id == 0){
                $item = new $this->repository;
            } else {
                $item = $this->repository->find($id);
            }

            $item->phone_world = $request->input('phone_world');
            $item->google_analytics = $request->input('google_analytics');
            $item->link_facebook = $request->input('link_facebook');
            $item->link_twitter = $request->input('link_twitter');
            $item->link_linkedin = $request->input('link_linkedin');
            $item->link_google_plus = $request->input('link_google_plus');
            $item->link_youtube = $request->input('link_youtube');
            $item->phone_sac = $request->input('phone_sac');
            $item->office_hours = $request->input('office_hours');
            $item->phone_pabx = $request->input('phone_pabx');
            $item->phone_fax = $request->input('phone_fax');
            $item->address = $request->input('address');
            $item->link_store = $request->input('link_store');
            $item->link_2_via_boletos = $request->input('link_2_via_boletos');

            $item->save();

            Session::flash('success', 'Edição das configurações concluída!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/configuracoes');
    }

}
