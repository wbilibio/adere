<?php

namespace App\Http\Controllers\Admin;


use App\Entities\Texts\Texts;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class TextsController extends Controller
{
    /**
     * @var Texts
     */
    protected $repository;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Texts $repository, StandardService $standard)
    {
        $this->repository = $repository;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = $this->repository->first();

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $item,
            ]);
        }

        return view('admin.texts.index', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO E ATUALIZANDO TEXTOS
    public function store(Request $request,$id=0)
    {
        try {
            if($id == 0){
                $item = new $this->repository;
            } else {
                $item = $this->repository->find($id);
            }

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->text_market_of_action = $request->input('text_market_of_action_'.$locale);
                $item->translateOrNew($locale)->text_our_brands = $request->input('text_our_brands_'.$locale);
                $item->translateOrNew($locale)->text_most_wanted_products = $request->input('text_most_wanted_products_'.$locale);
                $item->translateOrNew($locale)->text_custom_product = $request->input('text_custom_product_'.$locale);
                $item->translateOrNew($locale)->text_company_in_the_world = $request->input('text_company_in_the_world_'.$locale);
                $item->translateOrNew($locale)->text_latest_posts = $request->input('text_latest_posts_'.$locale);
                $item->translateOrNew($locale)->text_ombudsman = $request->input('text_ombudsman_'.$locale);
            }

            $item->save();

            Session::flash('success', 'Edição nos textos concluída!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/textos');
    }

}
