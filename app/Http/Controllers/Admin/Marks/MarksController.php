<?php

namespace App\Http\Controllers\Admin\Marks;

use App\Entities\Marks\Marks;
use App\Entities\Products\Product;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use JavaScript;
use Response;
use Redirect;
use Session;

class MarksController extends Controller
{

    /**
     * @var Marks
     */
    protected $repository;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(Marks $repository, Product $product, StandardService $standard)
    {
        $this->repository = $repository;
        $this->product = $product;
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items_list = $this->repository->orderBy('order','desc')->get();

        $items_list->map(function ($item_list) {
            if(!empty($item_list->title)) $item_list->title_pt = $item_list->title;
            else $item_list->title_pt = 'sem título';
        });

        JavaScript::put([
            'items_list_data' => $items_list
        ]);

        return view('admin.marks.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request)
    {
        try {
            $item = new $this->repository;

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            $name_image = $this->standard->doUpload($request->file('file_image'),'_files/marcas/',false,false);
            $item->image = $name_image;

            $ultimo_item = $this->repository->orderBy('order','desc')->first();
            $item->order = !empty($ultimo_item) ? $ultimo_item->order+1 : 1;

            $item->status = $request->input('status');

            $item->save();

            Session::flash('success', 'Marca criada com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/marcas/lista');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id=0)
    {
        $item = $this->repository->find($id);
        JavaScript::put([
            'status_data' => $item->status
        ]);
        return view('admin.marks.edit', compact('item'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id=0)
    {
        try {
            $item = $this->repository->find($id);

            foreach (['pt', 'en', 'es'] as $locale) {
                $item->translateOrNew($locale)->title = $request->input('title_'.$locale);
                $item->translateOrNew($locale)->text = $request->input('text_'.$locale);
            }

            if (!empty($request->file('file_image'))) {
                File::delete('_files/marcas/' . $item->image);
                $name_image = $this->standard->doUpload($request->file('file_image'), '_files/marcas/', false, false);
                $item->image = $name_image;
            }
            $item->status = $request->input('status');
            $item->save();

            Session::flash('success', 'Marca atualizada com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/cms/marcas/lista');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request)
    {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        $item = $this->repository->find($id);
        if(!empty($item->image)) File::delete('_files/marcas/'.$item->image);
        $item->delete();

        Session::flash('success', 'Marca excluída com sucesso!');
        return Redirect::to('/cms/marcas/lista');
    }


}
