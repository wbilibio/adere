<?php

namespace App\Http\Controllers;

use App\Entities\Configs\UsersDownloadFile;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;

use App\Http\Requests;

use JavaScript;
use Response;
use Redirect;
use Session;

class FormsController extends Controller
{
    /**
     * @var UsersDownloadFile
     */
    protected $usersDownloadFile;

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(UsersDownloadFile $usersDownloadFile, StandardService $standard)
    {
        $this->usersDownloadFile = $usersDownloadFile;
        $this->standard = $standard;
    }

    /**
     * saveUserDownloadFile a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function saveUserDownloadFile(Request $request, $locale){
            if(UsersDownloadFile::create($request->all())) {
                return 1;
            } else {
                return 2;
            }
    }

}
