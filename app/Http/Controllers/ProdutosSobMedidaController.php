<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Company\CustomProducts\CustomProducts;
use App\Entities\Company\CustomProducts\CustomProductsGallery;
use Illuminate\Http\Request;
use Mail;
use Session;
use Redirect;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class ProdutosSobMedidaController extends Controller
{
    public function index($locale)
    {
        app()->setLocale($locale);

        $item_custom = CustomProducts::first();
        $items_gallery = CustomProductsGallery::all();
        return view('front.produtos_sob_medida',compact('item_custom','items_gallery'));
    }

    public function messages()
    {
        return [
            'name.required' => 'Campo obrigatório',
            'name.alpha' => 'Nome não pode ter números',
            'email.required'  => 'Campo obrigatório',
            'phone.required' => 'Campo obrigatório',
            'subject.required' => 'Campo obrigatório',
            'text.required' => 'Campo obrigatório',
            'email.email' => 'Formato de e-mail inválido'
        ];
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function sendForm(Request $request,$locale) {
        $this->validate($request, [
            'name' => 'required|alpha|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:255',
            'subject' => 'required|max:255',
            'text' => 'required|max:255',
        ], $this->messages());

        $data['request'] = $request;
        Mail::send('emails.custom_products', $data, function($m){
            $m->to('contato@adere.com.br','Adere')->subject('Produtos Sob Medida');
        });

        // check for failures
        if (count(Mail::failures()) > 0) {
            Session::flash('error', 'Falha ao enviar a mensagem, tente novamente mais tarde!');
            return Redirect::to($locale.'/produtos_sob_medida/');
        } else {
            Session::flash('success', 'Sua mensagem foi enviada com sucesso!');
            return Redirect::to($locale.'/produtos_sob_medida/');
        }
    }
}