<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Company\About\AboutConcept;
use App\Entities\Company\About\Institutional;
use App\Entities\Company\About\InstitutionalGallery;
use App\Http\Requests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class InstitucionalController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index($locale)
    {
        app()->setLocale($locale);

        $item_institucional = Institutional::first();
        $items_gallery = InstitutionalGallery::all();
        $items_concepts = AboutConcept::all();

        return view('front.institucional',compact('item_institucional','items_gallery','items_concepts'));
    }
}