<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Banners\Banners;
use App\Entities\Catalog\Catalog;
use App\Http\Requests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class CatalogoController extends Controller
{
    public function index($locale)
    {
        app()->setLocale($locale);

        $items_catalog = Catalog::orderBy('order','desc')->get();
        $banner_center = Banners::where('type', 'solution-center')->first();

        return view('front.catalogos',compact('items_catalog','banner_center'));
    }
}