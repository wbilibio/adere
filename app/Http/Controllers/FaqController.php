<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Faq\Faq;
use App\Entities\Markets\Market;
use App\Entities\Products\ProductType;
use App\Http\Requests;

use JavaScript;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class FaqController extends Controller
{
    public function index($locale)
    {
        app()->setLocale($locale);

        $items_faq = Faq::orderBy('order','desc')->where('status',1)->get();
        $items_product_types = ProductType::orderBy('order','desc')->where('status',1)->get();
        $items_markets = Market::orderBy('order','desc')->where('status',1)->get();

        JavaScript::put([
            'items_faq_data' => $items_faq
        ]);

        return view('front.faq',compact('items_product_types','items_markets'));
    }
    public function get_list_market($locale,$market_id)
    {
        $items_markets = Market::find($market_id)->faqs()->orderBy('order','desc')->get();
        return $items_markets;
    }
    public function get_list_type($locale,$type_id)
    {
        $items_types = ProductType::find($type_id)->faqs()->orderBy('order','desc')->get();
        return $items_types;
    }
}