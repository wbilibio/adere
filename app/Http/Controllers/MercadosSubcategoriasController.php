<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Banners\Banners;
use App\Entities\Markets\Market;
use App\Entities\Products\ProductFamily;
use App\Http\Requests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class MercadosSubcategoriasController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index($locale,$category_slug,$subcategory_slug)
    {
        app()->setLocale($locale);

        $item_market = Market::where('slug',$category_slug)->first();
        $get_item = ProductFamily::where('slug',$subcategory_slug)->first();

        $banner_lateral = Banners::where('type', 'market-left')->first();
        $banner_center = Banners::where('type', 'market-center')->first();

        return view('front.mercado_subcategoria', compact('item_market','get_item','banner_lateral','banner_center'));
    }
}