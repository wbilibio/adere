<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Blog\PostCommentAnswers;
use App\Entities\Blog\PostComments;
use App\Entities\Blog\Posts;
use App\Entities\Products\ProductFamily;
use App\Http\Requests;
use Illuminate\Http\Request;

use Validator;
use Mail;
use Session;
use Redirect;
use JavaScript;
/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class BlogPostController extends Controller
{
    public function index($locale,$slug)
    {
        app()->setLocale($locale);

        $item_post = Posts::where('slug',$slug)->first();
        $item_post->increment('viewed');

        $items_posts = Posts::all();

        $items_post_comments = Posts::find($item_post->id)->comments()->where('status',1)->orderBy('created_at', 'desc')->get();

        $items_posts_most_viewed = Posts::orderBy('viewed', 'desc')->get()->take(3);

        $items_families = ProductFamily::where('status',1)->get();
        $items_families->map(function($item_family){
            if(!empty($item_family->posts) && count($item_family->posts) > 0) $item_family->visible = true;
            else $item_family->visible = false;
        });
        
        JavaScript::put([
            'posts_data' => $items_posts
        ]);

        return view('front.post', compact('items_post_comments','items_families','item_post','items_posts_most_viewed','items_posts'));
    }

    public function messages()
    {
        return [
            'title.required' => 'Campo obrigatório',
            'title.alpha' => 'Nome não pode ter números',
            'email.required'  => 'Campo obrigatório',
            'email.email' => 'Formato de e-mail inválido',
            'text.required' => 'Campo obrigatório',

        ];
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function sendComment(Request $request,$locale,$slug) {
        $validator = Validator::make($request->all(), [
            'title' => 'required|alpha|max:255',
            'email' => 'required|email|max:255',
            'text' => 'required'
        ], $this->messages());

        if ($validator->fails()) {
            return redirect($locale.'/post/'.$slug.'#comentarios')
                ->withErrors($validator)
                ->withInput();
        }

        $item = new PostComments();

        if ($item->create($request->all())) {
            Session::flash('success', 'Comentário enviado com sucesso! Aguardando aprovação.');
            return Redirect::to($locale.'/post/'.$slug.'#comentarios');

        } else {
            Session::flash('error', 'Falha ao enviar seu comentário, tente novamente mais tarde!');
            return Redirect::to($locale.'/post/'.$slug.'#comentarios');
        }

    }
    /**
     * @param Request $request
     * @return mixed
     */
    public function sendAnswer(Request $request,$locale,$slug) {
        $item = new PostCommentAnswers();

        if ($item->create($request->all())) {
            return 1;
        } else {
            return 0;
        }

    }
}