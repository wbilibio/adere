<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Company\SocialResponsability\Responsability;
use App\Entities\Company\SocialResponsability\SocialProjects;
use App\Http\Requests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class ResponsabilidadeSocialController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index($locale)
    {
        app()->setLocale($locale);

        $item_responsability = Responsability::first();
        $items_projects = SocialProjects::all();

        return view('front.responsabilidade_social',compact('item_responsability','items_projects'));
    }

    public function projects($locale,$slug)
    {
        app()->setLocale($locale);
        $item_responsability = Responsability::first();
        $items_projects = SocialProjects::all();
        $item_project = SocialProjects::where('slug',$slug)->first();

        return view('front.projeto_social',compact('item_responsability','items_projects','item_project'));
    }
}