<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Newsletter\Newsletter;
use Illuminate\Http\Request;

use App\Http\Requests;

use Response;
use Redirect;
use Session;
/**
 * Class NewsletterController
 * @package App\Http\Controllers
 */
class NewsletterController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function save(Request $request, $locale){
        $email_existente = Newsletter::where('email', $request['email'])->get()->first();
        if(empty($email_existente)){
            $item = new Newsletter;
            $item->name = $request['name'];
            $item->email = $request['email'];

            if($item->save()) {
                return 1;
            } else {
                return 2;
            }
        } else {
            return 3;
        };
    }
}