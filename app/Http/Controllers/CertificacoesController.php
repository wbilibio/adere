<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Company\Certification\Certification;
use App\Http\Requests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class CertificacoesController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index($locale)
    {
        app()->setLocale($locale);

        $items_certification = Certification::all();

        return view('front.certificacoes',compact('items_certification'));
    }
}