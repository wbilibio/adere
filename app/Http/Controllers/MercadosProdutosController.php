<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Banners\Banners;
use App\Entities\Markets\Market;
use App\Entities\Products\Product;
use App\Entities\Products\ProductFamily;
use App\Http\Requests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class MercadosProdutosController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index($locale,$category_slug,$subcategory_slug,$product_slug)
    {
        app()->setLocale($locale);

        // Mercados
        $market_new = new Market();

        $item_market = Market::where('slug',$category_slug)->first();
        $item_market_subcategory = ProductFamily::where('slug',$subcategory_slug)->first();
        $get_item = Product::where('slug',$product_slug)->where('status',1)->first();
        $get_item->increment('viewed');

        $banner_lateral = Banners::where('type', 'product-left')->first();
        $banner_center = Banners::where('type', 'product-center')->first();

        $items_posts = $get_item->family->posts->take(3);

        return view('front.mercado_produto', compact('item_market','item_market_subcategory','get_item','market_new','banner_lateral','banner_center','items_posts'));
    }
}