<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Company\Partners\PartnerGallery;
use App\Entities\Company\Partners\Partners;
use App\Entities\Company\Partners\Testimonials;
use App\Http\Requests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class ParceriaController extends Controller
{
    public function index($locale)
    {
        app()->setLocale($locale);

        $item_partner = Partners::first();
        $items_gallery = PartnerGallery::all();
        $items_testimonials = Testimonials::all();
        return view('front.parcerias',compact('item_partner','items_gallery','items_testimonials'));
    }
}