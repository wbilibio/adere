<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Banners\Banners;
use App\Entities\Products\ProductFamily;
use App\Http\Requests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class ProdutosSubcategoriasController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index($locale,$category_slug,$subcategory_slug)
    {
        app()->setLocale($locale);

        $get_item = ProductFamily::where('slug',$subcategory_slug)->first();

        $banner_lateral = Banners::where('type', 'solution-left')->first();
        $banner_center = Banners::where('type', 'solution-center')->first();

        return view('front.solucoes_subcategorias', compact('get_item','banner_lateral','banner_center'));
    }
}