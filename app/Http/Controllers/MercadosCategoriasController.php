<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Entities\Banners\Banners;
use App\Entities\Markets\Market;
use App\Entities\Products\ProductFamily;
use App\Http\Requests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class MercadosCategoriasController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index($locale,$category_slug)
    {
        app()->setLocale($locale);

        $get_item = Market::where('slug',$category_slug)->first();

        $banner_lateral = Banners::where('type', 'market-left')->first();
        $banner_center = Banners::where('type', 'market-center')->first();

        $get_families = ProductFamily::getByMarket($get_item->id);
        if(!empty($get_families->first())){
            $items_posts = $get_families->first()->posts()->take(3)->get();
        }

        $get_item->local_file = '_files/mercados_arquivos/';

        return view('front.mercado_categoria', compact('get_item','banner_lateral','banner_center','items_posts'));
    }
}