<?php

namespace App\Entities\Blog;

use App\Entities\Products\ProductFamily;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Posts extends Model
{
    use \Dimsav\Translatable\Translatable;
    use Sluggable;

    protected $fillable = ['viewed', 'image'];
    public $translatedAttributes = ['title','text'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function families(): BelongsToMany
    {
        return $this->BelongsToMany(ProductFamily::class);
    }

    public function tags(): HasMany
    {
        return $this->hasMany(PostTags::class, 'post_id');
    }
    
    public function comments(): HasMany
    {
        return $this->hasMany(PostComments::class, 'post_id');
    }
}