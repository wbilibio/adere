<?php

namespace App\Entities\Blog;

use Illuminate\Database\Eloquent\Model;

class PostsCategoriesTranslation extends Model
{
    public $timestamps = false;
}

