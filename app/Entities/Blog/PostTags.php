<?php

namespace App\Entities\Blog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PostTags extends Model
{

    protected $fillable = ['title'];

    public function post(): BelongsTo
    {
        return $this->belongsTo(Posts::class);
    }

}