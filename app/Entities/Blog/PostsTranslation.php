<?php

namespace App\Entities\Blog;

use Illuminate\Database\Eloquent\Model;

class PostsTranslation extends Model
{
    public $timestamps = false;
}

