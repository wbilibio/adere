<?php

namespace App\Entities\Blog;

use Illuminate\Database\Eloquent\Model;

class PostsProductFamilies extends Model
{

    protected $fillable = ['posts_id','product_families_id'];

    public $timestamps = false;

}