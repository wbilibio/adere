<?php

namespace App\Entities\Blog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PostComments extends Model
{
    protected $fillable = ['post_id','email','title','text','status'];

    public function post(): BelongsTo
    {
        return $this->belongsTo(Posts::class);
    }

    public function answers(): HasMany
    {
        return $this->hasMany(PostCommentAnswers::class);
    }
}