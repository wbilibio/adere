<?php

namespace App\Entities\Blog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PostCommentAnswers extends Model
{
    protected $fillable = ['post_comments_id','email','title','text','status'];

    public function comment(): BelongsTo
    {
        return $this->belongsTo(PostComments::class,'post_comments_id');
    }
}