<?php

namespace App\Entities\Blog;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PostsCategories extends Model
{
    use \Dimsav\Translatable\Translatable;
    use Sluggable;

    protected $fillable = ['order'];
    public $translatedAttributes = ['title'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function posts(): HasMany
    {
        return $this->hasMany(Posts::class);
    }
}