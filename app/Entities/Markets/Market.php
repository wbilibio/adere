<?php

namespace App\Entities\Markets;

use App\Entities\HasTranslationTrait;
use App\Entities\Products\Product;
use App\Entities\Faq\Faq;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Market extends Model
{
    use Sluggable, HasTranslationTrait;

    protected $fillable = ['pt_title', 'en_title', 'es_title','pt_description', 'en_description', 'es_description','icon','status'];
    protected $casts = [ 'metadata' => 'object' ];
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'pt_title'
            ]
        ];
    }

    public function files(): BelongsToMany
    {
        return $this->belongsToMany(MarketsFiles::class, 'markets_related_files', 'market_id','file_id');
    }

    public function galleries(): HasMany
    {
        return $this->hasMany(MarketsGallery::class);
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class);
    }

    public function faqs(): BelongsToMany
    {
        return $this->belongsToMany(Faq::class, 'faq_markets', 'markets_id','faq_id');
    }
}