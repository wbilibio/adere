<?php

namespace App\Entities\Markets;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MarketsGallery extends Model
{
    protected $table = "markets_gallery";
    protected $fillable = ['market_id','image', 'order'];

    public function market(): BelongsTo
    {
        return $this->belongsTo(Market::class);
    }
}