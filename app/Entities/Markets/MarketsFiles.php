<?php

namespace App\Entities\Markets;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class MarketsFiles extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $fillable = ['file','status','guia'];
    public $translatedAttributes = ['title'];

    public function markets(): BelongsToMany
    {
        return $this->belongsToMany(Market::class);
    }
}