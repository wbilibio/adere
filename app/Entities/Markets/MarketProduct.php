<?php

namespace App\Entities\Markets;

use Illuminate\Database\Eloquent\Model;

class MarketProduct extends Model
{
    protected $table = "market_product";

    protected $fillable = ['market_id','product_id'];
    public $timestamps = false;
}