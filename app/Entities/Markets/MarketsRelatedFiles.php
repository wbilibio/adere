<?php

namespace App\Entities\Markets;

use Illuminate\Database\Eloquent\Model;

class MarketsRelatedFiles extends Model
{

    protected $fillable = ['market_id','file_id'];

    public $timestamps = false;
}