<?php

namespace App\Entities\Markets;

use Illuminate\Database\Eloquent\Model;

class MarketsFilesTranslation extends Model
{
    public $timestamps = false;
}

