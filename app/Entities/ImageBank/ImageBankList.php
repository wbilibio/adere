<?php

namespace App\Entities\ImageBank;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ImageBankList extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = 'image_bank_list';
    protected $fillable = ['image','image_thumb','image_bank_id','status','order'];
    public $translatedAttributes = ['title'];

    public function category(): BelongsTo
    {
        return $this->belongsTo(ImageBank::class);
    }

}