<?php

namespace App\Entities\ImageBank;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ImageBank extends Model
{
    use Sluggable, \Dimsav\Translatable\Translatable;

    protected $table = 'image_bank';
    protected $fillable = ['image','status','order'];
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public $translatedAttributes = ['title'];

    public function images(): HasMany
    {
        return $this->HasMany(ImageBankList::class);
    }

}