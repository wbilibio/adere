<?php

namespace App\Entities\ImageBank;

use Illuminate\Database\Eloquent\Model;

class ImageBankListTranslation extends Model
{
    public $timestamps = false;
}

