<?php

namespace App\Entities\ImageBank;

use Illuminate\Database\Eloquent\Model;

class ImageBankTranslation extends Model
{
    public $timestamps = false;
}

