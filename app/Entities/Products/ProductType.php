<?php

namespace App\Entities\Products;

use App\Entities\HasTranslationTrait;
use App\Entities\Faq\Faq;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ProductType extends Model
{
    use Sluggable, HasTranslationTrait;

    protected $fillable = ['pt_title', 'en_title', 'es_title','status'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'pt_title'
            ]
        ];
    }

    public function families(): HasMany
    {
        return $this->hasMany(ProductFamily::class);
    }

    public function faqs(): BelongsToMany
    {
        return $this->belongsToMany(Faq::class, 'faq_product_types', 'product_types_id','faq_id');
    }
}