<?php

namespace App\Entities\Products;

use App\Entities\HasTranslationTrait;
use App\Entities\Markets\Market;
use App\Entities\Marks\Marks;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Product extends Model
{
    use Sluggable, HasTranslationTrait;
    protected $table = 'products';
    protected $fillable = ['pt_title', 'pt_description', 'pt_application', 'en_title', 'en_description', 'en_application', 'es_title', 'es_description', 'es_application', 'product_family_id','url_loja_virtual', 'viewed', 'status','mark_id'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'pt_title'
            ]
        ];
    }
    public function markets(): BelongsToMany
    {
        return $this->belongsToMany(Market::class);
    }
    public function family(): BelongsTo
    {
        return $this->belongsTo(ProductFamily::class, 'product_family_id');
    }
    public function mark(): belongsTo
    {
        return $this->belongsTo(Marks::class);
    }
    public function galleries(): HasMany
    {
        return $this->hasMany(ProductGallery::class);
    }

    public function videos(): HasMany
    {
        return $this->hasMany(ProductsVideos::class);
    }

    public function files(): BelongsToMany
    {
        return $this->belongsToMany(ProductsFiles::class, 'products_related_files', 'product_id','file_id');
    }

    public function applications(): BelongsToMany
    {
        return $this->belongsToMany(Applications::class, 'products_related_applications', 'product_id','application_id');
    }
}