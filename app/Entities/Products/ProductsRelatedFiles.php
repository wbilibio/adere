<?php

namespace App\Entities\Products;

use Illuminate\Database\Eloquent\Model;

class ProductsRelatedFiles extends Model
{

    protected $fillable = ['product_id','file_id'];

    public $timestamps = false;
}