<?php

namespace App\Entities\Products;

use Illuminate\Database\Eloquent\Model;

class ProductsFilesTranslation extends Model
{
    public $timestamps = false;
}

