<?php

namespace App\Entities\Products;

use Illuminate\Database\Eloquent\Model;

class ProductsRelatedApplications extends Model
{
    protected $fillable = ['product_id','application_id'];

    public $timestamps = false;
}