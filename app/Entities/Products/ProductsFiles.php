<?php

namespace App\Entities\Products;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ProductsFiles extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $fillable = ['file','status'];
    public $translatedAttributes = ['title'];

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class);
    }
}