<?php

namespace App\Entities\Products;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductFamilyGallery extends Model
{
    protected $table = "family_gallery";
    protected $fillable = ['family_id','image', 'order'];

    public function family(): BelongsTo
    {
        return $this->belongsTo(ProductFamily::class);
    }
}