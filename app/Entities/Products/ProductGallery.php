<?php

namespace App\Entities\Products;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductGallery extends Model
{
    protected $table = "product_gallery";
    protected $fillable = ['product_id','image', 'order'];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}