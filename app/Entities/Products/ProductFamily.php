<?php

namespace App\Entities\Products;

use App\Entities\Blog\Posts;
use App\Entities\HasTranslationTrait;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ProductFamily extends Model
{
    use Sluggable, HasTranslationTrait;

    protected $fillable = ['pt_title', 'en_title', 'es_title','product_type_id','pt_description', 'en_description', 'es_description','status'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'pt_title'
            ]
        ];
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(ProductType::class,'product_type_id');
    }

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
    public function galleries(): HasMany
    {
        return $this->hasMany(ProductFamilyGallery::class,'family_id');
    }

    public function posts(): BelongsToMany
    {
        return $this->belongsToMany(Posts::class, 'posts_product_families', 'product_families_id','posts_id');
    }

    public static function getByMarket($market_id)
    {
        return self::join('products', 'product_families.id', '=', 'products.product_family_id')
            ->join('market_product', 'products.id', '=', 'market_product.product_id')
            ->select('product_families.*')
            ->where('market_product.market_id', $market_id)
            ->groupBy('product_families.id')
            ->get();
    }
}