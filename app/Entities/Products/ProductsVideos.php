<?php

namespace App\Entities\Products;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductsVideos extends Model
{
    protected $fillable = ['product_id','image','link_youtube','order'];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}