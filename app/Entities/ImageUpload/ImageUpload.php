<?php

namespace App\Entities\ImageUpload;

use Illuminate\Database\Eloquent\Model;

class ImageUpload extends Model
{
    protected $table = 'image_upload';
    protected $fillable = ['image'];

}