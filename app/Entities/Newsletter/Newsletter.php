<?php

namespace App\Entities\Newsletter;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $table = 'newsletter';
    protected $fillable = ['name','email'];

}