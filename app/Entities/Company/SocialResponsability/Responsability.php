<?php

namespace App\Entities\Company\SocialResponsability;

use Illuminate\Database\Eloquent\Model;

class Responsability extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = 'responsability';
    protected $fillable = ['image'];
    public $translatedAttributes = ['title', 'text', 'title_projects', 'text_projects', 'text_gallery'];

}