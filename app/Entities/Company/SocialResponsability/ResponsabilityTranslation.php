<?php

namespace App\Entities\Company\SocialResponsability;

use Illuminate\Database\Eloquent\Model;

class ResponsabilityTranslation extends Model
{
    public $timestamps = false;
}

