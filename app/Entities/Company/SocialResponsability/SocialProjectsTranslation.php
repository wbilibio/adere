<?php

namespace App\Entities\Company\SocialResponsability;

use Illuminate\Database\Eloquent\Model;

class SocialProjectsTranslation extends Model
{
    public $timestamps = false;
}

