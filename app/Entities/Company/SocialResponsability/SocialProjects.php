<?php

namespace App\Entities\Company\SocialResponsability;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class SocialProjects extends Model
{
    use Sluggable;
    use \Dimsav\Translatable\Translatable;

    protected $table = 'social_projects';
    protected $fillable = [
        'image',
        'link_youtube_1',
        'link_youtube_2',
        'link_youtube_3',
        'link_youtube_4',
        'order'
    ];
    public $translatedAttributes = ['title', 'text'];
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}