<?php

namespace App\Entities\Company\Partners;

use Illuminate\Database\Eloquent\Model;

class PartnerGallery extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = 'partner_gallery';
    protected $fillable = ['link_youtube','image','order'];
    public $translatedAttributes = ['title', 'profession'];
}