<?php

namespace App\Entities\Company\Partners;

use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = 'testimonials';
    protected $fillable = ['image','order'];
    public $translatedAttributes = ['title', 'company', 'text'];
}