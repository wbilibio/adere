<?php

namespace App\Entities\Company\Partners;

use Illuminate\Database\Eloquent\Model;

class PartnersTranslation extends Model
{
    public $timestamps = false;
}

