<?php

namespace App\Entities\Company\Partners;

use Illuminate\Database\Eloquent\Model;

class TestimonialsTranslation extends Model
{
    public $timestamps = false;
}

