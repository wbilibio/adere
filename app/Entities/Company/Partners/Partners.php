<?php

namespace App\Entities\Company\Partners;

use Illuminate\Database\Eloquent\Model;

class Partners extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = 'partners';
    public $translatedAttributes = ['title', 'text', 'text_gallery', 'text_depositions'];

}