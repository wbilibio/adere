<?php

namespace App\Entities\Company\Partners;

use Illuminate\Database\Eloquent\Model;

class PartnerGalleryTranslation extends Model
{
    public $timestamps = false;
}

