<?php

namespace App\Entities\Company\CustomProducts;

use Illuminate\Database\Eloquent\Model;

class CustomProducts extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = 'custom_products';
    public $translatedAttributes = ['title', 'text', 'text_solution'];

}