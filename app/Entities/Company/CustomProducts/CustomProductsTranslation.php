<?php

namespace App\Entities\Company\CustomProducts;

use Illuminate\Database\Eloquent\Model;

class CustomProductsTranslation extends Model
{
    public $timestamps = false;
}

