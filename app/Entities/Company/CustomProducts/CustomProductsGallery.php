<?php

namespace App\Entities\Company\CustomProducts;

use Illuminate\Database\Eloquent\Model;

class CustomProductsGallery extends Model
{
    protected $table = 'custom_products_gallery';
    protected $fillable = [
        'image',
        'order'
    ];

}
