<?php

namespace App\Entities\Company\About;

use Illuminate\Database\Eloquent\Model;

class AboutConcept extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = 'about_concept';
    protected $fillable = ['image', 'order'];
    public $translatedAttributes = ['title', 'text'];

}