<?php

namespace App\Entities\Company\About;

use Illuminate\Database\Eloquent\Model;

class Institutional extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = 'institutional';
    public $translatedAttributes = ['title', 'text'];

}