<?php

namespace App\Entities\Company\About;

use Illuminate\Database\Eloquent\Model;

class InstitutionalTranslation extends Model
{
    public $timestamps = false;
}

