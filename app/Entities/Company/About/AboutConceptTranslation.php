<?php

namespace App\Entities\Company\About;

use Illuminate\Database\Eloquent\Model;

class AboutConceptTranslation extends Model
{
    public $timestamps = false;
}

