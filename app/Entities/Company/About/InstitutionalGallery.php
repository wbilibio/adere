<?php

namespace App\Entities\Company\About;

use Illuminate\Database\Eloquent\Model;

class InstitutionalGallery extends Model
{
    protected $table = 'institutional_gallery';
    protected $fillable = [
        'image',
        'order'
    ];

}
