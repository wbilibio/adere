<?php

namespace App\Entities\Company\Certification;

use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = 'certification';
    protected $fillable = ['image','pdf', 'order'];
    public $translatedAttributes = ['title', 'text'];

}