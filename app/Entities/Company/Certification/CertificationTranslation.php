<?php

namespace App\Entities\Company\Certification;

use Illuminate\Database\Eloquent\Model;

class CertificationTranslation extends Model
{
    public $timestamps = false;
}

