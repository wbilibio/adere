<?php

namespace App\Entities\Catalog;

use Illuminate\Database\Eloquent\Model;

class CatalogTranslation extends Model
{
    public $timestamps = false;
}

