<?php

namespace App\Entities\Catalog;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = 'catalog';
    protected $fillable = ['image','file', 'order'];
    public $translatedAttributes = ['title', 'text'];

}