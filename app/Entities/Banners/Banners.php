<?php

namespace App\Entities\Banners;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    protected $table = 'banners';
    protected $fillable = ['image','link', 'type'];

}