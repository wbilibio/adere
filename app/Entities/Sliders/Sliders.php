<?php

namespace App\Entities\Sliders;

use Illuminate\Database\Eloquent\Model;

class Sliders extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = 'sliders';
    protected $fillable = ['image','image_background','link', 'order'];
    public $translatedAttributes = ['title', 'text'];

}