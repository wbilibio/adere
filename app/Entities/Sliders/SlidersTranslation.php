<?php

namespace App\Entities\Sliders;

use Illuminate\Database\Eloquent\Model;

class SlidersTranslation extends Model
{
    public $timestamps = false;
}

