<?php
/**
 *  * MUNDO TIGRE - GESTORES
 *  *
 * @package App\Entities
 * @author Vanderlei Sbaraini Amancio <vanderlei@nextidea.com.br>
 * @since 27/03/2017 - 16:57
 */

namespace App\Entities;

trait HasTranslationTrait
{
    public function getAttribute($key)
    {
        if (!$key) {
            return;
        }

        // If the attribute exists in the attribute array or has a "get" mutator we will
        // get the attribute's value. Otherwise, we will proceed as if the developers
        // are asking for a relationship's value. This covers both types of values.
        if (array_key_exists($key, $this->attributes) ||
            $this->hasGetMutator($key)
        ) {
            return $this->getAttributeValue($key);
        }

        // If the attribute exists in the form of pt_, en_, or es_, we will proceed
        // returning the current locale value.
        if (array_key_exists('pt_' . $key, $this->attributes)) {
            $lang = app()->getLocale();
            return $this->getAttributeValue($lang . '_' . $key);
        }

        return parent::getAttribute($key);
    }
}