<?php

namespace App\Entities\Texts;

use Illuminate\Database\Eloquent\Model;

class Texts extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = 'texts';
    public $translatedAttributes = [
        'text_market_of_action',
        'text_our_brands',
        'text_most_wanted_products',
        'text_custom_product',
        'text_company_in_the_world',
        'text_latest_posts',
        'text_ombudsman'
    ];

}