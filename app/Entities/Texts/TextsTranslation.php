<?php

namespace App\Entities\Texts;

use Illuminate\Database\Eloquent\Model;

class TextsTranslation extends Model
{
    public $timestamps = false;
}

