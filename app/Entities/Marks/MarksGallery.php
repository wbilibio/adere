<?php

namespace App\Entities\Marks;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MarksGallery extends Model
{
    protected $table = "marks_gallery";
    protected $fillable = ['marks_id','image', 'order'];

    public function marks(): BelongsTo
    {
        return $this->belongsTo(Marks::class);
    }
}