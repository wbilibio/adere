<?php

namespace App\Entities\Marks;

use App\Entities\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Marks extends Model
{
    use \Dimsav\Translatable\Translatable;
    use Sluggable;

    protected $fillable = ['image','status','order'];
    public $translatedAttributes = ['title','text'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function products(): HasMany
    {
        return $this->HasMany(Product::class,'mark_id');
    }

    public function galleries(): HasMany
    {
        return $this->hasMany(MarksGallery::class);
    }
}