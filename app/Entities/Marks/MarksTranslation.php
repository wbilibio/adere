<?php

namespace App\Entities\Marks;

use Illuminate\Database\Eloquent\Model;

class MarksTranslation extends Model
{
    public $timestamps = false;
}

