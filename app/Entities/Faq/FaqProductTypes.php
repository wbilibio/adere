<?php

namespace App\Entities\Faq;

use Illuminate\Database\Eloquent\Model;

class FaqProductTypes extends Model
{

    protected $fillable = ['faq_id','product_types_id'];

    public $timestamps = false;

}