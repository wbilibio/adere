<?php

namespace App\Entities\Faq;

use Illuminate\Database\Eloquent\Model;

class FaqMarkets extends Model
{

    protected $fillable = ['faq_id','markets_id'];

    public $timestamps = false;

}