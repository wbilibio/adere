<?php

namespace App\Entities\Faq;

use App\Entities\Markets\Market;
use App\Entities\Products\ProductType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Faq extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $table = 'faq';
    protected $fillable = ['status','order'];
    public $translatedAttributes = ['title', 'text'];

    public function types(): BelongsToMany
    {
        return $this->BelongsToMany(ProductType::class);
    }

    public function markets(): BelongsToMany
    {
        return $this->BelongsToMany(Market::class);
    }

}