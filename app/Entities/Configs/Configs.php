<?php

namespace App\Entities\Configs;

use Illuminate\Database\Eloquent\Model;

class Configs extends Model
{

    protected $table = 'configs';
    protected $fillable = [
        'phone_world',
        'google_analytics',
        'link_facebook',
        'link_twitter',
        'link_linkedin',
        'link_google_plus',
        'link_youtube',
        'phone_sac',
        'office_hours',
        'phone_pabx',
        'phone_fax',
        'address',
        'link_store',
        'link_2_via_boletos'
    ];

}