<?php

namespace App\Entities\Configs;

use Illuminate\Database\Eloquent\Model;

class UsersDownloadFile extends Model
{
    protected $table = 'users_download_files';
    protected $fillable = [
        'name',
        'office',
        'company',
        'market',
        'email',
        'file_name'
    ];

}