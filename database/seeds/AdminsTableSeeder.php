<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (\App\Entities\Admin::count() > 0) {
            return;
        }


        DB::table('admins')->insert([
            'name'        => 'Adere',
            'email'       => 'contato@adere.com.br',
            'permissions' => '0',
            'password'    => bcrypt('aten!@420--'),
            'created_at'  => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'  => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('admins')->insert([
            'name'        => 'William Bilibio',
            'email'       => 'wbilibio@gmail.com',
            'permissions' => '1',
            'password'    => bcrypt('master1234'),
            'created_at'  => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'  => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('admins')->insert([
            'name'        => 'Admin',
            'email'       => 'admin@admin.com',
            'permissions' => '1',
            'password'    => bcrypt('admin'),
            'created_at'  => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'  => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
