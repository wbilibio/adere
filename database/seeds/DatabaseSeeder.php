<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(ProductionDataSeeder::class);
    }

    public function canSeed()
    {
        $seedExists = File::exists(base_path('.seed'));

        if (!$seedExists) {
            File::put(base_path('.seed'), '');
        }

        $seedFile = File::get(base_path('.seed'));
        preg_match('/\d{2}\/\d{2}\/\d{4} \d{2}:\d{2}:\d{2}/', $seedFile, $matches);

        return !$matches || !count($matches);
    }
}