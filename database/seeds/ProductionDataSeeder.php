<?php

use App\Entities\Markets\Market;
use App\Entities\Products\Product;
use App\Entities\Products\ProductFamily;
use App\Entities\Products\ProductType;
use Illuminate\Database\Seeder;

class ProductionDataSeeder extends Seeder
{

    public $markets;
    public $products;
    public $families;
    public $types;

    public function __construct()
    {
        $this->markets = collect([]);
        $this->products = collect([]);
        $this->families = collect([]);
        $this->types = collect([]);
    }

    public function loadData($filename)
    {
        return collect(json_decode(file_get_contents(database_path('seeds' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . $filename))));
    }

    public function createMarkets()
    {
        $items = $this->loadData('mercados.json');
        $items->each(function ($item) {
            $market = Market::create([
                'pt_title' => $item->pt_mercado,
                'en_title' => $item->en_mercado,
                'es_title' => $item->es_mercado,
                'order'    => null,
                'status'   => 1,
            ]);
            $this->markets->put($item->id, $market);
        });
    }

    public function createTypes()
    {
        $items = $this->loadData('tipos_produtos.json');

        $items->each(function ($item) {
            $type = ProductType::create([
                'pt_title' => $item->pt_tipo,
                'en_title' => $item->en_tipo,
                'es_title' => $item->es_tipo,
                'status'   => 1,
            ]);

            $this->types->put($item->adere_tipo_id, $type);
        });
    }

    public function createFamilies()
    {
        $items = $this->loadData('familias_produtos.json');

        $items->each(function ($item) {
            $type = $this->types->get($item->adere_tipo);

            $family = $type->families()->create([
                'pt_title' => $item->pt_familia,
                'en_title' => $item->en_familia,
                'es_title' => $item->es_familia,
                'status'   => 1,
            ]);

            $this->families->put($item->id, $family);
        });
    }

    public function createProducts()
    {
        $items = $this->loadData('produtos.json');

        $items->each(function ($item) {
            $family = $this->families->get($item->familia);

            $product = $family->products()->create([
                'pt_title'       => $item->pt_titulo,
                'en_title'       => $item->en_titulo,
                'es_title'       => $item->es_titulo,
                'pt_description' => $item->pt_descricao,
                'en_description' => $item->en_descricao,
                'es_description' => $item->es_descricao,
                'pt_application' => $item->pt_aplicacao,
                'en_application' => $item->en_aplicacao,
                'es_application' => $item->es_aplicacao,
                'order'          => null,
                'status'         => 1,
            ]);

            $this->products->put($item->id, $product);
        });
    }

    public function associateMarketsAndProducts()
    {
        $relations = $this->loadData('mercados_produtos.json');
        $relations->each(function ($relation) {
            $product = $this->products->get($relation->produto);
            $market = $this->markets->get($relation->mercado);
            if ($product && $market) {
                $product->markets()->attach($market->id);
            }
        });
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Market::count() === 0) {
            $this->createMarkets();
        }

        if (ProductType::count() === 0) {
            $this->createTypes();
        }

        if (ProductFamily::count() === 0) {
            $this->createFamilies();
        }

        if (Product::count() === 0) {
            $this->createProducts();
            $this->associateMarketsAndProducts();
        }

    }
}
