<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function(Blueprint $table) {
            $table->increments('id');

            $table->string('phone_world',255)->nullable();
            $table->text('google_analytics')->nullable();
            $table->string('link_facebook',255)->nullable();
            $table->string('link_twitter',255)->nullable();
            $table->string('link_linkedin',255)->nullable();
            $table->string('link_google_plus',255)->nullable();
            $table->string('link_youtube',255)->nullable();
            $table->string('phone_sac',255)->nullable();
            $table->text('office_hours')->nullable();
            $table->string('phone_pabx',255)->nullable();
            $table->string('phone_fax',255)->nullable();
            $table->text('address')->nullable();
            $table->string('link_store',255)->nullable();
            $table->string('link_2_via_boletos',255)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
