<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_products', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('custom_products_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('custom_products_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();
            $table->text('text')->nullable();
            $table->text('text_solution')->nullable();

            $table->unique(['custom_products_id','locale']);
            $table->foreign('custom_products_id')->references('id')->on('custom_products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_products');
        Schema::dropIfExists('custom_products_translations');
    }
}
