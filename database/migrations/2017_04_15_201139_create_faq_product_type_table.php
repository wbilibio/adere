<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqProductTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_product_types', function (Blueprint $table) {
            $table->integer('faq_id')->unsigned();
            $table->integer('product_types_id')->unsigned();

            $table->foreign('faq_id')->references('id')->on('faq')->onDelete('cascade');
            $table->foreign('product_types_id')->references('id')->on('product_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_product_types');
    }
}
