<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_projects', function(Blueprint $table) {
            $table->increments('id');
            $table->string('image',255)->nullable();
            $table->string('slug',255)->nullable();
            $table->string('link_youtube_1',255)->nullable();
            $table->string('image_1',255)->nullable();
            $table->string('link_youtube_2',255)->nullable();
            $table->string('image_2',255)->nullable();
            $table->string('link_youtube_3',255)->nullable();
            $table->string('image_3',255)->nullable();
            $table->string('link_youtube_4',255)->nullable();
            $table->string('image_4',255)->nullable();
            $table->integer('order');
            $table->timestamps();
        });

        Schema::create('social_projects_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('social_projects_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();
            $table->text('text')->nullable();

            $table->unique(['social_projects_id','locale']);
            $table->foreign('social_projects_id')->references('id')->on('social_projects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_projects');
        Schema::dropIfExists('social_projects_translations');
    }
}
