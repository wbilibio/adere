<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersDownloadFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_download_files', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name',255)->nullable();
            $table->string('office',255)->nullable();
            $table->string('company',255)->nullable();
            $table->string('market',255)->nullable();
            $table->string('file_name',255)->nullable();
            $table->string('email',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_download_files');
    }
}
