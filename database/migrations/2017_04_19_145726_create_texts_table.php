<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('texts', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('texts_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('texts_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->text('text_market_of_action')->nullable();
            $table->text('text_our_brands')->nullable();
            $table->text('text_most_wanted_products')->nullable();
            $table->text('text_custom_product')->nullable();
            $table->text('text_company_in_the_world')->nullable();
            $table->text('text_latest_posts')->nullable();
            $table->text('text_ombudsman')->nullable();

            $table->unique(['texts_id','locale']);
            $table->foreign('texts_id')->references('id')->on('texts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('texts');
        Schema::dropIfExists('texts_translations');
    }
}
