<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function(Blueprint $table) {
            $table->increments('id');
            $table->string('image',255)->nullable();
            $table->string('image_background',255)->nullable();
            $table->string('link',255)->nullable();
            $table->integer('order');
            $table->timestamps();
        });

        Schema::create('sliders_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('sliders_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();
            $table->text('text')->nullable();

            $table->unique(['sliders_id','locale']);
            $table->foreign('sliders_id')->references('id')->on('sliders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
        Schema::dropIfExists('sliders_translations');
    }
}
