<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageBankListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_bank_list', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('image_bank_id')->unsigned();
            $table->string('image',255)->nullable();
            $table->string('image_thumb',255)->nullable();
            $table->integer('status')->nullable();
            $table->integer('order');
            $table->foreign('image_bank_id')->references('id')->on('image_bank')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('image_bank_list_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('image_bank_list_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();

            $table->unique(['image_bank_list_id','locale']);
            $table->foreign('image_bank_list_id')->references('id')->on('image_bank_list')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_bank_list');
        Schema::dropIfExists('image_bank_list_translations');
    }
}
