<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketsRelatedFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('markets_related_files', function (Blueprint $table) {
            $table->integer('market_id')->unsigned();
            $table->integer('file_id')->unsigned();

            $table->foreign('market_id')->references('id')->on('markets')->onDelete('cascade');
            $table->foreign('file_id')->references('id')->on('markets_files')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('markets_related_files');
    }
}
