<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('partners_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('partners_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();
            $table->text('text')->nullable();
            $table->text('text_gallery')->nullable();
            $table->text('text_testimonials')->nullable();

            $table->unique(['partners_id','locale']);
            $table->foreign('partners_id')->references('id')->on('partners')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
        Schema::dropIfExists('partners_translations');
    }
}
