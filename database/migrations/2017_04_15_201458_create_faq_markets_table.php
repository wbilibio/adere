<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqMarketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_markets', function (Blueprint $table) {
            $table->integer('faq_id')->unsigned();
            $table->integer('markets_id')->unsigned();

            $table->foreign('faq_id')->references('id')->on('faq')->onDelete('cascade');
            $table->foreign('markets_id')->references('id')->on('markets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_markets');
    }
}
