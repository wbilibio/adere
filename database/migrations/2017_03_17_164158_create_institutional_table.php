<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitutionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institutional', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('institutional_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('institutional_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();
            $table->text('text')->nullable();

            $table->unique(['institutional_id','locale']);
            $table->foreign('institutional_id')->references('id')->on('institutional')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institutional');
        Schema::dropIfExists('institutional_translations');
    }
}
