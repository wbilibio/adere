<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marks', function(Blueprint $table) {
            $table->increments('id');
            $table->string('image',255)->nullable();
            $table->string('slug',255)->nullable();
            $table->integer('status')->nullable();
            $table->integer('order')->nullable();
            $table->timestamps();
        });

        Schema::create('marks_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('marks_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();
            $table->string('text')->nullable();

            $table->unique(['marks_id','locale']);
            $table->foreign('marks_id')->references('id')->on('marks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marks');
        Schema::dropIfExists('marks_translations');
    }
}
