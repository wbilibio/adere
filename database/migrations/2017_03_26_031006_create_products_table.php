<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pt_title')->nullable();
            $table->text('pt_description')->nullable();
            $table->text('pt_application')->nullable();
            $table->string('en_title')->nullable();
            $table->text('en_description')->nullable();
            $table->text('en_application')->nullable();
            $table->string('es_title')->nullable();
            $table->text('es_description')->nullable();
            $table->text('es_application')->nullable();
            $table->integer('product_family_id')->unsigned();
            $table->string('slug')->nullable();
            $table->integer('order')->nullable();
            $table->timestamps();

            $table->foreign('product_family_id')->references('id')->on('product_families');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
