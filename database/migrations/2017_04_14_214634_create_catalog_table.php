<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog', function(Blueprint $table) {
            $table->increments('id');
            $table->string('image',255)->nullable();
            $table->string('file',255)->nullable();
            $table->integer('order');
            $table->timestamps();
        });

        Schema::create('catalog_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('catalog_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();
            $table->text('text')->nullable();

            $table->unique(['catalog_id','locale']);
            $table->foreign('catalog_id')->references('id')->on('catalog')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog');
        Schema::dropIfExists('catalog_translations');
    }
}
