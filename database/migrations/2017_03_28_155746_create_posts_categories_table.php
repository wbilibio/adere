<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_categories', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->integer('order')->nullable();
            $table->timestamps();
        });
        Schema::create('posts_categories_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('posts_categories_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();

            $table->unique(['posts_categories_id','locale']);
            $table->foreign('posts_categories_id')->references('id')->on('posts_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts_categories');
        Schema::dropIfExists('posts_categories_translations');
    }
}
