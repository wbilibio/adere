<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsProductFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_product_families', function (Blueprint $table) {
            $table->integer('posts_id')->unsigned();
            $table->integer('product_families_id')->unsigned();

            $table->foreign('posts_id')->references('id')->on('posts')->onDelete('cascade');
            $table->foreign('product_families_id')->references('id')->on('product_families')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts_product_families');
    }
}
