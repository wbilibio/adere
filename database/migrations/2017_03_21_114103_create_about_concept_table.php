<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutConceptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_concept', function(Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->integer('order')->nullable();
            $table->timestamps();
        });

        Schema::create('about_concept_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('about_concept_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();
            $table->text('text')->nullable();

            $table->unique(['about_concept_id','locale']);
            $table->foreign('about_concept_id')->references('id')->on('about_concept')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_concept');
        Schema::dropIfExists('about_concept_translations');
    }
}
