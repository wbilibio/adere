<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_files', function(Blueprint $table) {
            $table->increments('id');
            $table->string('file',255);
            $table->integer('status')->nullable();
            $table->timestamps();
        });

        Schema::create('products_files_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('products_files_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();

            $table->unique(['products_files_id','locale']);
            $table->foreign('products_files_id')->references('id')->on('products_files')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_files');
        Schema::dropIfExists('products_files_translations');
    }
}
