<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_gallery', function(Blueprint $table) {
            $table->increments('id');
            $table->string('link_youtube',255)->nullable();
            $table->string('image',255)->nullable();
            $table->integer('order');
            $table->timestamps();
        });

        Schema::create('partner_gallery_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('partner_gallery_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title',255)->nullable();
            $table->string('profession',255)->nullable();

            $table->unique(['partner_gallery_id','locale']);
            $table->foreign('partner_gallery_id')->references('id')->on('partner_gallery')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_gallery');
        Schema::dropIfExists('partner_gallery_translations');
    }
}
