<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketsFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('markets_files', function(Blueprint $table) {
            $table->increments('id');
            $table->string('file',255);
            $table->integer('status')->nullable();
            $table->timestamps();
        });

        Schema::create('markets_files_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('markets_files_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();

            $table->unique(['markets_files_id','locale']);
            $table->foreign('markets_files_id')->references('id')->on('markets_files')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('markets_files');
        Schema::dropIfExists('markets_files_translations');
    }
}
