<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_bank', function(Blueprint $table) {
            $table->increments('id');
            $table->string('image',255)->nullable();
            $table->string('slug',255)->nullable();
            $table->integer('status')->nullable();
            $table->integer('order');
            $table->timestamps();
        });

        Schema::create('image_bank_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('image_bank_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();

            $table->unique(['image_bank_id','locale']);
            $table->foreign('image_bank_id')->references('id')->on('image_bank')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_bank');
        Schema::dropIfExists('image_bank_translations');
    }
}
