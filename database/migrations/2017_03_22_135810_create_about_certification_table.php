<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutCertificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certification', function(Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('pdf')->nullable();
            $table->integer('order')->nullable();
            $table->timestamps();
        });

        Schema::create('certification_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('certification_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();
            $table->text('text')->nullable();

            $table->unique(['certification_id','locale']);
            $table->foreign('certification_id')->references('id')->on('certification')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certification');
        Schema::dropIfExists('certification_translations');
    }
}
