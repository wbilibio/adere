<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostCommentAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_comment_answers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('post_comments_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('email')->nullable();
            $table->text('text')->nullable();
            $table->integer('status')->nullable();
            $table->foreign('post_comments_id')->references('id')->on('post_comments')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_comment_answers');
    }
}
