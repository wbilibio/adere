<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerTestimonialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonials', function(Blueprint $table) {
            $table->increments('id');
            $table->string('image',255);
            $table->integer('order');
            $table->timestamps();
        });

        Schema::create('testimonials_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('testimonials_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title',255)->nullable();
            $table->string('company',255)->nullable();
            $table->text('text')->nullable();

            $table->unique(['testimonials_id','locale']);
            $table->foreign('testimonials_id')->references('id')->on('testimonials')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonials');
        Schema::dropIfExists('testimonials_translations');
    }
}
