<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('slug')->nullable();
            $table->bigInteger('viewed')->default(0);
            $table->string('image',255);
            $table->foreign('category_id')->references('id')->on('posts_categories');
            $table->timestamps();
        });

        Schema::create('posts_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('posts_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();
            $table->mediumText('text')->nullable();

            $table->unique(['posts_id','locale']);
            $table->foreign('posts_id')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
        Schema::dropIfExists('posts_translations');
    }
}
