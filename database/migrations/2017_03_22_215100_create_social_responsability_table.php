<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialResponsabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responsability', function(Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->timestamps();
        });

        Schema::create('responsability_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('responsability_id')->unsigned();
            $table->string('locale')->index();

            // campos traduziveis
            $table->string('title')->nullable();
            $table->string('title_projects')->nullable();
            $table->text('text')->nullable();
            $table->text('text_projects')->nullable();
            $table->text('text_gallery')->nullable();

            $table->unique(['responsability_id','locale']);
            $table->foreign('responsability_id')->references('id')->on('responsability')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responsability');
        Schema::dropIfExists('responsability_translations');
    }
}
